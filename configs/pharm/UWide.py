from m5.objects import *

# My attempt to model UWide, need to add/extend instruction classes to
# really model. Latencies are also probably off in some places, but set
# to what is used by crib

# Execution Unit Resources
class Port0_FU(FUDesc):
    opList = [ OpDesc(opClass="IntAlu", opLat=1),
               OpDesc(opClass="IntMult", opLat=3),
               OpDesc(opClass="IprAccess", opLat=3),
               OpDesc(opClass="IntDiv", opLat=20, issueLat=20),
               OpDesc(opClass="FloatAdd", opLat=5),
               OpDesc(opClass="FloatMult", opLat=5),
               OpDesc(opClass="FloatCvt", opLat=3),
               OpDesc(opClass="FloatCmp", opLat=5),
               OpDesc(opClass="FloatDiv", opLat=10),
               OpDesc(opClass="FloatSqrt", opLat=33),
               OpDesc(opClass="SimdFloatAlu", opLat=5),
               OpDesc(opClass="SimdFloatAdd", opLat=5),
               OpDesc(opClass="SimdFloatMult", opLat=5),
               OpDesc(opClass="SimdFloatMultAcc", opLat=6),
               OpDesc(opClass="SimdFloatCvt", opLat=3),
               OpDesc(opClass="SimdFloatDiv", opLat=10),
               OpDesc(opClass="SimdFloatSqrt", opLat=10),
               OpDesc(opClass="SimdFloatCmp", opLat=3),
               OpDesc(opClass="SimdFloatMisc", opLat=3),
               OpDesc(opClass="SimdCmp", opLat=4),
               OpDesc(opClass="SimdMisc", opLat=3),
               OpDesc(opClass="SimdAddAcc", opLat=4),
               OpDesc(opClass="SimdAdd", opLat=4),
               OpDesc(opClass="SimdAlu", opLat=4),
               OpDesc(opClass="SimdCvt", opLat=3),
               OpDesc(opClass="SimdMult", opLat=5),
               OpDesc(opClass="SimdMultAcc", opLat=5),
               OpDesc(opClass="SimdShiftAcc", opLat=3),
               OpDesc(opClass="SimdShift", opLat=3),
               OpDesc(opClass="SimdSqrt", opLat=9),
               OpDesc(opClass="MemRead", opLat=1),
               OpDesc(opClass="MemWrite", opLat=1) ]
    count = 8

class UWide_FUPool(FUPool):
    FUList = [ Port0_FU() ]

class UWide_BP(BranchPredictor):
    predType = "tournament"
    localCtrBits = 2
    localHistoryTableSize = 64
    localHistoryBits = 6
    globalPredictorSize = 8192
    globalCtrBits = 2
    globalHistoryBits = 13
    choicePredictorSize = 8192
    choiceCtrBits = 2
    BTBEntries = 2048
    BTBTagSize = 18
    RASSize = 16
    instShiftAmt = 2

class UWide(DerivO3CPU):
    LQEntries = 192
    SQEntries = 128
    LSQDepCheckShift = 0
    LFSTSize = 1024
    SSITSize = 1024
    decodeToFetchDelay = 1
    renameToFetchDelay = 1
    iewToFetchDelay = 1
    commitToFetchDelay = 1
    renameToDecodeDelay = 1
    iewToDecodeDelay = 1
    commitToDecodeDelay = 1
    iewToRenameDelay = 1
    commitToRenameDelay = 1
    commitToIEWDelay = 1
    fetchWidth = 8
    fetchToDecodeDelay = 4
    decodeWidth = 8
    decodeToRenameDelay = 1
    renameWidth = 8
    renameToIEWDelay = 3
    issueToExecuteDelay = 1
    dispatchWidth = 8
    issueWidth = 8
    wbWidth = 8
    wbDepth = 1
    fuPool = UWide_FUPool()
    iewToCommitDelay = 1
    renameToROBDelay = 1
    commitWidth = 8
    squashWidth = 16
    trapLatency = 13
    backComSize = 10
    forwardComSize = 5
    numPhysIntRegs = 512
    numPhysFloatRegs = 512
    numIQEntries = 120
    numROBEntries = 512

    switched_out = False
    branchPred = UWide_BP()
