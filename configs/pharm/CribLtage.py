from m5.objects import *

class Crib_BP(BranchPredictor):
    predType = "ltage"
    localCtrBits = 2
    localHistoryTableSize = 64
    localHistoryBits = 6
    globalPredictorSize = 8192
    globalCtrBits = 2
    globalHistoryBits = 13
    choicePredictorSize = 8192
    choiceCtrBits = 2
    BTBEntries = 2048
    BTBTagSize = 18
    RASSize = 16
    instShiftAmt = 2

class CribLtage(CribCPU):
    branchPred = Crib_BP()
