from m5.objects import *

# My attempt to model Nehalem, need to add/extend instruction classes to
# really model. Latencies are also probably off in some places, but set
# to what is used by crib

# Execution Unit Resources
class Port0_FU(FUDesc):
    opList = [ OpDesc(opClass="IntAlu", opLat=1),
               OpDesc(opClass="IntDiv", opLat=20, issueLat=20),
               OpDesc(opClass="FloatMult", opLat=5),
               OpDesc(opClass="FloatCvt", opLat=3),
               OpDesc(opClass="FloatDiv", opLat=10),
               OpDesc(opClass="FloatSqrt", opLat=33),
               OpDesc(opClass="SimdFloatMult", opLat=5),
               OpDesc(opClass="SimdFloatMultAcc", opLat=6),
               OpDesc(opClass="SimdFloatCvt", opLat=3),
               OpDesc(opClass="SimdFloatDiv", opLat=10),
               OpDesc(opClass="SimdFloatSqrt", opLat=10),
               OpDesc(opClass="SimdAddAcc", opLat=4),
               OpDesc(opClass="SimdAdd", opLat=4),
               OpDesc(opClass="SimdAlu", opLat=4),
               OpDesc(opClass="SimdShiftAcc", opLat=3),
               OpDesc(opClass="SimdShift", opLat=3) ]
    count = 1

class Port1_FU(FUDesc):
    opList = [ OpDesc(opClass="IntAlu", opLat=1),
               OpDesc(opClass="IntMult", opLat=3),
               OpDesc(opClass="IprAccess", opLat=3),
               OpDesc(opClass="FloatAdd", opLat=5),
               OpDesc(opClass="SimdFloatAlu", opLat=5),
               OpDesc(opClass="SimdFloatAdd", opLat=5),
               OpDesc(opClass="SimdMult", opLat=5),
               OpDesc(opClass="SimdMultAcc", opLat=5),
               OpDesc(opClass="SimdSqrt", opLat=9),
               OpDesc(opClass="SimdCvt", opLat=3) ]
    count = 1

class Port2_FU(FUDesc):
    opList = [ OpDesc(opClass="MemRead", opLat=1),
               OpDesc(opClass="MemWrite", opLat=1) ]
    count = 1

class Port3_FU(FUDesc):
    opList = [ OpDesc(opClass="MemRead", opLat=1),
               OpDesc(opClass="MemWrite", opLat=1) ]
    count = 1

class Port4_FU(FUDesc):
    opList = [ OpDesc(opClass="MemWrite", opLat=1) ]
    count = 1

class Port5_FU(FUDesc):
    opList = [ OpDesc(opClass="IntAlu", opLat=1),
               OpDesc(opClass="FloatCmp", opLat=5),
               OpDesc(opClass="SimdFloatCmp", opLat=3),
               OpDesc(opClass="SimdFloatMisc", opLat=3),
               OpDesc(opClass="SimdCmp", opLat=4),
               OpDesc(opClass="SimdMisc", opLat=3),
               OpDesc(opClass="SimdAdd", opLat=4),
               OpDesc(opClass="SimdAddAcc", opLat=4),
               OpDesc(opClass="SimdShiftAcc", opLat=3),
               OpDesc(opClass="SimdShift", opLat=3),
               OpDesc(opClass="SimdAlu", opLat=4) ]
    count = 1

class Port6_FU(FUDesc):
    opList = [ OpDesc(opClass="MemWrite", opLat=1) ]
    count = 1

class Port7_FU(FUDesc):
    opList = [ OpDesc(opClass="IntAlu", opLat=1)]
    count = 1

class Nehalem_FUPool(FUPool):
    FUList = [ Port0_FU(), Port1_FU(), Port2_FU(), Port3_FU(), Port4_FU(), Port5_FU(), Port6_FU(), Port7_FU() ]

class Nehalem_BP(BranchPredictor):
    predType = "ltage"
    localCtrBits = 2
    localHistoryTableSize = 64
    localHistoryBits = 6
    globalPredictorSize = 8192
    globalCtrBits = 2
    globalHistoryBits = 13
    choicePredictorSize = 8192
    choiceCtrBits = 2
    BTBEntries = 2048
    BTBTagSize = 18
    RASSize = 16
    instShiftAmt = 2

class NehalemLtage(DerivO3CPU):
    LQEntries = 72
    SQEntries = 42
    LSQDepCheckShift = 0
    LFSTSize = 1024
    SSITSize = 1024
    decodeToFetchDelay = 1
    renameToFetchDelay = 1
    iewToFetchDelay = 1
    commitToFetchDelay = 1
    renameToDecodeDelay = 1
    iewToDecodeDelay = 1
    commitToDecodeDelay = 1
    iewToRenameDelay = 1
    commitToRenameDelay = 1
    commitToIEWDelay = 1
    fetchWidth = 4
    fetchToDecodeDelay = 2
    decodeWidth = 4
    decodeToRenameDelay = 1
    renameWidth = 4
    renameToIEWDelay = 3
    issueToExecuteDelay = 1
    dispatchWidth = 4
    issueWidth = 8
    wbWidth = 6
    wbDepth = 1
    fuPool = Nehalem_FUPool()
    iewToCommitDelay = 1
    renameToROBDelay = 1
    commitWidth = 4
    squashWidth = 16
    trapLatency = 13
    backComSize = 10
    forwardComSize = 5
    numPhysIntRegs = 168
    numPhysFloatRegs = 168
    numIQEntries = 60
    numROBEntries = 192

    switched_out = False
    branchPred = Nehalem_BP()
