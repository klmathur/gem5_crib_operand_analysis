#include "enums/ArmMachineType.hh"
namespace Enums {
    const char *ArmMachineTypeStrings[Num_ArmMachineType] =
    {
        "RealView_EB",
        "RealView_PBX",
        "VExpress_CA9",
        "VExpress_ELT",
        "VExpress_EMM",
    };
} // namespace Enums
