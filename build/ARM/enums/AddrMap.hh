#ifndef __ENUM__AddrMap__
#define __ENUM__AddrMap__

namespace Enums {
    enum AddrMap {
        RaBaChCo = 0,
        RaBaCoCh = 1,
        CoRaBaCh = 2,
        Num_AddrMap = 3
    };
extern const char *AddrMapStrings[Num_AddrMap];
}

#endif // __ENUM__AddrMap__
