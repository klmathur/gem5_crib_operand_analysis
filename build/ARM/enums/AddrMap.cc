#include "enums/AddrMap.hh"
namespace Enums {
    const char *AddrMapStrings[Num_AddrMap] =
    {
        "RaBaChCo",
        "RaBaCoCh",
        "CoRaBaCh",
    };
} // namespace Enums
