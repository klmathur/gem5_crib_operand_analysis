/*
 * DO NOT EDIT THIS FILE! Automatically generated
 */

#include "base/debug.hh"
#include "debug/Crib.hh"
#include "debug/CribCPU.hh"
#include "debug/CribCommit.hh"
#include "debug/CribDecode.hh"
#include "debug/CribDispatch.hh"
#include "debug/CribExecute.hh"
#include "debug/CribFetch.hh"
#include "debug/CribLSQ.hh"
#include "debug/CribLoop.hh"
#include "debug/CribMem.hh"
#include "debug/CribPrefetch.hh"

namespace Debug {

CompoundFlag CribCPUAll("CribCPUAll", "None",
    Crib,
    CribCPU,
    CribCommit,
    CribDecode,
    CribDispatch,
    CribExecute,
    CribFetch,
    CribLSQ,
    CribLoop,
    CribMem,
    CribPrefetch);

} // namespace Debug
