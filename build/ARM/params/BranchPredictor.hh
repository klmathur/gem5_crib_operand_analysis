#ifndef __PARAMS__BranchPredictor__
#define __PARAMS__BranchPredictor__

class BPredUnit;

#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include <string>
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"

#include "params/SimObject.hh"

struct BranchPredictorParams
    : public SimObjectParams
{
    BPredUnit * create();
    unsigned logSizeTagTables;
    unsigned tagTableCounterBits;
    unsigned numThreads;
    unsigned choicePredictorSize;
    unsigned choiceCtrBits;
    unsigned BTBEntries;
    unsigned localPredictorSize;
    unsigned globalCtrBits;
    unsigned minTagWidth;
    unsigned localHistoryTableSize;
    std::string predType;
    unsigned logSizeLoopPred;
    unsigned histBufferSize;
    unsigned minHist;
    unsigned localCtrBits;
    unsigned nHistoryTables;
    unsigned globalHistoryBits;
    unsigned logSizeBiMP;
    unsigned RASSize;
    unsigned globalPredictorSize;
    unsigned maxHist;
    unsigned instShiftAmt;
    unsigned localHistoryBits;
    unsigned BTBTagSize;
};

#endif // __PARAMS__BranchPredictor__
