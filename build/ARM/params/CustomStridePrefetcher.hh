#ifndef __PARAMS__CustomStridePrefetcher__
#define __PARAMS__CustomStridePrefetcher__

class CustomStridePrefetcher;


#include "params/BasePrefetcher.hh"

struct CustomStridePrefetcherParams
    : public BasePrefetcherParams
{
    CustomStridePrefetcher * create();
};

#endif // __PARAMS__CustomStridePrefetcher__
