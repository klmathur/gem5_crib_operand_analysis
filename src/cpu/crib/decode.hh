#ifndef __CPU_CRIB_DECODE_HH__
#define __CPU_CRIB_DECODE_HH__

#include <queue>

#include "cpu/crib/comm.hh"
#include "cpu/crib/const.hh"
#include "cpu/crib/dyn_inst.hh"
#include "cpu/timebuf.hh"
#include "params/CribCPU.hh"

class CribCPU;

class CribDecode
{
  public:
    CribDecode(CribCPU* _cpu, CribCPUParams *params);
    std::string name() const;
    void regStats();

    void setTimeBuffer(TimeBuffer<TimeStruct> *time_buffer);
    void setDecodeQueue(TimeBuffer<DecodeStruct> *decode_queue);
    void setFetchQueue(TimeBuffer<FetchStruct> *fetch_queue);
    void setActiveThreads(std::list<ThreadID>* active_threads);

    void startupStage();
    void takeOverFrom();
    bool isDrained() const;
    void tick();

  private:
    CribCPU *cpu;

    unsigned decodeWidth;
    unsigned numThreads;
    unsigned fetchToDecodeDelay;
    unsigned dispatchToDecodeDelay;
    unsigned cribToDecodeDelay;
    unsigned commitToDecodeDelay;

    unsigned skidBufferMax;

    TimeBuffer<FetchStruct>::wire fromFetch;
    TimeBuffer<TimeStruct>::wire fromDispatch;
    TimeBuffer<TimeStruct>::wire fromCrib;
    TimeBuffer<TimeStruct>::wire fromCommit;
    TimeBuffer<TimeStruct>::wire toFetch;
    TimeBuffer<DecodeStruct>::wire toDispatch;

    std::list<ThreadID> *activeThreads;
    bool wroteToTimeBuffer;

    unsigned decodeIndex;

    enum DecodeStatus {
        Active,
        Inactive
    };

    enum ThreadStatus {
        Running,
        Idle,
        Squashing,
        Blocked,
        Unblocking
    };

    struct Stalls {
        bool dispatch;
        bool crib;
        bool commit;
    };

    DecodeStatus status;

    struct DecodeState {
        ThreadStatus status;
        std::queue<CribDynInstPtr> insts;
        std::queue<CribDynInstPtr> skidBuffer;
        Stalls stalls;
        CribDynInstPtr squashInst;
        bool squashAfterDelaySlot;
        InstSeqNum ackSquashSeqNum;
        Addr ackPcAddr;
    };

    DecodeState decodeState[CribConst::MaxThreads];

    Stats::Scalar decodeIdleCycles;
    Stats::Scalar decodeBlockedCycles;
    Stats::Scalar decodeRunCycles;
    Stats::Scalar decodeUnblockCycles;
    Stats::Scalar decodeSquashCycles;
    Stats::Scalar decodeBranchResolved;
    Stats::Scalar decodeBranchMispredict;
    Stats::Scalar decodeControlMispredict;
    Stats::Scalar decodeDecodedInsts;

  private:
    // Helper functions
    void resetStage();
    void sortInstructions();
    bool checkSignalsAndUpdate(ThreadID tid);
    void readStallSignals(ThreadID tid);
    void squash(ThreadID tid);
    void squash(const CribDynInstPtr &inst, ThreadID tid);

    bool checkStall(ThreadID tid);
    bool block(ThreadID tid);
    bool unblock(ThreadID tid);
    void skidInsert(ThreadID tid);
    void decode(bool &status_change, ThreadID tid);
    void decodeInstructions(ThreadID tid);
    void updateStatus();
};

#endif // __CPU_CRIB_DECODE_HH__
