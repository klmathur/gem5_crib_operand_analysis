#include "arch/utility.hh"
#include "cpu/crib/cpu.hh"
#include "cpu/crib/thread_context.hh"
#include "debug/Activity.hh"
#include "debug/CribCPU.hh"
#include "debug/Drain.hh"
#include "enums/MemoryMode.hh"
#include "sim/system.hh"

CribCPU *
CribCPUParams::create()
{
  return new CribCPU(this);
}

CribCPU::CribCPU(CribCPUParams *params)
    : BaseCPU(params),
      isa(numThreads, NULL),
      drainManager(NULL),
      contextSwitch(false),
      cribStridePrefetchEnable(params->cribStridePrefetchEnable),
      cribPointerPrefetchEnable(params->cribPointerPrefetchEnable),
      activityRec(name(), NumStages, params->backComSize + params->fwdComSize,
                  params->activity),
      lastRunningCycle(curCycle()),
      lastActivatedCycle(0),
      commitDrainInProgress(false),
      tickEvent(this),
      itb(params->itb),
      dtb(params->dtb),
      fetch(this, params),
      decode(this, params),
      dispatch(this, params),
      crib(this, &lsq, params),
      lsq(this, &crib, params),
      icachePort(&fetch, this),
      dcachePort(&lsq, this),
      timeBuffer(params->backComSize, params->fwdComSize),
      fetchQueue(params->backComSize, params->fwdComSize),
      decodeQueue(params->backComSize, params->fwdComSize),
      dispatchQueue(params->backComSize, params->fwdComSize)
{
    DPRINTF(CribCPU, "Creating CribCPU object.\n");

    fetch.setActiveThreads(&activeThreads);
    decode.setActiveThreads(&activeThreads);

    fetch.setTimeBuffer(&timeBuffer);
    decode.setTimeBuffer(&timeBuffer);
    dispatch.setTimeBuffer(&timeBuffer);
    crib.setTimeBuffer(&timeBuffer);

    fetch.setFetchQueue(&fetchQueue);
    decode.setFetchQueue(&fetchQueue);

    decode.setDecodeQueue(&decodeQueue);
    dispatch.setDecodeQueue(&decodeQueue);

    dispatch.setDispatchQueue(&dispatchQueue);
    crib.setDispatchQueue(&dispatchQueue);

    ThreadID active_threads;
    if (!FullSystem) {
        active_threads = params->workload.size();

        if (active_threads > CribConst::MaxThreads) {
            panic("Workload size too large.");
        }
    }


    thread.resize(numThreads);
    for (ThreadID tid = 0; tid < numThreads; tid++) {
        // Set ISA
        isa[tid] = params->isa[tid];

        // Setup ThreadState
        if (FullSystem) {
          // SMT is not supported in FS mode yet
          assert(numThreads == 1);
          thread[tid] = new CribThreadState(this, tid, NULL);
        } else {
            if (tid < params->workload.size()) {
                DPRINTF(CribCPU, "Workload[%i] process is %#x\n",
                        tid, thread[tid]);

                thread[tid] = new CribThreadState(this, tid,
                    params->workload[tid]);
            } else {
              // Allocate an empty thread so m5 can later use
              // when scheduling threads to this CPU
              thread[tid] = new CribThreadState(this, tid, NULL);
            }
        }

        // Setup ThreadContext
        CribThreadContext *tc = new CribThreadContext(this, thread[tid]);

        thread[tid]->tc = tc;
        threadContexts.push_back(tc);

        activateThreadEvent[tid].init(tid,this);
        deallocateContextEvent[tid].init(tid,this);
    }
}

void
CribCPU::wakeCPU()
{
    if (activityRec.active() || tickEvent.scheduled()) {
        DPRINTF(Activity, "CPU already running.\n");
        return;
    }

    DPRINTF(Activity, "Waking up CPU.\n");
    Cycles cycles(curCycle() - lastRunningCycle);
    // todo this is an oddity here to match the stats
    if (cycles != 0)
        --cycles;

    idleCycles += cycles;
    numCycles += cycles;

    schedule(tickEvent, nextCycle());
}

void
CribCPU::wakeup()
{
    panic("Unimplemented function!");
}

void
CribCPU::activateContext(ThreadID tid, Cycles delay)
{
    if (delay) {
        DPRINTF(CribCPU, "[tid:%i]: Scheduling thread context to activate "
                "on cycle %d\n", tid, clockEdge(delay));
        scheduleActivateThreadEvent(tid, delay);
    } else {
        activateThread(tid);
    }

    if (lastActivatedCycle == 0 || lastActivatedCycle < curTick()) {
        scheduleTickEvent(delay);

        activityRec.activity();
        fetch.wakeFromQuiesce();

        Cycles cycles(curCycle() - lastRunningCycle);
        if (cycles != 0)
            --cycles;
        quiesceCycles += cycles;

        lastActivatedCycle = curTick();

        _status = Running;
    }
}

void
CribCPU::suspendContext(ThreadID tid)
{
    panic("Unimplemented function!");
}

void
CribCPU::deallocateContext(ThreadID tid)
{
    panic("Unimplemented function!");
}

void
CribCPU::haltContext(ThreadID tid)
{
    panic("Unimplemented function!");
}

void
CribCPU::init()
{
    BaseCPU::init();

    if (!params()->switched_out &&
        system->getMemoryMode() != Enums::timing) {
        fatal("The Crib CPU requires the memory system to be in "
              "'timing' mode.\n");
    }

    for (ThreadID tid = 0; tid < numThreads; tid++) {
        // Set inSyscall so that the CPU doesnt squash when initially
        // setting up registers.
        thread[tid]->noSquashFromTC = true;

        // Initialise the ThreadContext's memory proxies
        thread[tid]->initMemProxies(thread[tid]->getTC());
    }

    if (FullSystem && !params()->switched_out) {
        for (ThreadID tid = 0; tid < numThreads; tid++) {
            ThreadContext *src_tc = threadContexts[tid];
            TheISA::initCPU(src_tc, src_tc->contextId());
        }
    }

    // Clear inSyscall
    for (ThreadID tid = 0; tid < numThreads; tid++) {
        thread[tid]->noSquashFromTC = false;
    }
}

void
CribCPU::startup()
{
    fetch.startupStage();
    decode.startupStage();
    lsq.startupStage();
    crib.startupStage();
}

unsigned int
CribCPU::drain(DrainManager *drain_manager)
{
    if (switchedOut()) {
        setDrainState(Drainable::Drained);
        return 0;
    }

    DPRINTF(Drain, "Draining...\n");
    setDrainState(Drainable::Draining);

    crib.drain();

    if (!isDrained()) {
        drainManager = drain_manager;
        wakeCPU();
        activityRec.activity();

        DPRINTF(Drain, "CPU not drained.\n");
        return 1;
    } else {
        setDrainState(Drainable::Drained);
        DPRINTF(Drain, "CPU is already drained.\n");
        if (tickEvent.scheduled()) {
            deschedule(tickEvent);
        }

        for (unsigned i = 0; i < timeBuffer.getSize(); i++) {
            timeBuffer.advance();
            fetchQueue.advance();
            decodeQueue.advance();
            dispatchQueue.advance();
        }
        return 0;
    }
}

void
CribCPU::drainResume()
{
    setDrainState(Drainable::Running);
    if (switchedOut())
        return;

    DPRINTF(Drain, "Resuming...\n");

    if (system->getMemoryMode() != Enums::timing) {
        fatal("The Crib CPU requires the memory system to be in "
              "'timing' mode.\n");
    }

    crib.drainResume();
    _status = Idle;
    for (ThreadID i = 0; i < thread.size(); i++) {
        if (thread[i]->status() == ThreadContext::Active) {
            DPRINTF(Drain, "Activating thread: %i\n", i);
            activateThread(i);
            _status = Running;
        }
    }

    assert(!tickEvent.scheduled());
    if (_status == Running)
        schedule(tickEvent, nextCycle());

}

void
CribCPU::regStats()
{
    BaseCPU::regStats();

    fetch.regStats();
    decode.regStats();
    dispatch.regStats();
    crib.regStats();

    idleCycles
        .name(name() + ".idleCycles")
        .desc("Total number of cycles the CPU has spent unscheduled due "
              "to idling")
        .prereq(idleCycles);

    quiesceCycles
        .name(name() + ".quiesceCycles")
        .desc("Total number of cycles the CPU has spent quiesced or waiting "
              "for an interrupt")
        .prereq(quiesceCycles);


    committedInsts
        .init(numThreads)
        .name(name() + ".committedInsts")
        .desc("Number of instructions committed");

    committedOps
        .init(numThreads)
        .name(name() + ".committedOps")
        .desc("Number of ops (including micro ops) committed");


    cpi
        .name(name() + ".cpi")
        .desc("Cycles per instruction")
        .precision(6);
    cpi = numCycles / committedInsts;

    ipc
        .name(name() + ".ipc")
        .desc("Instructions per cycle")
        .precision(6);
    ipc = committedInsts / numCycles;

    upc
        .name(name() + ".upc")
        .desc("Micro-ops per cycle")
        .precision(6);
    upc = committedOps / numCycles;

    stridePrefetch
        .name(name() + ".stridePrefetch")
        .desc("Number of stride prefetchs generated");
    constantPrefetch
        .name(name() + ".constantPrefetch")
        .desc("Number of constnat prefetchs generated");
    pointerPrefetch
        .name(name() + ".pointerPrefetch")
        .desc("Number of pointer prefetchs generated");
    

}

void
CribCPU::activateWhenReady(ThreadID tid)
{
    panic("Unimplemented function!");
}

void
CribCPU::switchOut()
{
    DPRINTF(CribCPU, "Switching out.\n");

    BaseCPU::switchOut();

    activityRec.reset();

    _status = SwitchedOut;
}

void
CribCPU::takeOverFrom(BaseCPU *cpu)
{
    DPRINTF(Drain, "Take over from.\n");
    BaseCPU::takeOverFrom(cpu);
    fetch.takeOverFrom();
    decode.takeOverFrom();
    dispatch.takeOverFrom();
    crib.takeOverFrom();
    lsq.takeOverFrom();
    assert(!tickEvent.scheduled());

    lastRunningCycle = curCycle();
    _status = Idle;
}

void
CribCPU::serializeThread(std::ostream &os, ThreadID tid)
{
    thread[tid]->serialize(os);
}

void
CribCPU::unserializeThread(Checkpoint *cp, const std::string &section,
                           ThreadID tid)
{
    thread[tid]->unserialize(cp, section);
}

Counter
CribCPU::totalInsts() const
{
    Counter total(0);
    for (ThreadID tid = 0; tid < thread.size(); tid++) {
        total += thread[tid]->numInst;
    }
    return total;
}

Counter
CribCPU::totalOps() const
{
    Counter total(0);
    for (ThreadID tid = 0; tid < thread.size(); tid++) {
        total += thread[tid]->numOp;
    }
    return total;
}

TheISA::PCState
CribCPU::pcState(ThreadID tid)
{
    return crib.pcState(tid);
}

void
CribCPU::pcState(const TheISA::PCState &val, ThreadID tid)
{
    crib.pcState(val, tid);
}

Addr
CribCPU::instAddr(ThreadID tid)
{
    panic("Unimplemented function!");
    M5_DUMMY_RETURN
}

Addr
CribCPU::nextInstAddr(ThreadID tid)
{
    panic("Unimplemented function!");
    M5_DUMMY_RETURN
}

MicroPC
CribCPU::microPC(ThreadID tid)
{
    panic("Unimplemented function!");
    M5_DUMMY_RETURN
}

MiscReg
CribCPU::readMiscRegNoEffect(int misc_reg, ThreadID tid)
{
    return isa[tid]->readMiscRegNoEffect(misc_reg);
}

MiscReg
CribCPU::readMiscReg(int misc_reg, ThreadID tid)
{
    return isa[tid]->readMiscReg(misc_reg, thread[tid]->getTC());
}

void
CribCPU::setMiscRegNoEffect(int misc_reg, const MiscReg &val, ThreadID tid)
{
    isa[tid]->setMiscRegNoEffect(misc_reg, val);
}

void
CribCPU::setMiscReg(int misc_reg, const MiscReg &val, ThreadID tid)
{
    isa[tid]->setMiscReg(misc_reg, val, thread[tid]->getTC());
}

IntReg
CribCPU::readArchIntReg(int reg_idx, ThreadID tid)
{
        	
    printf("Read Arch Int :%d\n",reg_idx);
    return crib.readArchIntReg(reg_idx, tid);
}

FloatReg
CribCPU::readArchFloatReg(int reg_idx, ThreadID tid)
{
    return crib.readArchFloatReg(reg_idx, tid);
}

FloatRegBits
CribCPU::readArchFloatRegBits(int reg_idx, ThreadID tid)
{
    return crib.readArchFloatRegBits(reg_idx, tid);
}

void
CribCPU::setArchIntReg(int reg_idx, IntReg val, ThreadID tid)
{
    crib.setArchIntReg(reg_idx, val, tid);
}

void
CribCPU::setArchFloatReg(int reg_idx, FloatReg val, ThreadID tid)
{
    crib.setArchFloatReg(reg_idx, val, tid);
}

void
CribCPU::setArchFloatRegBits(int reg_idx, FloatRegBits val, ThreadID tid)
{
    crib.setArchFloatRegBits(reg_idx, val, tid);
}

/*
void
CribCPU::syscall(int64_t callnum, ThreadID tid)
{
    panic("Unimplemented function!");
}*/

void
CribCPU::squashFromTC(ThreadID tid)
{
    crib.squashFromTC(tid);
}

void
CribCPU::tick()
{
    DPRINTF(CribCPU, "tick\n");
    assert(!switchedOut());
    assert(getDrainState() != Drainable::Drained);

    ++numCycles;

    fetch.tick();
    decode.tick();
    dispatch.tick();
    // LSQ assumes it's ticked before crib for timing
    lsq.tick();
    crib.tick();

    timeBuffer.advance();
    fetchQueue.advance();
    decodeQueue.advance();
    dispatchQueue.advance();

    activityRec.advance();

    if (!tickEvent.scheduled()) {
        if ( _status == SwitchedOut) {
            DPRINTF(CribCPU, "Switched out.\n");
            lastRunningCycle = curCycle();
        //} else if (!activityRec.active() || _status == Idle) {
        //    DPRINTF(CribCPU, "Idle.\n");
        //    lastRunningCycle = curCycle();
        } else {
            schedule(tickEvent, clockEdge(Cycles(1)));
            DPRINTF(CribCPU, "Scheduling next tick.\n");
        }
    }

    tryDrain();
}

inline void
CribCPU::tryDrain()
{
    if (!drainManager || !isDrained()) {
        return;
    }

    if (tickEvent.scheduled()) {
        deschedule(tickEvent);
    }

    DPRINTF(Drain, "CPU done draining, signaling drain event\n");
    drainManager->signalDrainDone();
    drainManager = NULL;
}

inline bool
CribCPU::isDrained() const
{
    bool drained = true;

    for (ThreadID i = 0; i < thread.size(); i++) {
        if (activateThreadEvent[i].scheduled()) {
            DPRINTF(Drain, "CPU not drained, thread %i has a pending "
                    "activate event.\n", i);
            drained = false;
        }
        if (deallocateContextEvent[i].scheduled()) {
            DPRINTF(Drain, "CPU not drained, thread %i has a pending "
                    "deallocate context event.\n", i);
            drained = false;
        }
    }

    if (!fetch.isDrained()) {
        DPRINTF(Drain, "Fetch is not drained.\n");
        drained = false;
    }

    if (!decode.isDrained()) {
        DPRINTF(Drain, "Decode is not drained.\n");
        drained = false;
    }

    if (!dispatch.isDrained()) {
        DPRINTF(Drain, "Dispatch is not drained.\n");
        drained = false;
    }

    if (!crib.isDrained()) {
        DPRINTF(Drain, "Crib is not drained.\n");
        drained = false;
    }

    if (!lsq.isDrained()) {
        DPRINTF(Drain, "LSQ is not drained.\n");
        drained = false;
    }

    if (commitDrainInProgress) {
        DPRINTF(Drain, "Commit drain in progress.\n");
        drained = false;
    }

    return drained;
}

/** ActivateThreadEvent */
CribCPU::ActivateThreadEvent::ActivateThreadEvent()
    : Event(CPU_Switch_Pri)
{

}

void
CribCPU::ActivateThreadEvent::init(ThreadID _tid, CribCPU *_cpu)
{
    tid = _tid;
    cpu = _cpu;
}

void
CribCPU::ActivateThreadEvent::process()
{
    cpu->activateThread(tid);
}

const char *
CribCPU::ActivateThreadEvent::description() const
{
    return "CribCPU \"Activate Thread\"";
}

void
CribCPU::scheduleActivateThreadEvent(ThreadID tid, Cycles delay)
{
    if (activateThreadEvent[tid].squashed()) {
        reschedule(activateThreadEvent[tid],
                clockEdge(delay));
    } else if (!activateThreadEvent[tid].scheduled()) {
        Tick when = clockEdge(delay);

        // Check if the deallocateEvent is also scheduled, and make
        // sure they do not happen at the same time causing a sleep that
        // is never woken from.
        if (deallocateContextEvent[tid].scheduled() &&
            deallocateContextEvent[tid].when() == when) {
            when++;
        }
        schedule(activateThreadEvent[tid], when);
    }
}

void
CribCPU::activateThread(ThreadID tid)
{
    std::list<ThreadID>::iterator isActive =
        std::find(activeThreads.begin(), activeThreads.end(), tid);

    DPRINTF(CribCPU, "[tid:%i]: Calling activate thread.\n", tid);

    if (isActive == activeThreads.end()) {
        DPRINTF(CribCPU, "[tid:%i]: Adding to active threads list.\n", tid);
        activeThreads.push_back(tid);
    }
}

/** DeallocateContextEvent */
CribCPU::DeallocateContextEvent::DeallocateContextEvent()
    : Event(CPU_Tick_Pri), tid(0), remove(false), cpu(NULL)
{

}

void
CribCPU::DeallocateContextEvent::init(ThreadID _tid, CribCPU *_cpu)
{
    tid = _tid;
    cpu = _cpu;
    remove = false;
}

void
CribCPU::DeallocateContextEvent::process()
{
    cpu->deactivateThread(tid);
    if (remove) {
        cpu->removeThread(tid);
    }
}

void
CribCPU::DeallocateContextEvent::setRemove(bool _remove)
{
    remove = remove;
}

const char *
CribCPU::DeallocateContextEvent::description() const
{
    return "CribCPU \"Deallocate Context\"";
}

void
CribCPU::deactivateThread(ThreadID tid)
{
    std::list<ThreadID>::iterator thread_it =
        std::find(activeThreads.begin(), activeThreads.end(), tid);

    DPRINTF(CribCPU, "[tid:%i]: Calling deactivate thread.\n", tid);

    if (thread_it != activeThreads.end() ) {
        DPRINTF(CribCPU, "[tid:%i]: Removing from active threads list.\n");
        activeThreads.erase(thread_it);
    }
}

void
CribCPU::removeThread(ThreadID tid)
{
    DPRINTF(CribCPU, "[tid:%i]: Removing thread context from CPU.\n");
}

/** TickEvent */
CribCPU::TickEvent::TickEvent(CribCPU *_cpu)
    : Event(CPU_Tick_Pri), cpu(_cpu)
{

}

void
CribCPU::TickEvent::process()
{
    cpu->tick();
}

const char *
CribCPU::TickEvent::description() const
{
    return "CribCPU \"Tick\"";
}

void
CribCPU::scheduleTickEvent(Cycles delay)
{
    if (tickEvent.squashed()) {
        reschedule(tickEvent, clockEdge(delay));
    } else if (!tickEvent.scheduled()) {
        schedule(tickEvent, clockEdge(delay));
    }
}


/** ICachePort */
CribCPU::ICachePort::ICachePort(CribFetch *_fetch, CribCPU *_cpu)
    : MasterPort(_cpu->name() + ".icache_port", _cpu), fetch(_fetch)
{

}


bool
CribCPU::ICachePort::recvTimingResp(PacketPtr pkt)
{
    DPRINTF(CribCPU, "Icache response received.\n");
    assert(!(pkt->memInhibitAsserted() && !pkt->sharedAsserted()));
    fetch->processCacheCompletion(pkt);

    return true;
}

void
CribCPU::ICachePort::recvTimingSnoopReq(PacketPtr pkt)
{
    // Fetch does nothing.
}

void
CribCPU::ICachePort::recvRetry()
{
    fetch->recvRetry();
}



/** DCachePort */
CribCPU::DCachePort::DCachePort(CribLSQ *_lsq, CribCPU *_cpu)
    : MasterPort(_cpu->name() + ".dcache_port", _cpu), lsq(_lsq)
{

}

bool
CribCPU::DCachePort::recvTimingResp(PacketPtr pkt)
{
    return lsq->recvTimingResp(pkt);
}

void
CribCPU::DCachePort::recvTimingSnoopReq(PacketPtr pkt)
{
    //panic("Unimplemented function!");
}

void
CribCPU::DCachePort::recvRetry()
{
    //panic("Unimplemented function!");
}


CribCPU::~CribCPU()
{

}


void
CribCPU::activateStage(const StageIdx idx)
{
    activityRec.activateStage(idx);
}

void
CribCPU::deactivateStage(const StageIdx idx)
{
    activityRec.deactivateStage(idx);
}

void
CribCPU::instDone(const CribDynInstPtr &inst, ThreadID tid)
{
    if (!inst->isMicroop() || inst->isLastMicroop()) {
        thread[tid]->numInst++;
        thread[tid]->numInsts++;
        committedInsts[tid]++;
    }
    thread[tid]->numOp++;
    thread[tid]->numOps++;
    committedOps[tid]++;

    system->totalNumInsts++;
    comInstEventQueue[tid]->serviceEvents(thread[tid]->numInst);
    system->instEventQueue.serviceEvents(system->totalNumInsts);
}
