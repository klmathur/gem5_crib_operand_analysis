#include "cpu/crib/cpu.hh"
#include "cpu/crib/decode.hh"
#include "debug/Activity.hh"
#include "debug/CribDecode.hh"

CribDecode::CribDecode(CribCPU *_cpu, CribCPUParams *params)
    : cpu(_cpu),
      decodeWidth(params->decodeWidth),
      numThreads(params->numThreads),
      fetchToDecodeDelay(params->fetchToDecodeDelay),
      dispatchToDecodeDelay(params->dispatchToFetchDelay),
      cribToDecodeDelay(params->cribToDecodeDelay),
      commitToDecodeDelay(params->commitToDecodeDelay),
      skidBufferMax((fetchToDecodeDelay + 1) * params->fetchWidth)
{

}

inline std::string
CribDecode::name() const
{
    return cpu->name() + ".decode";
}

void
CribDecode::regStats()
{
    decodeIdleCycles
        .name(name() + ".IdleCycles")
        .desc("Number of cycles decode is idle")
        .prereq(decodeIdleCycles);

    decodeBlockedCycles
        .name(name() + ".BlockedCycles")
        .desc("Number of cycles decode is blocked")
        .prereq(decodeBlockedCycles);

    decodeRunCycles
        .name(name() + ".RunCycles")
        .desc("Number of cycles decode is running")
        .prereq(decodeRunCycles);

    decodeUnblockCycles
        .name(name() + ".UnblockCycles")
        .desc("Number of cycles decode is unblocking")
        .prereq(decodeUnblockCycles);

    decodeSquashCycles
        .name(name() + ".SquashCycles")
        .desc("Number of cycles decode is squashing")
        .prereq(decodeSquashCycles);

    decodeBranchResolved
        .name(name() + ".BranchResolved")
        .desc("Number of times decode resolved a branch")
        .prereq(decodeBranchResolved);

    decodeBranchMispredict
        .name(name() + ".BranchMispredict")
        .desc("Number of times decode detected a branch misprediction")
        .prereq(decodeBranchMispredict);

    decodeControlMispredict
        .name(name() + ".ControlMispredict")
        .desc("Number of times decode detected an instruction incorrectly "
              "predicted as control")
        .prereq(decodeControlMispredict);

    decodeDecodedInsts
        .name(name() + ".DecodedInsts")
        .desc("Number of instructions handled by decode")
        .prereq(decodeDecodedInsts);
}

void
CribDecode::setTimeBuffer(TimeBuffer<TimeStruct> *time_buffer)
{
    // Setup wire to communicate information back to fetch
    toFetch = time_buffer->getWire(0);

    // Create wires to get info from proper places
    fromDispatch = time_buffer->getWire(-dispatchToDecodeDelay);
    fromCrib = time_buffer->getWire(-cribToDecodeDelay);
    fromCommit = time_buffer->getWire(-commitToDecodeDelay);
}

void
CribDecode::setDecodeQueue(TimeBuffer<DecodeStruct> *decode_queue)
{
    toDispatch = decode_queue->getWire(0);
}
void
CribDecode::setFetchQueue(TimeBuffer<FetchStruct> *fetch_queue)
{
    fromFetch = fetch_queue->getWire(-fetchToDecodeDelay);
}

void
CribDecode::setActiveThreads(std::list<ThreadID> *active_threads)
{
    activeThreads = active_threads;
}


void
CribDecode::startupStage()
{
    resetStage();
}

void
CribDecode::takeOverFrom()
{
    resetStage();
}


bool
CribDecode::isDrained() const
{
    for (ThreadID i = 0; i < numThreads; i++) {
        if (!decodeState[i].insts.empty() ||
            !decodeState[i].skidBuffer.empty()) {
            return false;
        }
    }
    return true;
}

void
CribDecode::tick()
{
    bool status_change = false;

    wroteToTimeBuffer = false;
    decodeIndex = 0;

    std::list<ThreadID>::iterator thread_itr = activeThreads->begin();
    std::list<ThreadID>::iterator thread_end = activeThreads->end();

    sortInstructions();

    while (thread_itr != thread_end) {
        ThreadID tid = *thread_itr++;

        DPRINTF(CribDecode, "[tid:%i]: Processing.\n", tid);
        status_change = checkSignalsAndUpdate(tid) || status_change;

        decode(status_change, tid);
    }

    if (status_change) {
        updateStatus();
    }

    if (wroteToTimeBuffer) {
        cpu->recordActivity();
    }
}

inline void
CribDecode::resetStage()
{
    status = Inactive;

    // Reset all state
    for (ThreadID tid = 0; tid < numThreads; tid++) {
        decodeState[tid].status = Idle;
        decodeState[tid].stalls.dispatch = false;
        decodeState[tid].stalls.crib = false;
        decodeState[tid].stalls.commit = false;
        decodeState[tid].ackSquashSeqNum = 0; 
        decodeState[tid].ackPcAddr = 0; 

        std::queue<CribDynInstPtr>().swap(decodeState[tid].insts);
        std::queue<CribDynInstPtr>().swap(decodeState[tid].skidBuffer);
    }

    wroteToTimeBuffer = false;
}

inline void
CribDecode::sortInstructions()
{
    int numInsts = fromFetch->size;
    DPRINTF(CribDecode, "Receiving %i instructions from fetch.\n", numInsts);
    for (int i=0; i < numInsts; i++) {
        unsigned tid = fromFetch->insts[i]->tcBase()->threadId();
        if (decodeState[tid].status != Squashing ||
            ((fromFetch->ackSquash[tid] && fromFetch->ackSquashSeqNum[tid] == decodeState[tid].ackSquashSeqNum) &&
             (fromFetch->insts[i]->pcState().instAddr() == decodeState[tid].ackPcAddr))) {
            decodeState[tid].insts.push(fromFetch->insts[i]);
        }
    }
}

inline bool
CribDecode::checkSignalsAndUpdate(ThreadID tid)
{
    readStallSignals(tid);

    if (fromCommit->cribInfo[tid].squash) {
        DPRINTF(CribDecode, "[tid:%i]: Squashing due to squash from commit."
                "\n", tid);
        squash(tid);
        return true;
    }

    if (decodeState[tid].status != Squashing && checkStall(tid)) {
        return block(tid);
    }

    if (decodeState[tid].status == Blocked) {
        DPRINTF(CribDecode, "[tid:%i]: Done blocking, switching to "
                "unblocking.\n", tid);
        decodeState[tid].status = Unblocking;
        unblock(tid);
        return true;
    }

    if (decodeState[tid].status == Squashing &&
        fromFetch->ackSquashSeqNum[tid] == decodeState[tid].ackSquashSeqNum &&
        (fromFetch->ackPcAddr[tid] == decodeState[tid].ackPcAddr)) {
        DPRINTF(CribDecode, "[tid:%i]: Done squashing, switching to "
                "running. [sn:%lli, %lx].\n", tid, fromFetch->ackSquashSeqNum[tid],fromFetch->ackPcAddr[tid]);

        decodeState[tid].status = Running;
        return false;
    }

    return false;
}

inline void
CribDecode::readStallSignals(ThreadID tid)
{
    if (fromDispatch->dispatchBlock[tid]) {
        decodeState[tid].stalls.dispatch = true;
    }

    if (fromDispatch->dispatchUnblock[tid]) {
        assert(decodeState[tid].stalls.dispatch);
        decodeState[tid].stalls.dispatch = false;
    }

    if (fromCrib->cribBlock[tid]) {
        decodeState[tid].stalls.crib = true;
    }

    if (fromCrib->cribUnblock[tid]) {
        assert(decodeState[tid].stalls.crib);
        decodeState[tid].stalls.crib = false;
    }

    if (fromCommit->commitBlock[tid]) {
        decodeState[tid].stalls.commit = true;
    }

    if (fromCommit->commitUnblock[tid]) {
        assert(decodeState[tid].stalls.commit);
        decodeState[tid].stalls.commit = false;
    }
}

inline bool
CribDecode::checkStall(ThreadID tid)
{
    bool ret_val = false;
    /*
    if(decodeState[tid].stalls.dispatch) {
        DPRINTF(CribDecode, "[tid:%i]: Stall from Dispatch detected.\n", tid);
        ret_val = true;
    } else 
    */if (decodeState[tid].stalls.crib) {
        DPRINTF(CribDecode, "[tid:%i]: Stall from Crib detected.\n", tid);
        ret_val = true;
    } else if (decodeState[tid].stalls.commit) {
        DPRINTF(CribDecode, "[tid:%i]: Stall from Commit detected.\n", tid);
        ret_val = true;
    }

    return ret_val;
}

inline bool
CribDecode::block(ThreadID tid)
{
    DPRINTF(CribDecode, "[tid:%i]: Blocking.\n", tid);

    // Add the current inputs to the skid buffer so they can be reprocessed
    // when this stage unblocks.
    skidInsert(tid);

    // If the decode status is blocked or unblocking then decode has not yet
    // signalled fetch to unblock.   In that case, there is no need to tell
    // fetch to block
    if (decodeState[tid].status != Blocked) {
        decodeState[tid].status = Blocked;
        toFetch->decodeBlock[tid] = true;
        wroteToTimeBuffer = true;
        return true;
    }

    return false;
}

inline bool
CribDecode::unblock(ThreadID tid)
{
    if (decodeState[tid].skidBuffer.empty()) {
        DPRINTF(CribDecode, "[tid:%i]: Done unblocking.\n", tid);
        toFetch->decodeUnblock[tid] = true;
        wroteToTimeBuffer = true;
        decodeState[tid].status = Running;
        return true;
    } else {
        DPRINTF(CribDecode, "[tid:%i]: Currently unblocking.\n", tid);
        return false;
    }
}

inline void
CribDecode::skidInsert(ThreadID tid)
{
    CribDynInstPtr inst = NULL;

    while (!decodeState[tid].insts.empty()) {
        inst = decodeState[tid].insts.front();
        decodeState[tid].insts.pop();

        assert(tid == inst->tcBase()->threadId());

        DPRINTF(CribDecode, "[tid:%i]: Inserting [sn:%lli][pc:%s] into "
                "decode skidbuffer.\n", tid, inst->seqNum, inst->pcState());

        decodeState[tid].skidBuffer.push(inst);
        assert(decodeState[tid].skidBuffer.size() <= skidBufferMax);
    }
}

inline void
CribDecode::squash(ThreadID tid)
{
    DPRINTF(CribDecode, "[tid:%i]: Squashing.\n", tid);

    if (decodeState[tid].status == Blocked ||
        decodeState[tid].status == Unblocking) {
        if (FullSystem) {
            toFetch->decodeUnblock[tid] = true;
        } else {
            // In syscall emulation, we can have both a block and a squash due
            // to a syscall in the same cycle.  This would cause both signals
            // to be high.  This shouldn't happen in full system.
            // TODO: Determine if this still happens.
            if (toFetch->decodeBlock[tid]) {
                toFetch->decodeBlock[tid] = false;
            } else {
                toFetch->decodeUnblock[tid] = true;
            }
        }
    }

    decodeState[tid].status = Squashing;
    decodeState[tid].ackSquashSeqNum = fromCommit->cribInfo[tid].doneSeqNum;
    decodeState[tid].ackPcAddr = fromCommit->cribInfo[tid].pc.instAddr(); 
    toDispatch->ackSquash[tid] = true;
    toDispatch->ackSquashSeqNum[tid] = decodeState[tid].ackSquashSeqNum;
    toDispatch->ackPcAddr[tid] = decodeState[tid].ackPcAddr;

    // Clear the inst queue and skid buffer
    std::queue<CribDynInstPtr>().swap(decodeState[tid].insts);
    std::queue<CribDynInstPtr>().swap(decodeState[tid].skidBuffer);
}

inline void
CribDecode::squash(const CribDynInstPtr &inst, ThreadID tid)
{
    DPRINTF(CribDecode, "[tid:%i]: [sn:%lli] Squashing due to incorrect "
            "branch prediction detected at decode.\n", tid, inst->seqNum);

    // Send back misprediction information
    toFetch->decodeInfo[tid].squash = true;
    toFetch->decodeInfo[tid].squashInst = inst;
    toFetch->decodeInfo[tid].doneSeqNum = inst->seqNum;
    toFetch->decodeInfo[tid].nextPC = inst->branchTarget();
    toFetch->decodeInfo[tid].branchTaken = inst->pcState().branching()
                                        || inst->isUncondCtrl();

    if (decodeState[tid].status == Blocked ||
        decodeState[tid].status == Unblocking) {
        toFetch->decodeUnblock[tid] = true;
    }

    decodeState[tid].status = Squashing;
    decodeState[tid].ackSquashSeqNum = inst->seqNum;
    decodeState[tid].ackPcAddr = inst->branchTarget().instAddr(); 

    // Clear the instruction buffers
    std::queue<CribDynInstPtr>().swap(decodeState[tid].insts);
    std::queue<CribDynInstPtr>().swap(decodeState[tid].skidBuffer);
}

inline void
CribDecode::decode(bool &status_change, ThreadID tid)
{
    // If status is Running or idle,
    //     call decodeInstructions()
    // If status is Unblocking,
    //     buffer any instructions coming from fetch
    //     continue trying to empty skid buffer
    //     check if stall conditions have passed

    if (decodeState[tid].status == Running ||
        decodeState[tid].status == Idle) {

        DPRINTF(CribDecode, "[tid:%i]: Not blocked, attempting to run.\n",
                tid);
        decodeInstructions(tid);
    } else if (decodeState[tid].status == Unblocking) {
        decodeInstructions(tid);

        if (fromFetch->size  > 0) {
            skidInsert(tid);
        }

        status_change = (decodeState[tid].status == Squashing || unblock(tid)) || status_change;
    } else {
        DPRINTF(CribDecode, "[tid:%i]: Decode not Running, Idle, or "
                "Unblocking.\n", tid);
    }
}

inline void
CribDecode::decodeInstructions(ThreadID tid)
{
    int insts_available = decodeState[tid].status == Unblocking ?
        decodeState[tid].skidBuffer.size() : decodeState[tid].insts.size();

    if (insts_available == 0) {
        DPRINTF(CribDecode, "[tid:%i]: No instructions to decode.\n", tid);
        ++decodeIdleCycles;
        return;
    } else if (decodeState[tid].status == Unblocking) {
        DPRINTF(CribDecode, "[tid:%i]: Unblocking, using skid buffer.\n", tid);
        ++decodeUnblockCycles;
    } else if (decodeState[tid].status == Running) {
        ++decodeRunCycles;
    }

    CribDynInstPtr inst;

    std::queue<CribDynInstPtr> &insts_to_decode =
        decodeState[tid].status == Unblocking ?
            decodeState[tid].skidBuffer : decodeState[tid].insts;

    DPRINTF(CribDecode, "[tid:%i]: Sending instructions to dispatch.\n", tid);

    while (insts_available > 0 && decodeIndex < decodeWidth) {
        assert(!insts_to_decode.empty());
        inst = insts_to_decode.front();
        insts_to_decode.pop();

        DPRINTF(CribDecode, "[tid:%i]: Decoding instruction [sn:%lli][pc:%s]"
                ".\n", tid, inst->seqNum, inst->pcState());


        ++decodeDecodedInsts;
        --insts_available;
        
        /*
        if (inst->isNop()) {
            DPRINTF(CribDecode, "[tid:%i]: Decode discarding nop "
                    "[sn:%lli][pc:%s]\n", tid, inst->seqNum, inst->pcState());
            wroteToTimeBuffer = true;
            continue;
        }
        */

        toDispatch->insts[decodeIndex] = inst;

        ++(toDispatch->size);
        ++decodeIndex;
#if TRACING_ON
        inst->decodeTick = curTick();
#endif

        // Ensure that if it was a predicted branch, it really is a branch
        // TODO: This code is not exercised because fetch is cheating and
        // only predicts control instructions
        if (inst->readPredTaken() && !inst->isControl()) {
            panic("Instruction predicted as a branch!");

            ++decodeControlMispredict;
            squash(inst, inst->tcBase()->threadId());
            break;
        }

        // Compute pc-relative branches
        if (inst->isDirectCtrl() && inst->isUncondCtrl()) {
            ++decodeBranchResolved;

            if (!(inst->branchTarget() == inst->readPredTarg()) && !inst->isLoopEnd()) {
                ++decodeBranchMispredict;

                squash(inst, inst->tcBase()->threadId());
                TheISA::PCState target = inst->branchTarget();

                DPRINTF(CribDecode, "[tid:%i]: Updating [sn:%lli][pc:%s] "
                        "target to %s.\n", tid, inst->seqNum, inst->pcState(),
                        target);

                if (inst->hasFallthrough())  {
                    inst->setLoopFail();
                }
                inst->setPredTarg(target);
                break;
            }
        }
    }

    // If we didn't process all instructions, then we need to block and put
    // all those instructions in the skid buffer.
    if (!insts_to_decode.empty()) {
        block(tid);
    }

    // Record that decode has written to the timebuffer
    if (decodeIndex) {
        wroteToTimeBuffer = true;
    }
}

inline void
CribDecode::updateStatus()
{
    bool any_unblocking = false;
    std::list<ThreadID>::iterator thread_itr = activeThreads->begin();
    std::list<ThreadID>::iterator thread_end = activeThreads->end();

    while (thread_itr != thread_end) {
        ThreadID tid = *thread_itr++;

        if (decodeState[tid].status == Unblocking) {
            any_unblocking = true;
            break;
        }
    }

    if (any_unblocking) {
        if (status == Inactive) {
            status = Active;
            DPRINTF(Activity, "Activating stage.\n");
            cpu->activateStage(CribCPU::DecodeIdx);
        }
    } else {
        if (status == Active) {
            status = Inactive;
            DPRINTF(Activity, "Deactivating stage.\n");
            cpu->deactivateStage(CribCPU::DecodeIdx);
        }
    }
}
