#ifndef __CPU_CRIB_THREAD_CONTEXT_HH__
#define __CPU_CRIB_THREAD_CONTEXT_HH__

#include "config/the_isa.hh"
#include "cpu/crib/cpu.hh"
#include "cpu/crib/thread_state.hh"
#include "cpu/thread_context.hh"

class CribThreadContext : public ThreadContext
{
  public:
    CribThreadContext(CribCPU *_cpu, CribThreadState *_thread);

    /** Returns a pointer to the ITB */
    TheISA::TLB *getITBPtr() { return cpu->getITBPtr(); }

    /** Returns a pointer to the DTB. */
    TheISA::TLB *getDTBPtr() { return cpu->getDTBPtr(); }

    CheckerCPU *getCheckerCpuPtr() { return NULL; }

    TheISA::Decoder *
    getDecoderPtr()
    {
      return cpu->getDecoderPtr(thread->threadId());
    }

    /** Returns a pointer to this CPU. */
    BaseCPU *getCpuPtr() { return cpu; }

    /** Reads this CPU's ID. */
    int cpuId() { return cpu->cpuId(); }

    /** Get/Set this threads context id. */
    int contextId() { return thread->contextId(); }
    void setContextId(int id) { return thread->setContextId(id); }

    /** Get/Set this threads thread id. */
    int threadId() { return thread->threadId(); }
    void setThreadId(int id) { return thread->setThreadId(id); }

    /** Returns a pointer to the system. */
    System* getSystemPtr() { return cpu->system; }

    /** Returns a pointer to this thread's kernel statistics. */
    TheISA::Kernel::Statistics *getKernelStats() { return thread->kernelStats; }

    /** Returns a pointer to this thread's process */
    Process *getProcessPtr() { return thread->getProcessPtr(); }

    /** External ports and memories. */
    PortProxy &getPhysProxy() { return thread->getPhysProxy(); }
    FSTranslatingPortProxy &getVirtProxy() { return thread->getVirtProxy(); }
    void initMemProxies(ThreadContext *tc) { thread->initMemProxies(tc); }
    SETranslatingPortProxy &getMemProxy() { return thread->getMemProxy(); }

    /** Get/Set this thread's status. */
    Status status() const { return thread->status(); }
    void setStatus(Status new_status) { thread->setStatus(new_status); }

    /** Set the status to Active.  Optional delay indicates the number of
     * cycles to wait before beginning execution. */
    void activate(Cycles delay = Cycles(1));

    /** Set the status to Suspended. */
    void suspend(Cycles delay = Cycles(0));

    /** Set the status to Halted. */
    void halt(Cycles delay = Cycles(0));

    /** Dumps the function profiling information. */
    void dumpFuncProfile();

    /** Takes over execution of a thread from another CPU. */
    void takeOverFrom(ThreadContext *old_context);

    /** Registers statistics associated with this TC. */
    void regStats(const std::string &name);

    /** Serlializes state. */
    void serialize(std::ostream &os);
    /** Unserializes state. */
    void unserialize(Checkpoint *cp, const std::string &section);

    /** Returns a pointer ot the quiesce event. */
    EndQuiesceEvent *getQuiesceEvent() { return thread->quiesceEvent; }

    /** Reads the last tick this thread was activated on. */
    Tick readLastActivate();

    /** Reads the last tick this thread was suspended on. */
    Tick readLastSuspend();

    /** Clears the function profiling information. */
    void profileClear();

    /** Samples the function profiling information. */
    void profileSample();

    /** Copies the architectural registers from another TC into this TC. */
    void copyArchRegs(ThreadContext * tc);

    /** Resets all architectural registers to 0. */
    void clearArchRegs();

    /** Register accessors. */
    IntReg readIntReg(int reg_idx);
    FloatReg readFloatReg(int reg_idx);
    FloatRegBits readFloatRegBits(int reg_idx);
    void setIntReg(int reg_idx, IntReg val);
    void setFloatReg(int reg_idx, FloatReg val);
    void setFloatRegBits(int reg_idx, FloatRegBits val);

    /** Flattened register accessors. */
    IntReg readIntRegFlat(int reg_idx);
    FloatReg readFloatRegFlat(int reg_idx);
    FloatRegBits readFloatRegBitsFlat(int reg_idx);
    void setIntRegFlat(int reg_idx, IntReg val);
    void setFloatRegFlat(int reg_idx, FloatReg val);
    void setFloatRegBitsFlat(int reg_idx, FloatRegBits val);

    /** Reads this thread's PC state. */
    TheISA::PCState pcState();

    /** Sets this thread's PC state. */
    void pcState(const TheISA::PCState &val);
    void pcStateNoRecord(const TheISA::PCState &val);

    /** Reads this thread's PC. */
    Addr instAddr();

    /** Reads this thread's next PC. */
    Addr nextInstAddr();

    /** Reads this thread's micro PC. */
    MicroPC microPC() { return cpu->microPC(thread->threadId()); }

    /** Reads a miscellaneous register. */
    MiscReg readMiscRegNoEffect(int misc_reg)
    { return cpu->readMiscRegNoEffect(misc_reg, thread->threadId()); }

    /** Reads a misc. register, including any side-effects the
     * read might have as defined by the architecture. */
    MiscReg readMiscReg(int misc_reg)
    { return cpu->readMiscReg(misc_reg, thread->threadId()); }

    /** Setas a misc. register. */
    void setMiscRegNoEffect(int misc_reg, const MiscReg& val);

    /** Sets a misc register, inclueding any side-effects the
     * write might have as defined by the architecture. */
    void setMiscReg(int misc_reg, const MiscReg& val);

    int flattenIntIndex(int reg);
    int flattenFloatIndex(int reg);

    /* Returns the number of consecutive store conditional failres. */
    unsigned readStCondFailures() { return thread->storeCondFailures; }

    /* Sets the number of consecutive store conditional failures. */
    void setStCondFailures(unsigned sc_failures)
    { thread->storeCondFailures = sc_failures; }

    // Only really makes sense for old CPU model.  Lots of code
    // outside the CPU still checks this function, so it will
    // always return false to keep everything working.
    /** Checks if the thread is misspeculating.  Because it is
     * very difficult to determine if the thread is
     * misspeculating, this is set as false. */
    bool misspeculating() { return false; }

    /** Reads the funcExeInst counter. */
    Counter readFuncExeInst() { return thread->funcExeInst; }

    /** Executes a syscall in SE mode. */
    void syscall(int64_t callnum)
    { thread->syscall(callnum); }

    int exit();

  private:
    /** Pointer to the CPU. */
    CribCPU *cpu;

    /** Pointer to the thread state that this TC corresponds to. */
    CribThreadState *thread;

    void conditionalSquash() {
        if (!thread->noSquashFromTC) {
            cpu->squashFromTC(thread->threadId());
        }
    }
};

#endif // __CPU_CRIB_THREAD_CONTEXT_HH__
