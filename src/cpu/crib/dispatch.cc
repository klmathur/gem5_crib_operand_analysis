#include "cpu/crib/cpu.hh"
#include "cpu/crib/dispatch.hh"
#include "debug/Activity.hh"
#include "debug/CribDispatch.hh"
#include "debug/CribLoop.hh"

CribDispatch::CribDispatch(CribCPU *_cpu, CribCPUParams *params)
    : cpu(_cpu),
      partitionSize(params->cribPartitionSize),
      loadsPerPartition(params->cribLoadsPerPartition),
      storesPerPartition(params->cribStoresPerPartition),
      cribPartitions(params->cribPartitions),
      cribToDispatchDelay(params->cribToDispatchDelay),
      commitToDispatchDelay(params->commitToDispatchDelay),
      decodeToDispatchDelay(params->decodeToDispatchDelay),
      ackSquashSeqNum(0),
      ackPcAddr(0),
      inLoopDispatch(false),
      loopPartitions(0),
      serializing(false),
      serializeAfter(false),
      serializeSeqNum(0)
{
    if (params->numThreads != 1) {
        panic("CribDispatch does not support multithreading.");
    }

    status = Inactive;
    threadStatus = Idle;

    stalls.crib = false;
    stalls.commit = false;

    wroteToTimeBuffer = false;
    cribCommitWidth = params->cribCommitWidth;
}

inline std::string
CribDispatch::name() const
{
    return cpu->name() + ".dispatch";
}

void
CribDispatch::regStats()
{
    dispatchIdleCycles
        .name(name() + ".IdleCycles")
        .desc("Number of cycles dispatch spent idle")
        .prereq(dispatchIdleCycles);

    dispatchBlockedCycles
        .name(name() + ".BlockedCycles")
        .desc("Number of cycles dispatch spent blocked")
        .prereq(dispatchBlockedCycles);

    dispatchUnblockCycles
        .name(name() + ".UnblockCycles")
        .desc("Number of cycles dispatch spent unblocking")
        .prereq(dispatchUnblockCycles);

    dispatchSquashCycles
        .name(name() + ".SquashCycles")
        .desc("Number of cycles dispatch spent squashing")
        .prereq(dispatchSquashCycles);

    dispatchRunCycles
        .name(name() + ".RunCycles")
        .desc("Number of cycles dispatch spent running")
        .prereq(dispatchRunCycles);

    dispatchInstDist
        .init(0, partitionSize, 1)
        .name(name() + ".instDist")
        .desc("Number of instructions dispatched each cycle (Total)")
        .flags(Stats::pdf);

    dispatchLoopSuccesses
        .name(name() + ".loopSuccesses")
        .desc("Number of loop dispatch successes")
        .prereq(dispatchLoopSuccesses);

    dispatchLoopFailures
        .name(name() + ".loopFailures")
        .desc("Total number of loop dispatch failures")
        .prereq(dispatchLoopFailures);

    dispatchLoopSizeFailures
        .name(name() + ".loopSizeFailures")
        .desc("Number of loop dispatch failures due to crib size")
        .prereq(dispatchLoopSizeFailures);

    dispatchLoopSuccessRate
        .name(name() + ".loopSuccessRate")
        .desc("Percentage of loops sucessfully dispatched")
        .prereq(dispatchLoopSuccessRate);

    dispatchLoopSuccessRate = dispatchLoopSuccesses / (dispatchLoopSuccesses + dispatchLoopFailures);
}

void
CribDispatch::setTimeBuffer(TimeBuffer<TimeStruct> *time_buffer)
{
    // Setup backwards communication
    toDecode = time_buffer->getWire(0);

    // Create wires to get info from proper places
    fromCrib = time_buffer->getWire(-cribToDispatchDelay);
    fromCommit = time_buffer->getWire(-commitToDispatchDelay);
}

void
CribDispatch::setDecodeQueue(TimeBuffer<DecodeStruct> *decode_queue)
{
    fromDecode = decode_queue->getWire(-decodeToDispatchDelay);
}

void
CribDispatch::setDispatchQueue(TimeBuffer<DispatchStruct> *dispatch_queue)
{
    toCrib = dispatch_queue->getWire(0);
}


bool
CribDispatch::isDrained() const
{
    return (insts.empty() && skidBuffer.empty());
}

void
CribDispatch::takeOverFrom()
{
    status = Inactive;
    threadStatus = Idle;

    stalls.crib = false;
    stalls.commit = false;

    wroteToTimeBuffer = false;

    std::queue<CribDynInstPtr>().swap(insts);
    std::queue<CribDynInstPtr>().swap(skidBuffer);
}

void
CribDispatch::tick()
{
    bool status_change = false;
    wroteToTimeBuffer = false;

    sortInstructions();

    status_change = checkSignalsAndUpdate();

    dispatch(status_change);

    if (status_change) {
        updateStatus();
    }

    if (wroteToTimeBuffer) {
        DPRINTF(Activity, "Activity this cycle.\n");
        cpu->recordActivity();
    }
}


inline void
CribDispatch::dispatch(bool &status_change)
{
    if (threadStatus == Blocked) {
        ++dispatchBlockedCycles;
    } else if(threadStatus == Squashing) {
        ++dispatchSquashCycles;
    }

    if (threadStatus == Running ||
        threadStatus == Idle) {

        DPRINTF(CribDispatch, "[tid:%i]: Not blocked, so attempting to run.\n",
                0);

        dispatchInstructions();
    } else if (threadStatus == Unblocking) {
        dispatchInstructions();

        if (!insts.empty()) {
           skidInsert();
        }

        status_change = unblock() || status_change;
    }
}

inline bool
CribDispatch::checkSignalsAndUpdate()
{
    readStallSignals();

    if (fromCommit->cribInfo[0].squash) {
        DPRINTF(CribDispatch, "[tid:%i]: Squashing due to squash from commit."
                "\n", 0);
        squash();
        return true;
    }

    if (checkStall()) {
        return block();
    }

    if (threadStatus == Blocked && !(serializing &&
                fromCommit->cribInfo[0].doneSeqNum != serializeSeqNum)) {
        DPRINTF(CribDispatch, "[tid:%i]: Done blocking, switching to "
                "unblocking.\n", 0);

        serializing = false;
        threadStatus = Unblocking;
        return false;
    }

    if (threadStatus == Squashing &&
        fromDecode->ackSquash[0] &&
        (fromDecode->ackSquashSeqNum[0]==ackSquashSeqNum) &&
        (fromDecode->ackPcAddr[0] == ackPcAddr)) {
        DPRINTF(CribDispatch, "[tid:%i]: Done blocking, squashing to "
                "running.\n", 0);
        threadStatus = Running;
        return false;
    }

    return false;
}

inline void
CribDispatch::readStallSignals()
{
    if (fromCrib->cribBlock[0]) {
        stalls.crib = true;
    }
    if (fromCrib->cribUnblock[0]) {
        assert(stalls.crib);
        stalls.crib = false;
    }

    if (fromCommit->commitBlock[0]) {
        stalls.commit = true;
    }
    if (fromCommit->commitUnblock[0]) {
        assert(stalls.commit);
        stalls.commit = false;
    }
}

inline void
CribDispatch::squash()
{
    DPRINTF(CribDispatch, "[tid:%i]: Squashing.\n", 0);

    inLoopDispatch = false;
    serializing = false;
    serializeAfter = false;
    serializeSeqNum = 0;

    if (threadStatus == Blocked ||
        threadStatus == Unblocking ) {

        toDecode->dispatchBlock[0] = false;
        toDecode->dispatchUnblock[0] = true;
    }

    threadStatus = Squashing;
    ackSquashSeqNum = fromCommit->cribInfo[0].doneSeqNum;
    ackPcAddr = fromCommit->cribInfo[0].pc.instAddr();

    toCrib->ackSquash[0] = true;
    toCrib->ackSquashSeqNum[0] = ackSquashSeqNum;
    toCrib->ackPcAddr[0] = ackPcAddr;

    // Clear all buffers
    std::queue<CribDynInstPtr>().swap(insts);
    std::queue<CribDynInstPtr>().swap(skidBuffer);
}

inline bool
CribDispatch::checkStall()
{
    bool ret_val = false;

    if (stalls.crib) {
        DPRINTF(CribDispatch, "[tid:%i]: Stall from Crib detected.\n", 0);
        ret_val = true;
    } else if (stalls.commit) {
        DPRINTF(CribDispatch, "[tid:%i]: Stall from Commit stage detected.\n",
                0);
        ret_val = true;
    }

    return ret_val;
}

inline bool
CribDispatch::block()
{
    DPRINTF(CribDispatch, "[tid:%i]: Blocking.\n", 0);

    skidInsert();

    if (threadStatus != Blocked) {
        threadStatus = Blocked;
        toDecode->dispatchBlock[0] = true;
        wroteToTimeBuffer = true;
        return true;
    }
    return false;
}

inline void
CribDispatch::skidInsert()
{
    CribDynInstPtr inst = NULL;

    while (!insts.empty()) {
        inst = insts.front();
        insts.pop();

        DPRINTF(CribDispatch, "[tid:%i]: Inserting [sn:%lli][pc:%s] into "
                "dispatch skid buffer.\n", 0, inst->seqNum, inst->pcState());
        skidBuffer.push(inst);
    }
}

inline void
CribDispatch::sortInstructions()
{
    // Doesn't really sort since we're only supportin 1 thread for now
    int num_insts = fromDecode->size;
    for (int i = 0; i < num_insts; i++) {
        if (threadStatus != Squashing ||
            (fromDecode->ackSquash[0] && (fromDecode->ackSquashSeqNum[0] == ackSquashSeqNum) &&
            (fromDecode->ackPcAddr[0] == ackPcAddr))) {
            insts.push(fromDecode->insts[i]);

        }
    }
}

inline void
CribDispatch::dispatchInstructions()
{
    int num_insts = threadStatus == Unblocking ?
        skidBuffer.size() : insts.size();

    if (num_insts == 0) {
        DPRINTF(CribDispatch, "[tid:%i]: No instructions to dispatch.\n", 0);
        ++dispatchIdleCycles;
        return;
    } else if (threadStatus == Unblocking) {
        DPRINTF(CribDispatch, "[tid:%i]: Unblocking, removing instructions "
                "from skid buffer.\n", 0);
        ++dispatchUnblockCycles;
    } else if (threadStatus == Running) {
        ++dispatchRunCycles;
    }


    std::queue<CribDynInstPtr> &insts_to_dispatch = threadStatus == Unblocking ?
        skidBuffer : insts;

    DPRINTF(CribDispatch, "[tid:%i]: Sending instructions to Crib.\n", 0);

    unsigned insts_dispatched = 0;
    unsigned loads_dispatched = 0;
    unsigned stores_dispatched = 0;
    unsigned partitions_dispatched = 0;
    toCrib->size = 0;
    while (num_insts > 0 && (insts_dispatched < partitionSize && partitions_dispatched < cribCommitWidth) && !serializing) {
        CribDynInstPtr inst = insts_to_dispatch.front();

        DPRINTF(CribDispatch, "[tid:%i]: Processing instruction "
                "[sn:%lli][pc:%s]%s.\n", 0, inst->seqNum, inst->pcState(),
                inst->staticInst->disassemble(inst->pcState().instAddr()).c_str());


        assert(!inst->staticInst->isWriteBarrier());
        assert(!inst->staticInst->isQuiesce());

        if (inst->isLoopStart()) {
            if (insts_dispatched != 0 || partitions_dispatched !=0) {
                DPRINTF(CribLoop, "[sn:%lli]: Stalling dispatch for loop dispatch.\n", inst->seqNum);
                break;
            } else {
                inLoopDispatch = true;
                loopPartitions = 0;
            }
        }

        if (inst->isLoad()) {
            if(loads_dispatched >= loadsPerPartition) break;
            else loads_dispatched++;
        }

        if (inst->isStore()) {
            if (stores_dispatched >= storesPerPartition) break;
            else stores_dispatched++;
        }

        if (inst->isNonSpeculative() && (insts_dispatched != 0 || partitions_dispatched !=0)) {
            DPRINTF(CribDispatch, "[sn:%lli]: Nonspeculative instruction "
                    "encountered, delaying dispatch.\n",
                    inst->seqNum);
            break;
        }

        // TODO: Temporarily disabling membarrier stalling, should this be handled after dispatch?
        if (inst->staticInst->isSerializing() /*|| inst->staticInst->isMemBarrier()*/) {
            DPRINTF(CribDispatch, "[sn:%lli]: Serializing instruction "
                    "encountered. [before:%i][after:%i] - %s\n",
                    inst->seqNum,
                    inst->staticInst->isSerializeBefore(),
                    inst->staticInst->isSerializeAfter(),
                    inst->staticInst->disassemble(inst->pcState().instAddr()).c_str());
            if ( (inst->staticInst->isSerializeBefore() /* || inst->staticInst->isMemBarrier()*/) &&
                    !fromCrib->cribInfo[0].cribEmpty) {
                DPRINTF(CribDispatch, "[sn:%lli]: Stalling on serialize.\n",
                        inst->seqNum);
                break;
            }
            if (inst->staticInst->isSerializeAfter()) {
                // Make dispatch block after we are issued
                serializing = true;
                serializeAfter = true;
                serializeSeqNum = inst->seqNum;
            }

        }

        if (inst->isSquashAfter()) {
            // Force dispatch to stall just like it does on serialize after
            serializing = true;
            serializeAfter = true;
            serializeSeqNum = inst->seqNum;
        }

        if (inLoopDispatch && inst->isLoopFail()) {
            dispatchLoopFailures++;
            inLoopDispatch = false;
        }

        if (!inLoopDispatch) {
            inst->loopEnd = false;
            inst->fallthrough = false;
        }

        toCrib->insts[partitions_dispatched*partitionSize + insts_dispatched] = inst;
        insts_to_dispatch.pop();

        ++(toCrib->size);
        --num_insts;
        ++insts_dispatched;
        if(insts_dispatched == partitionSize){
            partitions_dispatched++;
            insts_dispatched = 0;
        }
#if TRACING_ON
        inst->dispatchTick = curTick();
#endif

        if (inst->isLoopEnd()) break;
        if (inst->isNonSpeculative()) break;
    }

    dispatchInstDist.sample(partitions_dispatched*partitionSize + insts_dispatched);

    if (!insts_to_dispatch.empty() || serializing) {
        DPRINTF(CribDispatch, "Dispatch stalling. [loads:%i][stores:%i]"
                "[serializing:%i].\n", loads_dispatched, stores_dispatched,
                serializing);
        block();
    }

    if (partitions_dispatched || insts_dispatched) {
        if (inLoopDispatch) {
            DPRINTF(CribLoop, "Dispatching loop partition %i.\n", loopPartitions);
            ++loopPartitions;

            if (toCrib->insts[partitions_dispatched*partitionSize + insts_dispatched-1]->isLoopEnd()) {
                if (inLoopDispatch) {
                    dispatchLoopSuccesses++;
                    DPRINTF(CribLoop, "Dispatched loop sucessfully.\n");
                }
                inLoopDispatch = false;
            }

            if (loopPartitions == cribPartitions) {
                // Verify that this was the loop end
                if (!toCrib->insts[partitions_dispatched*partitionSize + insts_dispatched-1]->isLoopEnd()) {
                    dispatchLoopFailures++;
                    dispatchLoopSizeFailures++;

                    toDecode->dispatchInfo[0].loopSizeFail = true;
                    toDecode->dispatchInfo[0].loopAddr = toCrib->insts[partitions_dispatched*partitionSize + insts_dispatched-1]->getStartAddr();
                    toDecode->dispatchInfo[0].unrollFactor = toCrib->insts[partitions_dispatched*partitionSize + insts_dispatched-1]->getUnrollFactor();

                    DPRINTF(CribLoop, "[sn:%i]: Dispatch loop size failure.\n",
                            toCrib->insts[partitions_dispatched*partitionSize + insts_dispatched-1]->seqNum);
                    toCrib->insts[partitions_dispatched*partitionSize + insts_dispatched-1]->setLoopFail();
                    inLoopDispatch = false;
                }
            }

        }
        wroteToTimeBuffer = true;
    }
}

inline bool
CribDispatch::unblock()
{
    if (skidBuffer.empty()) {
        DPRINTF(CribDispatch, "[tid:%i]: Done unblocking.\n", 0);
        toDecode->dispatchUnblock[0] = true;
        wroteToTimeBuffer = true;
        threadStatus = Running;
        return true;
    } else {
        DPRINTF(CribDispatch, "[tid:%i]: Currently unblocking.\n", 0);
        return false;
    }
}

inline void
CribDispatch::updateStatus()
{
    // Note: if adding multithreading, check the version in decode
    if (threadStatus == Unblocking) {
        if (status == Inactive) {
            status = Active;
            DPRINTF(Activity, "Activating stage.\n");
            cpu->activateStage(CribCPU::DispatchIdx);
        }
    } else {
        if (status == Active) {
            status = Inactive;
            DPRINTF(Activity, "Deactivating stage.\n");
            cpu->deactivateStage(CribCPU::DispatchIdx);
        }
    }
}
