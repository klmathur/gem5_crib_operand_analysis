#ifndef __CPU_CRIB_THREAD_STATE_HH__
#define __CPU_CRIB_THREAD_STATE_HH__

#include "cpu/thread_context.hh"
#include "cpu/thread_state.hh"

class CribThreadState : public ThreadState
{
    typedef ThreadContext::Status Status;

  private:
    CribCPU * cpu;

  public:
    bool noSquashFromTC;
    bool trapPending;
    TheISA::PCState lastBranchPC;
    ThreadContext* tc;

    CribThreadState(CribCPU *_cpu, ThreadID _thread_num, Process *_process)
      : ThreadState(reinterpret_cast<BaseCPU*>(_cpu), _thread_num, _process),
        cpu(_cpu), noSquashFromTC(false), trapPending(false)
    { }

    void syscall(int64_t callnum) { process->syscall(callnum, tc); };
    void dumpFuncProfile();
    ThreadContext *getTC() { return tc; }

    void serialize(std::ostream &os)
    {
        ThreadState::serialize(os);
        ::serialize(*tc, os);
    }

    void unserialize(Checkpoint *cp, const std::string &section)
    {
        noSquashFromTC = true;
        ThreadState::unserialize(cp, section);
        ::unserialize(*tc, cp, section);
        noSquashFromTC = false;
    }

};

#endif  // __CPU_CRIB_THREAD_STATE_HH__
