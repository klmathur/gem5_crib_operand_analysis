
#include "cpu/crib/cpu.hh"
#include "cpu/crib/crib.hh"
#include "debug/CommitInst.hh"
#include "debug/Crib.hh"
#include "debug/CribCommit.hh"
#include "debug/CribExecute.hh"
#include "debug/CribLoop.hh"
#include "debug/CribSpecial.hh"
#include "debug/CribCommitRegs.hh"
#include "debug/CribPulse.hh"
#include "debug/Drain.hh"
#include "debug/klm.hh"
#include <iostream>
//#include <vector>

    unsigned mapOpToResource(unsigned op) {
        switch(op) {
            case IntAluOp: return INT_ALU_RESOURCE;
            case IntMultOp: return INT_MUL_DIV_RESOURCE;
            case IntDivOp: return INT_MUL_DIV_RESOURCE;

            case FloatAddOp: return FP_ALU_RESOURCE;
            case FloatCmpOp: return FP_ALU_RESOURCE;
            case FloatCvtOp: return FP_DIV_RESOURCE;
            case FloatMultOp: return FP_ALU_RESOURCE;
            case FloatDivOp: return FP_DIV_RESOURCE;
            case FloatSqrtOp: return FP_DIV_RESOURCE;

            case SimdAddOp:
            case SimdAddAccOp:
            case SimdAluOp:
            case SimdCmpOp:
            case SimdCvtOp:
            case SimdMiscOp:
            case SimdMultOp:
            case SimdMultAccOp:
            case SimdShiftOp:
            case SimdShiftAccOp:
            case SimdSqrtOp:
            case SimdFloatAddOp:
            case SimdFloatAluOp:
            case SimdFloatCmpOp:
            case SimdFloatCvtOp:
            case SimdFloatDivOp:
            case SimdFloatMiscOp:
            case SimdFloatMultOp:
            case SimdFloatMultAccOp:
            case SimdFloatSqrtOp:
                  return SIMD_RESOURCE;
            default:
                  return INT_ALU_RESOURCE;
        }

    }



Crib::Crib(CribCPU *_cpu, CribLSQ *_lsq, CribCPUParams *params)
    : cpu(_cpu), lsq(_lsq), numPartitions(params->cribPartitions),
      partitionSize(params->cribPartitionSize),
      partitions(new CribPartition[numPartitions]),
      propagateDistance(params->cribPropagateDistance),
      dispatchToCribDelay(params->dispatchToCribDelay),
      cribLoopEnable(params->cribLoopEnable),
      squashTC(false),
      architectedPartition(0),
      allocPartition(0),
      retirePartition(0),
      status(Idle),
      squashing(false),
      squashed(false),
      ackSquashSeqNum(0),
      ackPcAddr(0),
      loopPartitions(0),
      drainPending(false),
      intALU(params->cribIntALU),
      intMulDiv(params->cribMulDiv),
      simd(params->cribSIMD),
      fpALU(params->cribFPALU),
      fpDiv(params->cribFPDiv)
{
    for (unsigned i = 0; i < numPartitions; i++) {
        partitions[i].init(this, lsq, partitionSize, params, i);
    }

    partitions[0].architected = true;
    enableCommitCheck = false;
    lastCommitTick = curTick(); 
    lastCommitEntry = 0; 
    lastBranchNum = 0; 
    totalCommitUops = 0; 
    once = true; 
    mispredTick = curTick();
    mispredPart = -1;
    cribCommitWidth= params->cribCommitWidth;
    bpredFileName = params->bpredFileName;
    dumpBpred = params->dumpBpred;
    idealPred = params->idealPred;
    if(dumpBpred){
        bpredFile.open(bpredFileName.c_str());
    }
    printNextPc = false;
}

void
Crib::startupStage()
{

}

void
Crib::takeOverFrom()
{
    status = Idle;
    squashing = false;
    squashed = false;
}

void
Crib::drain()
{
    drainPending = true;
}

void
Crib::drainResume()
{
    drainPending = false;
}

bool
Crib::isDrained() const
{
    if (!(instBuffer.empty() && skidBuffer.empty() && empty())) {
        return false;
    }

        .desc("Number of issued load operations")
        .prereq(cribLoadOps);

    cribStoreOps
        .name(name() + ".storeOps")
        .desc("Number of issued store operations")
        .prereq(cribStoreOps);

    cribIntOps
        .name(name() + ".intOps")
        .desc("Number of issued int operations")
        .prereq(cribIntOps);

    cribMultOps
        .name(name() + ".multOps")
        .desc("Number of issued int operations")
        .prereq(cribMultOps);

    cribFPOps
        .name(name() + ".fpOps")
        .desc("Number of issued fp operations")
        .prereq(cribFPOps);

    cribComLoadOps
        .name(name() + ".comLoadOps")
        .desc("Number of committed load operations")
        .prereq(cribComLoadOps);

    cribComStoreOps
        .name(name() + ".comStoreOps")
        .desc("Number of committed store operations")
        .prereq(cribComStoreOps);

    cribComIntOps
        .name(name() + ".comIntOps")
        .desc("Number of committed int operations")
        .prereq(cribComIntOps);

    cribComFPOps
        .name(name() + ".comFPOps")
        .desc("Number of committed fp operations")
        .prereq(cribComFPOps);

    cribLoopMispredicted
        .name(name() + ".loopMispredicted")
        .desc("Number of mispredicted branches within loop partitions")
        .prereq(cribLoopMispredicted);

    cribLoopBreaks
        .name(name() + ".loopBreaks")
        .desc("Number of loop breaks handled")
        .prereq(cribLoopBreaks);

    cribLoopContinues
        .name(name() + ".loopContinues")
        .desc("Number of loop continues handled")
        .prereq(cribLoopContinues);

    totCommitInterval
        .name(name()+".totCommitInterval")
        .desc("Average time between commits.") 
        .prereq(totCommitInterval); 

    totCommitDistance
        .name(name()+".totCommitDistance")
        .desc("Average distance between commits.") 
        .prereq(totCommitDistance);

    nCommittedOps
        .name(name()+".nCommittedOps")
        .desc("total committed instructions")
        .prereq(nCommittedOps); 
}


void
Crib::setTimeBuffer(TimeBuffer<TimeStruct> *time_buffer)
{
    toDispatch = time_buffer->getWire(0);
}

void
Crib::setDispatchQueue(TimeBuffer<DispatchStruct> *dispatch_queue)
{
    fromDispatch = dispatch_queue->getWire(-dispatchToCribDelay);
}

void
Crib::tick()
{
    DPRINTF(Crib, "Running stage.\n");

    squashed = false;

    resources[INT_ALU_RESOURCE] = intALU;
    resources[INT_MUL_DIV_RESOURCE] = intMulDiv;
    resources[SIMD_RESOURCE] = simd;
    resources[FP_ALU_RESOURCE] = fpALU;
    resources[FP_DIV_RESOURCE] = fpDiv;
    
    if (squashTC) {
        doTCSquash();
        return;
    }

    // Parse incomming instructions
    dispatchInstructions();

    // Print some debugging info
#if TRACING_ON
    for (unsigned i = 0; i < numPartitions; i++) {
        partitions[i].print();
    }

    disassemble();
#endif

    for (unsigned i = 0; i < numPartitions; i++) {
        partitions[i].evaluate();
    }

    for (unsigned i = 0; i < propagateDistance; i++) {
        // Have every entry evaluate its inputs and decide its outputs
        for (unsigned j = 0; j < numPartitions; j++) {
            partitions[j].intraPropagate();
        }
        // Perform propagation within each partition
        for (unsigned j = 0; j < numPartitions; j++) {
            partitions[j].interPropagate();
        }
        // Perform propagation between partitions
        partitionPropagate();
    }

    // Branch detect
    removeSquashedPartitions();

    // Commit logic
    commitInstructions();
    resolveBranchMispredicts();

    // Blocking/stall/activity logic
    updateStatus();
}

inline void
Crib::partitionPropagate()
{
    for (unsigned i = 0; i < numPartitions; i++) {
        unsigned seq_num = (i+1) % numPartitions;
        CribPartition &cur_partition = partitions[i];
        CribPartition &seq_partition = partitions[seq_num];

        // Do propagate for the standard sequential case
        if (seq_num == architectedPartition || seq_num == retirePartition) {
            seq_partition.entries[0].inputs = new Propagated(seq_partition.latchRegs);
            if (seq_num == architectedPartition && !seq_partition.loopStart) {
                seq_partition.latchRexec |= cur_partition.entries[partitionSize-1].outputs->rexec;
            }
        } else {
            seq_partition.entries[0].inputs = cur_partition.entries[partitionSize-1].outputs;
        }

        // This is to guard feeding bad ready/values to a future loop iteration if the end hasn't been scheduled
        if (cribLoopEnable && seq_partition.loopStart && !seq_partition.architected && seq_partition.loopExecutions != 0) {
            seq_partition.entries[0].inputs->ready.reset();
            seq_partition.entries[0].inputs->rexec.reset();

        }
    }

    // Propagate from the loop end to the loop start
    if (cribLoopEnable) {
        for (unsigned i = 0; i < numPartitions; i++) {
            CribPartition &cur_partition = partitions[i];
            if (cur_partition.loopEnd) {
                unsigned loop_num = (i + numPartitions + 1 - cur_partition.loopNumPartitions) % numPartitions;
                CribPartition &loop_start = partitions[loop_num];
                if (loop_start.loopStart && loop_start.loopExecutions != 0) {
                    if (!loop_start.architected) {
                        delete loop_start.entries[0].inputs;
                        loop_start.entries[0].inputs =
                            new Propagated(cur_partition.entries[partitionSize-1].outputs->ready,
                                    cur_partition.entries[partitionSize-1].outputs->rexec,
                                    cur_partition.entries[partitionSize-1].outputs->regs);
                    } else {
                        loop_start.latchRexec |= cur_partition.entries[partitionSize-1].outputs->rexec;
                    }
                }
            }
        }
    }

    for (auto itr = pendingDelete.begin(); itr!=pendingDelete.end(); ++itr) {
        delete *itr;
    }
    pendingDelete.clear();

    for (unsigned i = 0; i < numPartitions; i++) {
        unsigned seq_num = (i+1) % numPartitions;
        CribPartition &cur_partition = partitions[i];
        if (seq_num == architectedPartition || seq_num == retirePartition) {
            pendingDelete.push_back(cur_partition.entries[partitionSize-1].outputs);
        }
    }

/*
    // Dont feed possibly changing/invalid values to instructions past the loop end
    if (cribLoopEnable) {
        for (unsigned i = 0; i < numPartitions; i++) {
            unsigned seq_num = (i+1) % numPartitions;
            CribPartition &cur_partition = partitions[i];
            CribPartition &seq_partition = partitions[seq_num];
            // Loop end clear valids
            if (cur_partition.loopEnd && !seq_partition.architected) {
                CribEntry& entry = cur_partition.entries[cur_partition.numInsts-1];
                if (!(entry.status == CribEntry::Status::Executed && !entry.inst->mispredicted())) {
                    seq_partition.entries[0].inputs->ready =
                        seq_partition.entries[0].inputs->ready  & ~entry.inst->loopWriteSet;
                }
            }
        }
    }
*/

}

bool
Crib::empty() const
{
    return (allocPartition == retirePartition) &&
           (allocPartition == architectedPartition) &&
            !partitions[allocPartition].isAllocated();
}

bool
Crib::full() const
{
    return partitions[allocPartition].isAllocated();
}


void
Crib::squashFromTC(ThreadID tid)
{
    squashTC = true;
}

inline void
Crib::dispatchInstructions()
{
    // Read whatever came from dispatch into the instruction queue
    int numInsts = fromDispatch->size;
    DPRINTF(Crib, "Receiving %i instructions from dispatch.\n", numInsts);

    if (squashing && fromDispatch->ackSquash[0] &&
            fromDispatch->ackSquashSeqNum[0] == ackSquashSeqNum) {
        squashing = false;
        DPRINTF(Crib, "Ending squash sequence [sn:%lli].\n", ackSquashSeqNum);
    }

    if (!squashing) {
        for (int i = 0; i < numInsts; i++) {
            instBuffer.push(fromDispatch->insts[i]);
        }
    }

    assert(skidBuffer.empty() || instBuffer.empty());

    if (skidBuffer.empty() && instBuffer.empty()) {
        return;
    }

    std::queue<CribDynInstPtr> &insts_to_dispatch = skidBuffer.empty() ?
        instBuffer : skidBuffer;

    if (skidBuffer.empty()) {
        DPRINTF(Crib, "Dispatching from input buffer.\n");
    } else {
        DPRINTF(Crib, "Dispatching from skid buffer.\n");
    }

    if (insts_to_dispatch.empty()) {
        DPRINTF(Crib, "No instructions to allocate.\n");
        return;
    }

    if (full()) {
        // Crib is currently full
        DPRINTF(Crib, "All partitions full. Unable to allocate.\n");
        if (skidBuffer.empty()) skidBuffer.swap(instBuffer);
        std::queue<CribDynInstPtr>().swap(instBuffer);
        return;
    }

    loopPartitions = partitions[allocPartition].allocate(insts_to_dispatch, loopPartitions);
    allocPartition = (allocPartition+1)%numPartitions;
}

inline void
Crib::commitInstructions()
{
    //if(enableCommitCheck && (curTick() - lastCommitTick) > 500000){
    //    fprintf(stderr, "Commit is stalled for more than 500000 ticks\n");
    //    assert(0);
    //}
    CribPartition& commit_partition = partitions[architectedPartition];
    bool commitable = commit_partition.isAllocated();
    if (commitable) {
        if (drainPending) {
            if (pcState(0).microPC() == 0) {
                DPRINTF(Drain, "Draining commit [sn:%lli].\n",
                        commit_partition.entries[0].inst->seqNum);
                commit_partition.entries[0].inst->fault = new ReExec;
            }
        }

        // Verify that it really is
        for (unsigned i = 0; i < commit_partition.numInsts; i++) {
            if (!(commit_partition.entries[i].status == CribEntry::Status::Executed ||
                        commit_partition.entries[i].status == CribEntry::Status::Squashed ||
                        commit_partition.entries[i].status == CribEntry::Status::BreakSquashed ||
                        commit_partition.entries[i].status == CribEntry::Status::BreakExecutedSquashed )) {
                commitable = false;
                break;
            }
            if (commit_partition.entries[i].status == CribEntry::Status::Executed &&
                     commit_partition.entries[i].inst->mispredicted() &&
                     (!commit_partition.entries[i].inst->mispredictSignaled
                      || !commit_partition.entries[i].inst->loopMispredExtra)) {
                commitable = false;
                break;
            }

            if (commit_partition.entries[i].status == CribEntry::Status::Executed &&
                commit_partition.entries[i].inst->isStoreConditional() &&
                commit_partition.entries[i].inst->readPredicate() &&
                !commit_partition.entries[i].inst->isStoreConditionalComplete()) {

                lsq->makeSenior(commit_partition.entries[i].inst);
                commitable = false;
                break;
            }

            if (commit_partition.entries[i].status != CribEntry::Status::Squashed &&
                commit_partition.entries[i].inst->fault != NoFault) {
                if (!commit_partition.entries[i].inst->commitFault) {
                    DPRINTF(CribCommit, "[sn:%lli]: Initiating fault.\n",
                            commit_partition.entries[i].inst->seqNum);
                    commit_partition.entries[i].inst->commitFault = true;
                    commit_partition.entries[i].inst->commitFaultCycles = 0;
                    commitable = false;
                    break;
                } else if (commit_partition.entries[i].inst->commitFaultCycles < 2) {
                    DPRINTF(CribCommit, "[sn:%lli]: Continuing fault.\n",
                            commit_partition.entries[i].inst->seqNum);
                    commit_partition.entries[i].inst->commitFaultCycles++;
                    commitable = false;
                    break;
                }
            }
        }
    }

    if (!commitable) {
        DPRINTF(CribCommit, "COMMIT Partition %i cannot be committed. "
                "[allocated:%i][numInsts:%i][numInstsDone:%i].\n",
                architectedPartition, commit_partition.isAllocated(),
                commit_partition.numInsts, commit_partition.numInstsDone);
        return;
    }

    CribDynInstPtr fault_inst = NULL;
    unsigned commit_entry_num = 0;
    for (unsigned i = 0; i < commit_partition.numInsts; i++) {
        CribEntry &entry = commit_partition.entries[i];


        if (entry.status != CribEntry::Status::Squashed && entry.status != CribEntry::Status::BreakSquashed &&
            entry.status != CribEntry::Status::BreakExecutedSquashed && entry.inst->fault != NoFault) {
            DPRINTF(CribCommit, "[sn:%lli]: Executing fault.\n", entry.inst->seqNum);
            fault_inst = entry.inst;
            if (entry.inst->isMemRef()) {
                lsq->deassertRequest(entry.inst, false);
            }
            commit_entry_num = i;
            break;
        }
        if (entry.status == CribEntry::Status::Squashed ||
            entry.status == CribEntry::Status::BreakSquashed ||
            entry.status == CribEntry::Status::BreakExecutedSquashed) break;

        if (entry.inst->isLoad() && entry.inst->readPredicate()) lsq->deassertRequest(entry.inst, true);
        if (entry.inst->isStore() && entry.inst->readPredicate()
                && !entry.inst->isStoreConditional()) lsq->makeSenior(entry.inst);

#if TRACING_ON
        entry.inst->retireTick = curTick();
        entry.inst->printPipeView();

        std::stringstream ss;
        if (entry.inst->readPredicate()) {
            for (unsigned j=0; j < entry.numDestRegs; j++) {
                if (entry.results[j].type == ResultInt) {
                    ss << "[r" << entry.results[j].index << ":" << std::hex << entry.results[j].reg.i << "]";
                }
            }

            if (entry.inst->isMemRef()) {
                ss << "[mem:0x"
                   << std::hex << entry.inst->memAddr << "-0x"
                   << std::hex << entry.inst->memAddr + entry.inst->memSize - 1
                   << "]";
            }
        }
        DPRINTF(CommitInst, "[sn:%lli %lli %lli]: Commiting instruction [pc:%#x (%d)].%s\n", //. %s\n",
              entry.inst->loopColor,
              entry.inst->loopItrCount,
              entry.inst->seqNum,
              entry.inst->pcState().instAddr(),
              entry.inst->pcState().microPC(),
              entry.inst->staticInst->disassemble(entry.inst->pcState().instAddr()).c_str());
              //ss.str().c_str());

#endif
        totCommitInterval += (curTick() - lastCommitTick);
        int cdist = architectedPartition*partitionSize + entry.entryNum - lastCommitEntry;
        if (cdist<0) cdist += (partitionSize * numPartitions);
        totCommitDistance += (cdist); 
        nCommittedOps++; 

        lastCommitTick = curTick();
        lastCommitEntry = architectedPartition*partitionSize + entry.entryNum;

        if (entry.inst->isLoad()) cribComLoadOps++;
        else if (entry.inst->isStore()) cribComStoreOps++;
        else if (entry.inst->isInteger()) cribComIntOps++;
        else if (entry.inst->isFloating()) cribComFPOps++;

        cpu->instDone(entry.inst, 0);

        if (!toDispatch->cribInfo[0].squash) toDispatch->cribInfo[0].doneSeqNum = entry.inst->seqNum;

        if (entry.status == CribEntry::Status::Executed) commit_entry_num = i;

        if (entry.inst->isSquashAfter()) {
            toDispatch->cribInfo[0].squashInst = entry.inst;
            toDispatch->cribInfo[0].squash = true;
            toDispatch->cribInfo[0].mispredictInst = NULL;
            toDispatch->cribInfo[0].doneSeqNum = entry.inst->seqNum;
            squashing = true;
            ackSquashSeqNum = entry.inst->seqNum;
            TheISA::PCState nextPC = entry.inst->pcState();
            TheISA::advancePC(nextPC, entry.inst->staticInst);
            toDispatch->cribInfo[0].pc = nextPC;

            DPRINTF(CribCommit, "[sn:%lli]: Commiting IsSquashAfter instruction.\n",
                   entry.inst->seqNum);
        }


        if (entry.inst->mispredicted()) break;
    }

    DPRINTF(CribCommit, "COMMIT Committing partition %i.\n",
            commit_partition.partitionNum);

    CribEntry &commit_entry =
        commit_partition.entries[commit_entry_num];

    bool move_retire = !commit_partition.loopPartition || commit_partition.loopFail || fault_inst || commit_partition.breakSquashed || (commit_partition.loopPartition && !commit_entry.inst->isLoopEnd() && commit_entry.inst->mispredictSignaled);
    if (commit_entry.inst->isLoopEnd() && !fault_inst) {
        DPRINTF(CribLoop, "[sn:%lli]: Committing loop end.\n", commit_entry.inst->seqNum);

        if (commit_entry.inst->mispredicted()) {
            DPRINTF(CribLoop, "[sn:%lli]: Loop iterates.\n", commit_entry.inst->seqNum);
        } else {
            DPRINTF(CribLoop, "[sn:%lli]: Loop falls through.\n", commit_entry.inst->seqNum);
            move_retire = true;
        }
    }

    if (commit_partition.loopPartition) {
        commit_partition.loopExecutions++;
    }

    unsigned next_architected;
    if (commit_partition.loopEnd && commit_entry.inst->mispredicted() && !fault_inst) {
        // Iterating
        unsigned num_loop_partitions = commit_partition.loopNumPartitions;
        next_architected =  ((architectedPartition + numPartitions + 1) - num_loop_partitions) % numPartitions;
    } else {
        // Moving forward
        next_architected = (architectedPartition + 1) % numPartitions;
    }

    // We are doing some trickery here by storing this instructions
    // un-incremented PC to the latchPC (so that exceptions that work through
    // the threadcontext use the instructions current pc
    // (and increment from there)
    TheISA::PCState next_pc = commit_entry.inst->pcState();
    TheISA::advancePC(next_pc, commit_entry.inst->staticInst);

    partitions[next_architected].latchSeqNum = commit_entry.inst->seqNum;
    partitions[next_architected].latchRegs   = commit_partition.entries[commit_partition.partitionSize-1].outputs->regs;
    partitions[next_architected].latchPC     = commit_entry.inst->pcState();

    partitions[architectedPartition].commit();
    partitions[architectedPartition].architected = false;
    partitions[architectedPartition].entries[0].inputs->rexec = partitions[architectedPartition].latchRexec;
    partitions[architectedPartition].latchRexec.reset();
    partitions[next_architected].architected = true;

    // Retire unnecessary partitions here
    if (move_retire) {
        do {
            DPRINTF(CribCommit, "Retiring partition %i.\n", retirePartition);
            //if (retirePartition != next_architected) {
                for (unsigned i = 0; i < partitionSize; i++) {
                    partitions[retirePartition].entries[i].inputs->ready.reset();
                    partitions[retirePartition].entries[i].inputs->flags.reset();
                    partitions[retirePartition].entries[i].squash();
                }
            //}
            partitions[retirePartition].deallocate();
            retirePartition = (retirePartition + 1) % numPartitions;
        } while(retirePartition != next_architected);
    }

    architectedPartition = next_architected;

    // Handle faults here (so we modify the regs in the next partition)
    if (fault_inst) {
        fault_inst->threadState->noSquashFromTC = true;
        fault_inst->fault->invoke(fault_inst->tcBase(), fault_inst->staticInst);
        fault_inst->threadState->noSquashFromTC = false;

        // Signal squash
        toDispatch->cribInfo[0].squash = true;
        toDispatch->cribInfo[0].pc = pcState(0);
        toDispatch->cribInfo[0].doneSeqNum = committedSeqNum(0);
        toDispatch->cribInfo[0].squashInst = NULL;
        toDispatch->cribInfo[0].mispredictInst = NULL;
        if (drainPending) {
            toDispatch->cribInfo[0].drain = true;
            cpu->commitDrainInitiated();
        }
        squashed = true;
        ackSquashSeqNum = committedSeqNum(0);

        fault_inst->fault = NoFault;
        allocPartition = architectedPartition;
    } else {
        // Properly increment latched pc now that we know there are no exceptions
        pcState(next_pc, 0);
    }
}

inline void
Crib::updateStatus()
{
    if (status == Idle) {
        if (!empty()) {
            status = Running;
            // Tell cpu we're active?
            DPRINTF(Crib, "Status changing: IDLE->RUNNING.\n");
        }
    }

    if (status == Running) {
        if (full()) {
            status = Blocking;
            DPRINTF(Crib, "Status changing: RUNNING->BLOCKING.\n");
            toDispatch->cribBlock[0] = true;
        } else if (empty()) {
            status = Idle;
            DPRINTF(Crib, "Status changing: RUNNING->IDLE.\n");
        }
    }

    if (status == Blocking) {
        if (!full()) {
            DPRINTF(Crib, "Status changing: BLOCKING->RUNNING.\n");
            status = Running;
            toDispatch->cribUnblock[0] = true;
        }
    }

    if (squashed) {
        squashing = true;
        DPRINTF(Crib, "Beginning squash sequence [sn:%lli].\n", ackSquashSeqNum);
        std::queue<CribDynInstPtr>().swap(instBuffer);
        std::queue<CribDynInstPtr>().swap(skidBuffer);
    }

    if(empty() && instBuffer.empty() && skidBuffer.empty()) {
        toDispatch->cribInfo[0].cribEmpty = true;
    }
}

inline void
Crib::doTCSquash()
{
    DPRINTF(Crib, "Squash from thread context processed this cycle.\n");

    // Zero out the instruction buffers
    std::queue<CribDynInstPtr>().swap(instBuffer);
    std::queue<CribDynInstPtr>().swap(skidBuffer);

    // Squash the crib
    for (unsigned i = 0; i < numPartitions; i++) {
        partitions[i].doTCSquash();
    }
    allocPartition = architectedPartition;
    status = Idle;

    // Signal squash
    toDispatch->cribInfo[0].squash = true;
    toDispatch->cribInfo[0].pc = pcState(0);
    toDispatch->cribInfo[0].doneSeqNum = committedSeqNum(0);
    toDispatch->cribInfo[0].squashInst = NULL;
    toDispatch->cribInfo[0].mispredictInst = NULL;
    squashTC = false;

    squashed = true;
    ackSquashSeqNum = committedSeqNum(0);
}

void
Crib::setArchIntReg(RegIndex index, IntReg val, ThreadID tid)
{
    partitions[architectedPartition].latchRegs.setIntReg(index, val);
    partitions[architectedPartition].entries[0].inputs->regs.setIntReg(index, val);
}

IntReg
Crib::readArchIntReg(RegIndex index, ThreadID tid)
{
    return partitions[architectedPartition].latchRegs.readIntReg(index);
}

void
Crib::setArchFloatReg(RegIndex index, FloatReg val, ThreadID tid)
{
    partitions[architectedPartition].latchRegs.setFloatReg(index+TheISA::NumIntRegs, val);
    partitions[architectedPartition].entries[0].inputs->regs.setFloatReg(index+TheISA::NumIntRegs, val);
}

void
Crib::setArchFloatRegBits(RegIndex index, FloatRegBits val, ThreadID tid)
{
    partitions[architectedPartition].latchRegs.setFloatRegBits(index+TheISA::NumIntRegs, val);
    partitions[architectedPartition].entries[0].inputs->regs.setFloatRegBits(index+TheISA::NumIntRegs, val);
}

FloatReg
Crib::readArchFloatReg(RegIndex index, ThreadID tid)
{
    return partitions[architectedPartition].latchRegs.readFloatReg(index+TheISA::NumIntRegs);
}

FloatRegBits
Crib::readArchFloatRegBits(RegIndex index, ThreadID tid)
{
    return partitions[architectedPartition].latchRegs.readFloatRegBits(index+TheISA::NumIntRegs);
}

TheISA::PCState
Crib::pcState(ThreadID tid)
{
    return partitions[architectedPartition].latchPC;
}


InstSeqNum
Crib::committedSeqNum(ThreadID tid)
{
    return partitions[architectedPartition].latchSeqNum;
}

void
Crib::pcState(const TheISA::PCState &val, ThreadID tid)
{
    partitions[architectedPartition].latchPC = val;
}

inline void
Crib::disassemble()
{
    unsigned partition = retirePartition;
    do
    {
    //    if (partitions[partition].isAllocated())
            partitions[partition].disassemble();
        partition = (partition + 1) % numPartitions;
    } while (partition != retirePartition);
}

inline void
Crib::resolveBranchMispredicts()
{
    // Redirect front end with oldest misprediction
    unsigned partition = architectedPartition;
    CribEntry *break_entry = NULL;
    bool break_failed = false;
    bool break_resolved = false;
    bool already_failed = false;
    do
    {
        if (!partitions[partition].isAllocated() ||
             partitions[partition].instsSquashed() ) break;
        CribDynInstPtr inst = NULL;
        bool mispred = partitions[partition].resolveBranchMispredicts(inst, break_entry, break_failed, break_resolved);

        if (break_entry) {
            if (break_failed && !already_failed) {
                already_failed = true;
                break_entry->inst->setFailedLoopBreak();
                if (break_entry->inst->breakAcked) {
                    mispred = true;
                    inst = break_entry->inst;
                    DPRINTF(CribLoop, "[sn:%i]: Setting inst to break entry for resolution.\n", inst->seqNum);
                    inst->loopMispredExtra = false;
                } else {
                    DPRINTF(CribLoop, "[sn:%i]: Setting inst to break acked.\n", break_entry->inst->seqNum);
                    break_entry->inst->breakAcked = true;
                    break_entry->setFlags.reset(Break);
                }
            } else if (break_resolved && !already_failed) {
                DPRINTF(CribLoop, "[sn:%i]: Break inst properly resolved.\n", break_entry->inst->seqNum);
                if (!break_entry->inst->isLoopUnrollEnd()) {
                    cribLoopBreaks++;
                }
                TheISA::PCState nextPC = break_entry->inst->pcState();
                TheISA::advancePC(nextPC, break_entry->inst->staticInst);
                break_entry->inst->mispredictSignaled = true;
                break_entry->inst->setPredTarg(nextPC);
            }
        }

        if (mispred && inst) {
            // Signal squash
            DPRINTF(Crib, "[sn:%lli]: Signalling mispredict.\n", inst->seqNum);

            TheISA::PCState nextPC = inst->pcState();
            TheISA::advancePC(nextPC, inst->staticInst);

            if (inst->hasFallthrough()) {
                cribLoopMispredicted++;
            }

            toDispatch->cribInfo[0].squash = true;
            toDispatch->cribInfo[0].pc = nextPC;
            toDispatch->cribInfo[0].doneSeqNum = inst->seqNum;
            toDispatch->cribInfo[0].squashInst = inst;
            toDispatch->cribInfo[0].mispredictInst = inst;
            toDispatch->cribInfo[0].branchTaken = inst->pcState().branching();

            inst->mispredictSignaled = true;
            inst->setPredTarg(nextPC);
            squashed = true;
            ackSquashSeqNum = inst->seqNum;
            allocPartition = (partition + 1) % numPartitions;
        }

        partition = (partition + 1) % numPartitions;
    } while (partition != architectedPartition && partition != allocPartition && !squashed);
}

inline void
Crib::removeSquashedPartitions()
{
    for (unsigned i = 0; i < numPartitions; i++) {
        if (partitions[i].isSquashed()) {
            DPRINTF(Crib, "Removing squashed partition %i.\n", i);
            partitions[i].deallocate();
        }
    }
}


inline void
CribPartition::init(Crib *_crib, CribLSQ *_lsq, unsigned _size, CribCPUParams *params, unsigned _num)
{
    crib = _crib;
    lsq = _lsq;
    partitionSize = _size;
    partitionNum = _num;
    latchPC.set(0);
    latchSeqNum = 0;
    entries = new CribEntry[partitionSize]();
    for (unsigned i = 0; i < partitionSize; i++) {
        entries[i].init(this, lsq, i, params);
    }

    architected = false;
    allocated = false;
    numInsts = 0;
    numInstsDone = 0;

    loopStart = false;
    loopEnd = false;
    loopFail = false;
    loopPartition = false;
    breakSquashed = false;
}

inline std::string
CribPartition::name() const
{
    std::stringstream ss;
    ss << crib->name() << ".partition" << partitionNum;
    return ss.str();
}

inline void
CribPartition::print() const
{
    DPRINTF(Crib, "DebugPrint: [archPartition:%i][allocPartiton:%o][retirePartition:%i][alloc:%i][arch:%i][loop:%i][l_start:%i][l_end:%i][l_fail:%i][insts:%i][instsdone:%i][latchRexec5:%i].\n",

            crib->architectedPartition == partitionNum,
            crib->allocPartition == partitionNum,
            crib->retirePartition == partitionNum,
            allocated, architected,
            loopPartition, loopStart, loopEnd, loopFail,
            numInsts, numInstsDone,
            latchRexec.test(5)
            );
}

inline void
CribPartition::doTCSquash()
{
    if (allocated) {
        DPRINTF(Crib, "Deallocating partition due to TC squash.\n");
        for (unsigned i = 0; i < partitionSize; i++) {
            entries[i].squash();
        }

        allocated = false;
    }
}

inline bool
CribPartition::isAllocated()
{
    return allocated;
}

inline unsigned
CribPartition::allocate(std::queue<CribDynInstPtr> &insts, unsigned loop_partitions)
{
    DPRINTF(Crib, "Allocating partition.\n");
    assert(!allocated);

    allocated = true;
    numInsts = insts.size();
    numInstsDone = 0;
    loopExecutions = 0;
    breakSquashed = false;

    int dispatched = 0;
    while (!insts.empty()) {
        CribDynInstPtr &inst = insts.front();
        entries[dispatched++].allocate(inst);
        loopStart = loopStart || inst->isLoopStart();
        loopEnd = loopEnd || inst->isLoopEnd();
        loopFail = loopFail || inst->isLoopFail();
        loopPartition = loopPartition || inst->hasFallthrough();

        if (inst->isLoopStart()) {
            loop_partitions = 1;
        } else if (inst->isLoopEnd()) {
            loopNumPartitions = loop_partitions;
        }

        insts.pop();
    }

    if(dispatched >0){
        crib->enableCommitCheck = true;
    }

    loop_partitions++;

    while (dispatched < partitionSize) {
        entries[dispatched++].allocateNoop();
    }

    RegSet valids = entries[0].inputs->ready;
    for (unsigned j = 0; j < partitionSize-1; j++) {
        valids = entries[j].inputs->ready & valids;
        if (j < numInsts) {
            valids = valids & ~entries[j].regWrite;
        }
        if ((j+1) < numInsts) {
            entries[j+1].inputs->ready = entries[j+1].inputs->ready & valids;
        }
    }

    for (unsigned j = 0; j < partitionSize; j++) {
        entries[j].inputs->flags.reset();
    }

    return loop_partitions;
}

inline void
CribPartition::deallocate()
{
    DPRINTF(Crib, "Deallocating partition.\n");

    // This assert falsely fires sometimes on already squashed partitions
    // (comes up with loopy crib).  Commenting it out for now, no harm from this I think
    //assert(allocated);
    allocated = false;
    //architected = false;
    numInsts = 0;
    numInstsDone = 0;
    for (unsigned i = 0; i < partitionSize; i++) {
        //entries[i].inst = NULL;
        //entries[i].status = CribEntry::Status::Invalid;
        entries[i].reset();
    }

    loopStart = false;
    loopEnd = false;
    loopFail = false;
    loopPartition = false;
}

inline void
CribPartition::commit()
{
    DPRINTF(Crib, "Committing partition.\n");
    assert(allocated);
    numInstsDone = 0;
    for (unsigned i = 0; i < numInsts; i++) {
        entries[i].status = CribEntry::Status::Allocated;
        entries[i].inst->loopItrCount++;
        entries[i].isSplit = false;
        entries[i].splitLoDone = false;
        entries[i].splitHiDone = false;
        entries[i].inst->setPredicate(true);
        entries[i].inst->pc = entries[i].inst->origPC;
    }

    for (unsigned j = 0; j < partitionSize; j++) {
        entries[j].inputs->ready.reset();
    }
}

inline bool
CribPartition::isSquashed()
{
    if (!isAllocated()) return false;

    for (unsigned i = 0; i < partitionSize; i++) {
        if (entries[i].status != CribEntry::Status::Squashed || entries[i].inputs->flags.test(Squash)) {
            return false;
        }
    }

    return true;
}

inline bool
CribPartition::instsSquashed()
{
    for (unsigned i = 0; i < partitionSize; i++) {
        if (entries[i].status != CribEntry::Status::Squashed) {
            return false;
        }
    }

    return true;
}

inline void
CribPartition::evaluate()
{
    for (unsigned i = 0; i < partitionSize; i++) {
        entries[i].evaluate();
    }
}

inline void
CribPartition::intraPropagate()
{
    for (unsigned i = 0; i < partitionSize; i++) {
        entries[i].intraPropagate();
    }
}

inline void
CribPartition::interPropagate()
{
    for (unsigned i = 0; i < partitionSize-1; i++) {
        entries[i+1].inputs = entries[i].outputs;
    }
}

inline void
CribPartition::disassemble()
{
    for (unsigned i = 0; i < partitionSize; i++) {
        entries[i].disassemble();
    }
}

inline bool
CribPartition::resolveBranchMispredicts(CribDynInstPtr &inst, CribEntry *&break_entry, bool &break_failed, bool &break_resolved)
{
    for (unsigned i = 0; i < numInsts; i++) {
        if (break_entry && (entries[i].status == CribEntry::Status::Squashed || entries[i].inst->isLoopFail())) {
            break_failed = true;
            DPRINTF(CribLoop, "[sn:%i]: Loop break failed - squashed/failed entry.\n", entries[i].inst->seqNum);
        }

        if (break_entry && !break_failed && entries[i].inst->isLoopEnd() &&
                (entries[i].status==CribEntry::Status::BreakSquashed || entries[i].status==CribEntry::Status::BreakExecutedSquashed)) {
            break_resolved = true;
            DPRINTF(CribLoop, "[sn:%i]: Loop break resolved.\n", entries[i].inst->seqNum);
        }


        if ((entries[i].status == CribEntry::Status::Executed || entries[i].status == CribEntry::Status::BreakExecutedSquashed) &&
            (entries[i].inst->isControl() || entries[i].inst->readPredicate()) &&
            entries[i].inst->mispredicted()) {
            if (break_entry) {
                break_failed = true;
                DPRINTF(CribLoop, "[sn:%i]: Loop break failed - mispredicted entry.\n", entries[i].inst->seqNum);
            }

            if (entries[i].inst->isLoopEnd()) {
                if (entries[i].inst->pcState().instNPC() == entries[i].inst->getFallthroughAddr()) {
                    // Never executes currently because the prediction forced by the front end predicts the
                    // fallthough, so its never a misprediction
                    DPRINTF(CribLoop, "[sn:%lli]: Loop branch falling through.\n",
                            entries[i].inst->seqNum);
                }
                return false;
            } else if (entries[i].inst->isLoopBreak()) {
                DPRINTF(CribLoop, "[sn:%lli]: Loop break detected by bpred.\n",
                        entries[i].inst->seqNum);
                if (!break_entry) {
                    break_entry = &entries[i];
                    //return false;
                }
            }

            if (!break_entry) {
                if (entries[i].inst->mispredictSignaled) {
                    return false;
                } else {
                    inst = entries[i].inst;
                    if (loopPartition) {
                        DPRINTF(CribLoop, "[sn:%lli]: "
                                "Mispredicted instruction in loop partition. "
                                "[pc:%s].\n",
                                inst->seqNum,
                                inst->pcState());
                        inst->loopMispredExtra = false;
                        if (!architected) {
                            DPRINTF(CribLoop, "[sn:%lli]: "
                                    "Delaying loop branch mispredict resolution. "
                                    "[pc:%s].\n",
                                    inst->seqNum,
                                    inst->pcState());
                            return false;
                        }
                        loopFail = true;
                    }
                    return true;
                }
            }
        }
    }

    return false;
}

unsigned CribEntry::exeLatencies[Num_OpClasses];

CribEntry::CribEntry()
    : inputs(new Propagated),
      outputs(NULL)
{
    reset();
}

inline void
CribEntry::init(CribPartition *_partition, CribLSQ* _lsq,
    unsigned entry_number, CribCPUParams *params)
{
    partition = _partition;
    lsq = _lsq;
    entryNum = entry_number;
    isSplit = false;
    splitLoDone = false;
    splitHiDone = false;

    exeLatencies[IntAluOp          ] = params->IntAluLatency;
    exeLatencies[IntMultOp         ] = params->IntMultLatency;
    exeLatencies[IntDivOp          ] = params->IntDivLatency;
    exeLatencies[FloatAddOp        ] = params->FloatAddLatency;
    exeLatencies[FloatCmpOp        ] = params->FloatCmpLatency;
    exeLatencies[FloatCvtOp        ] = params->FloatCvtLatency;
    exeLatencies[FloatMultOp       ] = params->FloatMultLatency;
    exeLatencies[FloatDivOp        ] = params->FloatDivLatency;
    exeLatencies[FloatSqrtOp       ] = params->FloatSqrtLatency;
    exeLatencies[SimdAddOp         ] = params->SimdAddLatency;
    exeLatencies[SimdAddAccOp      ] = params->SimdAddAccLatency;
    exeLatencies[SimdAluOp         ] = params->SimdAluLatency;
    exeLatencies[SimdCmpOp         ] = params->SimdCmpLatency;
    exeLatencies[SimdCvtOp         ] = params->SimdCvtLatency;
    exeLatencies[SimdMiscOp        ] = params->SimdMiscLatency;
    exeLatencies[SimdMultOp        ] = params->SimdMultLatency;
    exeLatencies[SimdMultAccOp     ] = params->SimdMultAccLatency;
    exeLatencies[SimdShiftOp       ] = params->SimdShiftLatency;
    exeLatencies[SimdShiftAccOp    ] = params->SimdShiftAccLatency;
    exeLatencies[SimdSqrtOp        ] = params->SimdSqrtLatency;
    exeLatencies[SimdFloatAddOp    ] = params->SimdFloatAddLatency;
    exeLatencies[SimdFloatAluOp    ] = params->SimdFloatAluLatency;
    exeLatencies[SimdFloatCmpOp    ] = params->SimdFloatCmpLatency;
    exeLatencies[SimdFloatCvtOp    ] = params->SimdFloatCvtLatency;
    exeLatencies[SimdFloatDivOp    ] = params->SimdFloatDivLatency;
    exeLatencies[SimdFloatMiscOp   ] = params->SimdFloatMiscLatency;
    exeLatencies[SimdFloatMultOp   ] = params->SimdFloatMultLatency;
    exeLatencies[SimdFloatMultAccOp] = params->SimdFloatMultAccLatency;
    exeLatencies[SimdFloatSqrtOp   ] = params->SimdFloatSqrtLatency;
    exeLatencies[MemReadOp         ] = params->MemReadLatency;
    exeLatencies[MemWriteOp        ] = params->MemWriteLatency;
    exeLatencies[IprAccessOp       ] = params->IprAccessLatency;
    exeLatencies[InstPrefetchOp    ] = params->InstPrefetchLatency;
}

std::string
CribEntry::name() const
{
    std::stringstream ss;
    ss << partition->name() << ".entry" << entryNum;
    return ss.str();
}

inline void
CribEntry::reset()
{
    inst = NULL;
    regRead.reset();
    regWrite.reset();
    //setFlags.reset();
    status = Invalid;
    setRexec = false;
    isSplit = false;
    splitLoDone = false;
    splitHiDone = false;
}

inline void
CribEntry::squash()
{
    DPRINTF(CribExecute, "Squashing entry.\n");
    if ((status == Executing || status == Executed) && inst->isMemRef() && inst->readPredicate()) {
        lsq->deassertRequest(inst);
    }

    if (status != Noop && status != Invalid && status!= Squashed &&
        inst->isLoad() && inst->prefetchOutstanding) {
        lsq->deassertAllPrefetches(inst);
    }
    reset();
}

inline void
CribEntry::allocate(CribDynInstPtr &_inst)
{
    DPRINTF(CribExecute, "Allocating entry for [sn:%lli][pc:%s].\n", _inst->seqNum,
            _inst->pcState());
    status = Allocated;
    inst = _inst;
    //inputs->ready.reset();
    inst->setCribEntry(this);
    regRead = inst->srcRegSet();
    regWrite = inst->destRegSet(destRegMap);
    numDestRegs = regWrite.count();
    setFlags.reset();
}

inline void
CribEntry::allocateNoop()
{
    status = Noop;
    inst = NULL;
    regRead.reset();
    regWrite.reset();
}

inline void
CribEntry::evaluate()
{
    firstPropagation = true;
    if (status == Invalid  || status == Squashed) return;

    if (inputs->flags.test(Squash)) {
        if (status != Noop && status != BreakSquashed && status != BreakExecutedSquashed) {
            DPRINTF(CribExecute, "[sn:%lli]: Squashing in evaluate.\n", inst->seqNum);
            if (inst->isMemRef() && (status == Executing || status == Executed) && inst->readPredicate()) {
                lsq->deassertRequest(inst);
            }
            if (inst->isLoad() && inst->prefetchOutstanding) {
                lsq->deassertAllPrefetches(inst);
            }
            inst->setPredicate(false);
        }
        status = Squashed;
        return;
    } else if (status != BreakSquashed && status != BreakExecutedSquashed && status != Noop && inputs->flags.test(Break)) {
        if (status != Noop) {
            DPRINTF(CribExecute, "[sn:%lli]: Break squashing in evaluate.\n", inst->seqNum);
            if (inst->isMemRef() && (status == Executing || status == Executed) && inst->readPredicate()) {
                lsq->deassertRequest(inst);
            }
            if (inst->isLoad() && inst->prefetchOutstanding) {
                lsq->deassertAllPrefetches(inst);
            }
        }
        setRexec = true;
        if (status == Executed || (status == Executing && inst->isMemRef())) {
            DPRINTF(CribExecute, "[sn:%lli]: Moving to BreakExecutedSquashed.\n", inst->seqNum);
            status =  BreakExecutedSquashed;
        } else {
            DPRINTF(CribExecute, "[sn:%lli]: Moving to BreakSquashed.\n", inst->seqNum);
            status = BreakSquashed;
        }
        partition->breakSquashed = true;
        return;
    }

    if (status == Noop || status == BreakSquashed || status == BreakExecutedSquashed) return;

    // Check if we've lost ready bits
    if (status == Executing || status == Executed) {
        if (((regRead & ~inputs->ready).any()) || (regRead & inputs->rexec).any()) {
            DPRINTF(CribExecute, "[sn:%i]: Source operand became unready."
                    "Require re-execution [mispredict signaled: %d][mispredicted: %d].\n", inst->seqNum, inst->mispredictSignaled,
                    inst->mispredicted());

            if (inst->isMemRef() && inst->readPredicate()) {
                lsq->deassertRequest(inst);
            }
            if (inst->isLoad() && inst->prefetchOutstanding) {
                lsq->deassertAllPrefetches(inst);
            }

            if (!inst->isLoopEnd() && !inst->mispredictSignaled && status == Executed && inst->mispredicted()) {
                // We squashed instructions below us, make sure we redirect the front end again
                DPRINTF(CribExecute, "[sn:%i]: Modifying predicted target to force fallthrough.\n", inst->seqNum);
                inst->predTarget = PCState();
            }
            inst->setPredicate(true);
            inst->pc = inst->origPC;
            inst->mispredictSignaled = false;
            // May be a branch we re-execute, restore the origial PC
            status = Allocated;
            setFlags.reset();
        }
    }

    if (status == Allocated) {
        // Make sure we can execute this on one of the resources
        if ((inst->isMemRef() || partition->crib->resources[mapOpToResource(inst->staticInst->opClass())])


        // Test if all required inpust are ready
        //if (
           && (regRead & ~inputs->ready).none() && (!inst->isNonSpeculative() || partition->architected) ) {
#if TRACING_ON
            inst->issueTick = curTick();
#endif

            if (!inst->isMemRef()) {
                partition->crib->resources[mapOpToResource(inst->staticInst->opClass())]--;
            }
            status = Executing;
            DPRINTF(CribExecute, "[sn:%lli %lli %lli]: Triggering execute. %d\n",
                    inst->loopColor,
                    inst->loopItrCount,
                    inst->seqNum, partition->loopExecutions);

            if (inst->isLoad()) partition->crib->cribLoadOps++;
            else if (inst->isStore()) partition->crib->cribStoreOps++;
            else if (inst->staticInst->opClass() == IntMultOp) partition->crib->cribMultOps++;
            else if (inst->isInteger()) partition->crib->cribIntOps++;
            else if (inst->isFloating()) partition->crib->cribFPOps++;

            // Really execute now, destinations buffered
            if (!inst->isMemRef()) {
                inst->execute();
                if (inst->readPredicate()) {
                    exeCycles = exeLatencies[inst->staticInst->opClass()];
                } else {
                    exeCycles = 1;
                }
            } else {
                inst->initiateAcc();
                if (!inst->readPredicate()) {
                    exeCycles = 1;
                }
            }
        }
    }

    if (status == Executing && (!inst->isMemRef() || !inst->readPredicate())) {
        --exeCycles;
        if (exeCycles == 0) {
            markExecuted();

            if (inst->isLoopEnd()) {
                if(!inst->mispredicted()) {
                    DPRINTF(CribExecute, "[sn:%i]: Loop end encountered. "
                            "[npc:%#x].\n",
                            inst->seqNum, inst->pcState().instNPC());
                } else {
                    DPRINTF(CribExecute, "[sn:%i]: Loop iterating. "
                            "[npc:%#x].\n",
                            inst->seqNum, inst->pcState().instNPC());
                    inst->mispredictSignaled = true;
                }
            } else if (inst->isLoopBreak() && !inst->breakAcked) {
                DPRINTF(CribExecute, "[sn:%i]: Loop break instruction.\n",
                        inst->seqNum);
                setFlags.set(Break);
            }
        }
    }

    if (status == Executed && !inst->isLoopEnd() && !inst->isLoopBreak()) {
        if (inst->mispredicted() && !inst->mispredictSignaled) {
            inst->setLoopFail();
            setFlags.set(Squash);
            DPRINTF(CribExecute, "[sn:%i]: Setting squash out.\n",
                    inst->seqNum);
        } else if (inst->mispredictSignaled && inst->loopMispredExtra) {
            setFlags.reset(Squash);
            DPRINTF(CribExecute, "[sn:%i]: Clearing squash out.\n",
                    inst->seqNum);
        } else if (inst->mispredictSignaled && !inst->loopMispredExtra) {
            inst->loopMispredExtra = true;
            if (inst->isLoopContinue()) {
                partition->crib->cribLoopContinues++;
            }
        }
    }

    if (status == Executed && inst->fault != NoFault && inst->commitFault) {
        inst->setLoopFail();
        setFlags.set(Squash);
    }
}

void
CribEntry::markExecuted()
{
#if TRACING_ON
    inst->completeTick = curTick();
#endif
    status = Executed;
    setRexec = true;
    partition->numInstsDone++;
    DPRINTF(CribExecute, "[sn:%i]: Execute completed.\n", inst->seqNum);
}

inline void
CribEntry::intraPropagate()
{
    RegSet &modifiedRexec = inputs->rexec;
    RegSet &modifiedReady = inputs->ready;
    RegState &modifiedRegs = inputs->regs;
    FlagSet &modifiedFlags = inputs->flags;

    if (!firstPropagation) {
        if (status != Invalid && status != Squashed) {
            if (inputs->flags.test(Squash)) {
                if (status != Noop && status != BreakSquashed && status != BreakExecutedSquashed) {
                    DPRINTF(CribExecute, "[sn:%lli]: Squashing in evaluate.\n", inst->seqNum);
                    if (inst->isMemRef() && (status == Executing || status == Executed) && inst->readPredicate()) {
                        lsq->deassertRequest(inst);
                    }
                    if (inst->isLoad() && inst->prefetchOutstanding) {
                        lsq->deassertAllPrefetches(inst);
                    }
                    inst->setPredicate(false);
                }
                status = Squashed;
            } else if (status != BreakSquashed && status != BreakExecutedSquashed && status != Noop && inputs->flags.test(Break)) {
                if (status != Noop) {
                    DPRINTF(CribExecute, "[sn:%lli]: Break squashing in evaluate.\n", inst->seqNum);
                    if (inst->isMemRef() && (status == Executing || status == Executed) && inst->readPredicate()) {
                        lsq->deassertRequest(inst);
                    }
                    if (inst->isLoad() && inst->prefetchOutstanding) {
                        lsq->deassertAllPrefetches(inst);
                    }
                }
                setRexec = true;
                if (status == Executed || (status == Executing && inst->isMemRef())) {
                    DPRINTF(CribExecute, "[sn:%lli]: Moving to BreakExecutedSquashed.\n", inst->seqNum);
                    status =  BreakExecutedSquashed;
                } else {
                    DPRINTF(CribExecute, "[sn:%lli]: Moving to BreakSquashed.\n", inst->seqNum);
                    status = BreakSquashed;
                }
                partition->breakSquashed = true;
            } else if (status == Executed || status == Executing) {
                if ((regRead & ~inputs->ready).any() || (regRead & inputs->rexec).any()) {
                    DPRINTF(CribExecute, "[sn:%i]: Source operand became unready."
                            "Require re-execution [mispredict signaled: %d][mispredicted: %d].\n", inst->seqNum, inst->mispredictSignaled,
                            inst->mispredicted());

                    if (inst->isMemRef() && inst->readPredicate()) {
                        lsq->deassertRequest(inst);
                    }
                    if (inst->isLoad() && inst->prefetchOutstanding) {
                        lsq->deassertAllPrefetches(inst);
                    }

                    if (!inst->isLoopEnd() && !inst->mispredictSignaled && status == Executed && inst->mispredicted()) {
                        // We squashed instructions below us, make sure we redirect the front end again
                        DPRINTF(CribExecute, "[sn:%i]: Modifying predicted target to force fallthrough.\n", inst->seqNum);
                        inst->predTarget = PCState();
                    }
                    inst->setPredicate(true);
                    inst->pc = inst->origPC;
                    inst->mispredictSignaled = false;
                    // May be a branch we re-execute, restore the origial PC
                    if (status == Executed) {
                        // Done for a tricky timing case relating to loop mode support and breaks...
                        modifiedRexec |= regWrite;
                    }
                    status = Allocated;
                    setFlags.reset();
                }
            }
        }
    }

    if (status == BreakExecutedSquashed && setRexec) {
        modifiedRexec |= regWrite;
        setRexec = false;
    }


    firstPropagation = false;

    if (status != Invalid && status != Noop && status != Squashed && status != BreakSquashed && status != BreakExecutedSquashed && !inputs->flags.test(Break) && (inst->isControl() || inst->readPredicate())) {
        if (status == Executed && (!inst->isStoreConditional() || inst->isStoreConditionalComplete())) {
            modifiedReady |= regWrite;
            if (!inst->fault) {
                for (unsigned j = 0; j < numDestRegs; j++) {
                    modifiedRegs.set(results[j]);
                }
            }
            modifiedFlags |= setFlags;

            if (setRexec) {
                modifiedRexec |= regWrite;
                setRexec = false;
            } else {
                modifiedRexec = modifiedRexec & ~regWrite;
            }
        } else {
            modifiedReady = modifiedReady & ~regWrite;
            modifiedRexec = modifiedRexec & ~regWrite;
        }
    }



    // Should really rethink the control in this function, just needed to make
    // sure the squash flag is propagated on exceptions/drains properly
    if (status != Invalid && status !=Noop && status != Squashed && status != BreakSquashed && status != BreakExecutedSquashed &&  inst->fault != NoFault) {
        modifiedFlags |= setFlags;
    }

    if (entryNum < partition->numInsts && (inst->isLoopEnd() || inst->isLoopFail())) {
        modifiedFlags.reset(Break);
    }

    outputs = inputs;
}

inline void
CribEntry::disassemble()
{
    if (entryNum >= partition->numInsts) {
        DPRINTF(Crib, "[sn:%-12lli]:[executed:%i][status:%i][r1x:%i][r1r:%i][r1:%#x][break:%i][sf:%i][squash:%i][squashed:%i][loop:x][lf:x][ls:x][le:x][predicated:%i][mispred:%i]  Noop\n",
                0,
                status == Executed,
                status,
                inputs->rexec.test(1),
                inputs->ready.test(1),
                inputs->regs.readIntReg(1),
                inputs->flags.test(Break),
                setFlags.test(Squash),
                inputs->flags.test(Squash),
                status == Squashed,
                0,
                0);
    } else {
        DPRINTF(Crib, "[sn:%-12lli]:[executed:%i][status:%i][r1x:%i][r1r:%i][r1:%#x][break:%i][sf:%i][squash:%i][squashed:%i][loop:%i][lf:%i][ls:%i][le:%i][predicated:x][mispred:%i]%s\n",
                inst->seqNum,
                status == Executed,
                status,
                inputs->rexec.test(1),
                inputs->ready.test(1),
                inputs->regs.readIntReg(1),
                inputs->flags.test(Break),
                setFlags.test(Squash),
                inputs->flags.test(Squash),
                status == Squashed,
                inst->hasFallthrough(),
                inst->isLoopFail(),
                inst->isLoopStart(),
                inst->isLoopEnd(),
                //inst->readPredicate(),
                inst->mispredicted(),
                inst->staticInst->disassemble(inst->pcState().instAddr()).c_str());
    }
}
#include "arch/utility.hh"
#include "cpu/crib/cpu.hh"
#include "cpu/crib/crib.hh"
#include "debug/CommitInst.hh"
#include "debug/Crib.hh"
#include "debug/CribCommit.hh"
#include "debug/CribExecute.hh"
#include "debug/CribLoop.hh"
#include "debug/CribSpecial.hh"
#include "debug/Drain.hh"


    unsigned mapOpToResource(unsigned op) {
        switch(op) {
            case IntAluOp: return INT_ALU_RESOURCE;
            case IntMultOp: return INT_MUL_DIV_RESOURCE;
            case IntDivOp: return INT_MUL_DIV_RESOURCE;

            case FloatAddOp: return FP_ALU_RESOURCE;
            case FloatCmpOp: return FP_ALU_RESOURCE;
            case FloatCvtOp: return FP_DIV_RESOURCE;
            case FloatMultOp: return FP_ALU_RESOURCE;
            case FloatDivOp: return FP_DIV_RESOURCE;
            case FloatSqrtOp: return FP_DIV_RESOURCE;

            case SimdAddOp:
            case SimdAddAccOp:
            case SimdAluOp:
            case SimdCmpOp:
            case SimdCvtOp:
            case SimdMiscOp:
            case SimdMultOp:
            case SimdMultAccOp:
            case SimdShiftOp:
            case SimdShiftAccOp:
            case SimdSqrtOp:
            case SimdFloatAddOp:
            case SimdFloatAluOp:
            case SimdFloatCmpOp:
            case SimdFloatCvtOp:
            case SimdFloatDivOp:
            case SimdFloatMiscOp:
            case SimdFloatMultOp:
            case SimdFloatMultAccOp:
            case SimdFloatSqrtOp:
                  return SIMD_RESOURCE;
            default:
                  return INT_ALU_RESOURCE;
        }

    }



Crib::Crib(CribCPU *_cpu, CribLSQ *_lsq, CribCPUParams *params)
    : cpu(_cpu), lsq(_lsq), numPartitions(params->cribPartitions),
      partitionSize(params->cribPartitionSize),
      partitions(new CribPartition[numPartitions]),
      propagateDistance(params->cribPropagateDistance),
      dispatchToCribDelay(params->dispatchToCribDelay),
      cribLoopEnable(params->cribLoopEnable),
      squashTC(false),
      architectedPartition(0),
      allocPartition(0),
      retirePartition(0),
      status(Idle),
      squashing(false),
      squashed(false),
      ackSquashSeqNum(0),
      loopPartitions(0),
      drainPending(false),
      intALU(params->cribIntALU),
      intMulDiv(params->cribMulDiv),
      simd(params->cribSIMD),
      fpALU(params->cribFPALU),
      fpDiv(params->cribFPDiv)
{
    for (unsigned i = 0; i < numPartitions; i++) {
        partitions[i].init(this, lsq, partitionSize, params, i);
    }

    partitions[0].architected = true;
    enableCommitCheck = false;
}

void
Crib::startupStage()
{

}

void
Crib::takeOverFrom()
{
    status = Idle;
    squashing = false;
    squashed = false;
}

void
Crib::drain()
{
    drainPending = true;
}

void
Crib::drainResume()
{
    drainPending = false;
}

bool
Crib::isDrained() const
{
    if (!(instBuffer.empty() && skidBuffer.empty() && empty())) {
        return false;
    }

    for (unsigned i = 0; i < numPartitions; i++) {
        if (partitions[i].isAllocated()) {
            return false;
        }
    }

    return true;
}

inline std::string
Crib::name() const
{
    return cpu->name() + ".crib";
}

void
Crib::regStats()
{
    cribLoadOps
        .name(name() + ".loadOps")
        .desc("Number of issued load operations")
        .prereq(cribLoadOps);

    cribStoreOps
        .name(name() + ".storeOps")
        .desc("Number of issued store operations")
        .prereq(cribStoreOps);

    cribIntOps
        .name(name() + ".intOps")
        .desc("Number of issued int operations")
        .prereq(cribIntOps);

    cribMultOps
        .name(name() + ".multOps")
        .desc("Number of issued int operations")
        .prereq(cribMultOps);

    cribFPOps
        .name(name() + ".fpOps")
        .desc("Number of issued fp operations")
        .prereq(cribFPOps);

    cribComLoadOps
        .name(name() + ".comLoadOps")
        .desc("Number of committed load operations")
        .prereq(cribComLoadOps);

    cribComStoreOps
        .name(name() + ".comStoreOps")
        .desc("Number of committed store operations")
        .prereq(cribComStoreOps);

    cribComIntOps
        .name(name() + ".comIntOps")
        .desc("Number of committed int operations")
        .prereq(cribComIntOps);

    cribComFPOps
        .name(name() + ".comFPOps")
        .desc("Number of committed fp operations")
        .prereq(cribComFPOps);

    cribLoopMispredicted
        .name(name() + ".loopMispredicted")
        .desc("Number of mispredicted branches within loop partitions")
        .prereq(cribLoopMispredicted);

    cribLoopBreaks
        .name(name() + ".loopBreaks")
        .desc("Number of loop breaks handled")
        .prereq(cribLoopBreaks);

    cribLoopContinues
        .name(name() + ".loopContinues")
        .desc("Number of loop continues handled")
        .prereq(cribLoopContinues);

    totCommitInterval
        .name(name()+".totCommitInterval")
        .desc("Average time between commits.") 
        .prereq(totCommitInterval); 

    totCommitDistance
        .name(name()+".totCommitDistance")
        .desc("Average distance between commits.") 
        .prereq(totCommitDistance);

    nCommittedOps
        .name(name()+".nCommittedOps")
        .desc("total committed instructions")
        .prereq(nCommittedOps); 
}


void
Crib::setTimeBuffer(TimeBuffer<TimeStruct> *time_buffer)
{
    toDispatch = time_buffer->getWire(0);
}

void
Crib::setDispatchQueue(TimeBuffer<DispatchStruct> *dispatch_queue)
{
    fromDispatch = dispatch_queue->getWire(-dispatchToCribDelay);
}

void
Crib::tick()
{
    DPRINTF(Crib, "Running stage.\n");

    squashed = false;

    resources[INT_ALU_RESOURCE] = intALU;
    resources[INT_MUL_DIV_RESOURCE] = intMulDiv;
    resources[SIMD_RESOURCE] = simd;
    resources[FP_ALU_RESOURCE] = fpALU;
    resources[FP_DIV_RESOURCE] = fpDiv;
    
    if (squashTC) {
        doTCSquash();
        return;
    }

    // Parse incomming instructions
    dispatchInstructions();

    // Print some debugging info
#if TRACING_ON
    for (unsigned i = 0; i < numPartitions; i++) {
        partitions[i].print();
    }

    disassemble();
#endif

    for (unsigned i = 0; i < numPartitions; i++) {
        partitions[i].evaluate();
    }

    for (unsigned i = 0; i < propagateDistance; i++) {
        // Have every entry evaluate its inputs and decide its outputs
        for (unsigned j = 0; j < numPartitions; j++) {
            partitions[j].intraPropagate();
        }
        // Perform propagation within each partition
        for (unsigned j = 0; j < numPartitions; j++) {
            partitions[j].interPropagate();
        }
        // Perform propagation between partitions
        partitionPropagate();
    }

    // Branch detect
    removeSquashedPartitions();

    // Commit logic
    commitInstructions();
    resolveBranchMispredicts();

    // Blocking/stall/activity logic
    updateStatus();
}

inline void
Crib::partitionPropagate()
{
    for (unsigned i = 0; i < numPartitions; i++) {
        unsigned seq_num = (i+1) % numPartitions;
        CribPartition &cur_partition = partitions[i];
        CribPartition &seq_partition = partitions[seq_num];

        // Do propagate for the standard sequential case
        if (seq_num == architectedPartition || seq_num == retirePartition) {
            seq_partition.entries[0].inputs = new Propagated(seq_partition.latchRegs);
            if (seq_num == architectedPartition && !seq_partition.loopStart) {
                seq_partition.latchRexec |= cur_partition.entries[partitionSize-1].outputs->rexec;
            }
        } else {
            seq_partition.entries[0].inputs = cur_partition.entries[partitionSize-1].outputs;
        }

        // This is to guard feeding bad ready/values to a future loop iteration if the end hasn't been scheduled
        if (cribLoopEnable && seq_partition.loopStart && !seq_partition.architected && seq_partition.loopExecutions != 0) {
            seq_partition.entries[0].inputs->ready.reset();
            seq_partition.entries[0].inputs->rexec.reset();

        }
    }

    // Propagate from the loop end to the loop start
    if (cribLoopEnable) {
        for (unsigned i = 0; i < numPartitions; i++) {
            CribPartition &cur_partition = partitions[i];
            if (cur_partition.loopEnd) {
                unsigned loop_num = (i + numPartitions + 1 - cur_partition.loopNumPartitions) % numPartitions;
                CribPartition &loop_start = partitions[loop_num];
                if (loop_start.loopStart && loop_start.loopExecutions != 0) {
                    if (!loop_start.architected) {
                        delete loop_start.entries[0].inputs;
                        loop_start.entries[0].inputs =
                            new Propagated(cur_partition.entries[partitionSize-1].outputs->ready,
                                    cur_partition.entries[partitionSize-1].outputs->rexec,
                                    cur_partition.entries[partitionSize-1].outputs->regs);
                    } else {
                        loop_start.latchRexec |= cur_partition.entries[partitionSize-1].outputs->rexec;
                    }
                }
            }
        }
    }

    for (auto itr = pendingDelete.begin(); itr!=pendingDelete.end(); ++itr) {
        delete *itr;
    }
    pendingDelete.clear();

    for (unsigned i = 0; i < numPartitions; i++) {
        unsigned seq_num = (i+1) % numPartitions;
        CribPartition &cur_partition = partitions[i];
        if (seq_num == architectedPartition || seq_num == retirePartition) {
            pendingDelete.push_back(cur_partition.entries[partitionSize-1].outputs);
        }
    }

/*
    // Dont feed possibly changing/invalid values to instructions past the loop end
    if (cribLoopEnable) {
        for (unsigned i = 0; i < numPartitions; i++) {
            unsigned seq_num = (i+1) % numPartitions;
            CribPartition &cur_partition = partitions[i];
            CribPartition &seq_partition = partitions[seq_num];
            // Loop end clear valids
            if (cur_partition.loopEnd && !seq_partition.architected) {
                CribEntry& entry = cur_partition.entries[cur_partition.numInsts-1];
                if (!(entry.status == CribEntry::Status::Executed && !entry.inst->mispredicted())) {
                    seq_partition.entries[0].inputs->ready =
                        seq_partition.entries[0].inputs->ready  & ~entry.inst->loopWriteSet;
                }
            }
        }
    }
*/

}

bool
Crib::empty() const
{
    return (allocPartition == retirePartition) &&
           (allocPartition == architectedPartition) &&
            !partitions[allocPartition].isAllocated();
}

bool
Crib::full() const
{
    return partitions[allocPartition].isAllocated();
}


void
Crib::squashFromTC(ThreadID tid)
{
    squashTC = true;
}

inline void
Crib::dispatchInstructions()
{
    // Read whatever came from dispatch into the instruction queue
    int numInsts = fromDispatch->size;
    DPRINTF(Crib, "Receiving %i instructions from dispatch.\n", numInsts);

    if (squashing && fromDispatch->ackSquash[0] &&
            fromDispatch->ackSquashSeqNum[0] == ackSquashSeqNum) {
        squashing = false;
        DPRINTF(Crib, "Ending squash sequence [sn:%lli].\n", ackSquashSeqNum);
    }

    if (!squashing) {
        for (int i = 0; i < numInsts; i++) {
            instBuffer.push(fromDispatch->insts[i]);
        }
    }

    assert(skidBuffer.empty() || instBuffer.empty());
