#include "arch/decoder.hh"
#include "arch/kernel_stats.hh"
#include "cpu/crib/thread_context.hh"
#include "debug/CribCPU.hh"

CribThreadContext::CribThreadContext(CribCPU *_cpu, CribThreadState *_thread)
    : cpu(_cpu), thread(_thread)
{

}

void
CribThreadContext::activate(Cycles delay)
{
    DPRINTF(CribCPU, "Calling activate on Thread Context %d\n", threadId());

    if (thread->status() == ThreadContext::Active)
        return;

    thread->lastActivate = curTick();
    thread->setStatus(ThreadContext::Active);

    cpu->activateContext(thread->threadId(), delay);
}

void
CribThreadContext::suspend(Cycles delay)
{
    panic("Unimplemented function!");
}

void
CribThreadContext::halt(Cycles delay)
{
    panic("Unimplemented function!");
}

void
CribThreadContext::dumpFuncProfile()
{
    panic("Unimplemented function!");
}

void
CribThreadContext::takeOverFrom(ThreadContext *old_context)
{
    ::takeOverFrom(*this, *old_context);
    TheISA::Decoder *new_decoder = getDecoderPtr();
    TheISA::Decoder *old_decoder = old_context->getDecoderPtr();
    new_decoder->takeOverFrom(old_decoder);

    thread->kernelStats = old_context->getKernelStats();
    thread->funcExeInst = old_context->readFuncExeInst();

    thread->noSquashFromTC = false;
    thread->trapPending = false;
}

void
CribThreadContext::regStats(const std::string &name)
{
    if (FullSystem) {
        thread->kernelStats = new TheISA::Kernel::Statistics(cpu->system);
        thread->kernelStats->regStats(name + ".kern");
    }
}

void
CribThreadContext::serialize(std::ostream &os)
{
    panic("Unimplemented function!");
}

void
CribThreadContext::unserialize(Checkpoint *cp, const std::string &section)
{
    panic("Unimplemented function!");
}

Tick
CribThreadContext::readLastActivate()
{
    panic("Unimplemented function!");
    M5_DUMMY_RETURN
}

Tick
CribThreadContext::readLastSuspend()
{
    panic("Unimplemented function!");
    M5_DUMMY_RETURN
}

void
CribThreadContext::profileClear()
{
    panic("Unimplemented function!");
}

void
CribThreadContext::profileSample()
{
    panic("Unimplemented function!");
}

void
CribThreadContext::copyArchRegs(ThreadContext *tc)
{
    thread->noSquashFromTC = true;
    TheISA::copyRegs(tc, this);
    thread->noSquashFromTC = false;

    if (!FullSystem) {
        thread->funcExeInst = tc->readFuncExeInst();
    }
}

void
CribThreadContext::clearArchRegs()
{
    panic("Unimplemented function!");
}

IntReg
CribThreadContext::readIntReg(int reg_idx)
{
    reg_idx = cpu->isa[thread->threadId()]->flattenIntIndex(reg_idx);

    DPRINTF(CribCPU, "Thread Context reading integer register %i with %#lx\n", reg_idx,cpu->readArchIntReg(reg_idx, thread->threadId()) );
    return cpu->readArchIntReg(reg_idx, thread->threadId());
}

FloatReg
CribThreadContext::readFloatReg(int reg_idx)
{
    panic("Unimplemented function!");
    return 0;
}

FloatRegBits
CribThreadContext::readFloatRegBits(int reg_idx)
{
    panic("Unimplemented function!");
    return 0;
}

IntReg
CribThreadContext::readIntRegFlat(int reg_idx)
{
    DPRINTF(CribCPU, "Thread Context reading flat integer register %i with %#lx\n", reg_idx, cpu->readArchIntReg(reg_idx, thread->threadId()) );
    return cpu->readArchIntReg(reg_idx, thread->threadId());
}

FloatReg
CribThreadContext::readFloatRegFlat(int reg_idx)
{
    return cpu->readArchFloatReg(reg_idx, thread->threadId());
}

FloatRegBits
CribThreadContext::readFloatRegBitsFlat(int reg_idx)
{
    return cpu->readArchFloatRegBits(reg_idx, thread->threadId());
}

void
CribThreadContext::setIntReg(int reg_idx, IntReg val)
{
    reg_idx = cpu->isa[thread->threadId()]->flattenIntIndex(reg_idx);
    cpu->setArchIntReg(reg_idx, val, thread->threadId());

    DPRINTF(CribCPU, "Thread Context writing integer register %i with %#lx\n", reg_idx, val);

    conditionalSquash();
}

void
CribThreadContext::setFloatReg(int reg_idx, FloatReg val)
{
    panic("Unimplemented function!");
}

void
CribThreadContext::setFloatRegBits(int reg_idx, FloatRegBits val)
{
    panic("Unimplemented function!");
}

void
CribThreadContext::setIntRegFlat(int reg_idx, IntReg val)
{
    DPRINTF(CribCPU, "Thread Context %d writing flat integer register %i with %#lx\n",
            thread->threadId(), reg_idx, val);
    cpu->setArchIntReg(reg_idx, val, thread->threadId());
    conditionalSquash();
}

void
CribThreadContext::setFloatRegFlat(int reg_idx, FloatReg val)
{
    cpu->setArchFloatReg(reg_idx, val, thread->threadId());
    conditionalSquash();
}

void
CribThreadContext::setFloatRegBitsFlat(int reg_idx, FloatRegBits val)
{
    cpu->setArchFloatRegBits(reg_idx, val, thread->threadId());
    conditionalSquash();
}

TheISA::PCState
CribThreadContext::pcState()
{
    TheISA::PCState pc(cpu->pcState(thread->threadId()));
    DPRINTF(CribCPU, "Thread Context reading PCState %s\n", pc);
    return pc;
}

Addr
CribThreadContext::instAddr()
{
    Addr addr(cpu->instAddr(thread->threadId()));
    DPRINTF(CribCPU, "Thread Context reading instAddr %lx\n", addr);
    return addr;
}

Addr
CribThreadContext::nextInstAddr()
{
    Addr addr(cpu->nextInstAddr(thread->threadId()));
    DPRINTF(CribCPU, "Thread Context reading nextInstAddr %lx\n", addr);
    return addr;
}

void
CribThreadContext::pcState(const TheISA::PCState &val)
{
    DPRINTF(CribCPU, "Thread Context writing PCState with %s.\n", val);
    cpu->pcState(val, thread->threadId());

    conditionalSquash();
}

void
CribThreadContext::pcStateNoRecord(const TheISA::PCState &val)
{
    panic("Unimplemented function!");
}

void
CribThreadContext::setMiscRegNoEffect(int misc_reg, const MiscReg &val)
{
    cpu->setMiscRegNoEffect(misc_reg, val, thread->threadId());
    conditionalSquash();
}

void
CribThreadContext::setMiscReg(int misc_reg, const MiscReg &val)
{
    cpu->setMiscReg(misc_reg, val, thread->threadId());
    conditionalSquash();
}

int
CribThreadContext::flattenIntIndex(int reg)
{
    panic("Unimplemented function!");
    return 0;
}

int
CribThreadContext::flattenFloatIndex(int reg)
{
    panic("Unimplemented function!");
    return 0;
}


int
CribThreadContext::exit()
{
    panic("Unimplemented function!");
    return 0;
}
