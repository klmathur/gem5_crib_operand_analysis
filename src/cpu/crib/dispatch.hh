#ifndef __CPU_CRIB_DISPATCH_HH__
#define __CPU_CRIB_DISPATCH_HH__

#include "cpu/crib/comm.hh"
#include "params/CribCPU.hh"

class CribCPU;

class CribDispatch
{
  public:
    CribDispatch(CribCPU *_cpu, CribCPUParams *params);
    std::string name() const;
    void regStats();

    void setTimeBuffer(TimeBuffer<TimeStruct> *time_buffer);
    void setDecodeQueue(TimeBuffer<DecodeStruct> *decode_queue);
    void setDispatchQueue(TimeBuffer<DispatchStruct> *dispatch_queue);


    bool isDrained() const;
    void takeOverFrom();

    void tick();

  private:
    enum DispatchStatus {
        Active,
        Inactive
    };

    enum ThreadStatus {
        Running,
        Idle,
        Squashing,
        Blocked,
        Unblocking
    };

    DispatchStatus status;
    ThreadStatus threadStatus;

    CribCPU *cpu;
    unsigned partitionSize;
    unsigned loadsPerPartition;
    unsigned storesPerPartition;
    unsigned cribPartitions;

    unsigned cribToDispatchDelay;
    unsigned commitToDispatchDelay;
    unsigned decodeToDispatchDelay;
    unsigned cribCommitWidth;

    InstSeqNum ackSquashSeqNum;
    Addr ackPcAddr;

    bool inLoopDispatch;
    unsigned loopPartitions;

    bool serializing;
    bool serializeAfter;
    InstSeqNum serializeSeqNum;

    std::queue<CribDynInstPtr> insts;
    std::queue<CribDynInstPtr> skidBuffer;

    struct Stalls {
        bool crib;
        bool commit;
    };

    Stalls stalls;

    TimeBuffer<DecodeStruct>::wire fromDecode;
    TimeBuffer<TimeStruct>::wire fromCrib;
    TimeBuffer<TimeStruct>::wire fromCommit;
    TimeBuffer<TimeStruct>::wire toDecode;
    TimeBuffer<DispatchStruct>::wire toCrib;

    bool wroteToTimeBuffer;

    Stats::Scalar dispatchIdleCycles;
    Stats::Scalar dispatchBlockedCycles;
    Stats::Scalar dispatchUnblockCycles;
    Stats::Scalar dispatchSquashCycles;
    Stats::Scalar dispatchRunCycles;
    Stats::Distribution dispatchInstDist;
    Stats::Scalar dispatchLoopSuccesses;
    Stats::Scalar dispatchLoopFailures;
    Stats::Scalar dispatchLoopSizeFailures;
    Stats::Formula dispatchLoopSuccessRate;

  private:
    // Helper functions
    void dispatch(bool &status_change);
    bool checkSignalsAndUpdate();
    void readStallSignals();
    void squash();
    bool checkStall();
    bool block();
    void skidInsert();
    void sortInstructions();
    void dispatchInstructions();
    bool unblock();
    void updateStatus();

};

#endif // __CPU_CRIB_DISPATCH_HH__
