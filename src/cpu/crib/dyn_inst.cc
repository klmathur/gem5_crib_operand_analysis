#include "cpu/crib/cpu.hh"
#include "cpu/crib/crib.hh"
#include "cpu/crib/dyn_inst.hh"
#include "debug/CribExecute.hh"
#include "debug/CribPrefetch.hh"

CribDynInst::CribDynInst(StaticInstPtr static_inst, StaticInstPtr macro_op,
                         TheISA::PCState _pc, TheISA::PCState pred_pc,
                         InstSeqNum seq_num, CribCPU* _cpu)
    : staticInst(static_inst),
      macroop(macro_op),
      pc(_pc),
      origPC(_pc),
      predTarget(pred_pc),
      seqNum(seq_num),
      loopColor(0),
      loopItrCount(0),
      fault(NoFault),
      cpu(_cpu),
      mispredictSignaled(false),
      loopMispredExtra(true),
      commitFault(false),
      commitFaultCycles(0),
      storeConditionalComplete(false),
      loopStart(false),
      loopEnd(false),
      loopUnrollEnd(false),
      fallthrough(false),
      loopFail(false),
      failedBreak(false),
      breakAcked(false),
      prefetchOutstanding(false),
      prefetchAddrValid(false),
      prefetchStrideValid(false),
      prefetchDataValid(false),
      prefetchDataStrideValid(false),
      prefetchDataStrideVerified(false)
{
    flags[Predicate] = true;
#if TRACING_ON
    fetchTick = -1;
    decodeTick = -1;
    dispatchTick = -1;
    completeTick = -1;
    retireTick = -1;
#endif
}

CribDynInst::CribDynInst(const CribDynInst& oi){
    CribDynInst(oi.staticInst, oi.macroop, oi.pc, oi.predTarget, oi.seqNum, oi.cpu);
}


IntReg
CribDynInst::readIntRegOperand(const StaticInst *si, int idx)
{
    int real_idx = cpu->isa[threadNumber]->flattenIntIndex(staticInst->srcRegIdx(idx));
    IntReg val = cribEntry->inputs->regs.readIntReg(real_idx);
	//printf("Here\n");	
    DPRINTF(CribExecute, "Inst %s\t-\tReading int register %i containing %#x.\n", staticInst->disassemble(pc.instAddr()).c_str(), real_idx, val);

    return val;
}

FloatReg
CribDynInst::readFloatRegOperand(const StaticInst *si, int idx)
{
    int real_idx = (staticInst->srcRegIdx(idx) - TheISA::FP_Base_DepTag) + TheISA::NumIntRegs;
    FloatReg val = cribEntry->inputs->regs.readFloatReg(real_idx);

    DPRINTF(CribExecute, "Inst %s\t-\tReading float register %i containing %g.\n", staticInst->disassemble(pc.instAddr()).c_str(), real_idx, val);
    return val;
}

FloatRegBits
CribDynInst::readFloatRegOperandBits(const StaticInst *si, int idx)
{
    int real_idx = (staticInst->srcRegIdx(idx)- TheISA::FP_Base_DepTag) + TheISA::NumIntRegs;
    FloatRegBits val = cribEntry->inputs->regs.readFloatRegBits(real_idx);

    DPRINTF(CribExecute, "Inst %s\t-\tReading fb register %i containing %g.\n", staticInst->disassemble(pc.instAddr()).c_str(), real_idx, val);
    return val;
}

void
CribDynInst::setIntRegOperand(const StaticInst *si, int idx, IntReg val)
{
    RegResult result;
    result.index = cpu->isa[threadNumber]->flattenIntIndex(staticInst->destRegIdx(idx));
    result.type = ResultInt;
    result.reg.i = val;


    DPRINTF(CribExecute, "Writing int register %i with %#x.\n",  result.index, result.reg.i);
    cribEntry->results[cribEntry->destRegMap[idx]] = result;
}

void
CribDynInst::setFloatRegOperand(const StaticInst *si, int idx, FloatReg val)
{
    RegResult result;
    result.index = (staticInst->destRegIdx(idx) - TheISA::FP_Base_DepTag) + TheISA::NumIntRegs;
    result.type = ResultFloat;
    result.reg.f = val;


    DPRINTF(CribExecute, "Writing f register %i with %g.\n",  result.index, result.reg.f);
    cribEntry->results[cribEntry->destRegMap[idx]] = result;
}

void
CribDynInst::setFloatRegOperandBits(const StaticInst *si, int idx, FloatRegBits val)
{
    RegResult result;
    result.index = (staticInst->destRegIdx(idx)- TheISA::FP_Base_DepTag) + TheISA::NumIntRegs;
    result.type = ResultFloatBits;
    result.reg.fb = val;


    DPRINTF(CribExecute, "Writing fb register %i with %g.\n",  result.index, result.reg.fb);
    cribEntry->results[cribEntry->destRegMap[idx]] = result;
}

MiscReg
CribDynInst::readMiscReg(int misc_reg)
{
    panic("Unimplemented function!");
    M5_DUMMY_RETURN
}

MiscReg
CribDynInst::readMiscRegNoEffect(int misc_reg)
{
    panic("Unimplemented function!");
    M5_DUMMY_RETURN
}

MiscReg
CribDynInst::readMiscRegOperand(const StaticInst *si, int idx)
{
    return cpu->readMiscReg(si->srcRegIdx(idx) - TheISA::Ctrl_Base_DepTag,
        threadNumber);
}

MiscReg
CribDynInst::readMiscRegOperandNoEffect(const StaticInst *si, int idx)
{
    panic("Unimplemented function!");
    M5_DUMMY_RETURN
}

void
CribDynInst::setMiscReg(int misc_reg, const MiscReg& val)
{
    panic("Unimplemented function!");
}

void
CribDynInst::setMiscRegNoEffect(int misc_reg, const MiscReg& val)
{
    panic("Unimplemented function!");
}

void
CribDynInst::setMiscRegOperand(const StaticInst *si, int idx,
        const MiscReg& val)
{
    cpu->setMiscReg(si->destRegIdx(idx) - TheISA::Ctrl_Base_DepTag,
        val, threadNumber);
}

void
CribDynInst::setMiscRegOperandNoEffect(const StaticInst *si, int idx,
        const MiscReg& val)
{
    panic("Unimplemented function!");
}

void
CribDynInst::setPredicate(bool val)
{
    flags[Predicate] = val;
}

const PCState &
CribDynInst::pcState() const
{
    return pc;
}

void
CribDynInst::pcState(const PCState& _pc)
{
    DPRINTF(CribExecute, "Inst %s\t-\tWriting PCState (%s).\n",
            staticInst->disassemble(pc.instAddr()).c_str(),  _pc);

    pc = _pc;
}

ThreadContext *
CribDynInst::tcBase()
{
    return cpu->tcBase(threadNumber);
}

Fault
CribDynInst::readMem(Addr addr, uint8_t *data, unsigned size, unsigned flags)
{
    bool successful_prefetch = false;
    if (prefetchOutstanding) {
        successful_prefetch = cribEntry->lsq->validatePrefetch(this, addr, size, flags);
    }

    if (fallthrough && cpu->cribStridePrefetchEnable) {
        if (prefetchAddrValid) {
            if (prefetchStrideValid && !prefetchOutstanding) {
                if (addr - prefetchAddr == prefetchStride) {
                    DPRINTF(CribPrefetch, "Prefetch stride match. "
                            "[addr:%#lx][stride:%#lx].\n", addr, prefetchStride);
                    DPRINTF(CribPrefetch, "Generating prefetch [addr:%#lx].\n", addr + prefetchStride);
                    if (prefetchStride == 0) {
                        cpu->constantPrefetch++;
                    } else {
                        cpu->stridePrefetch++;
                    }
                    
                    cribEntry->lsq->initiatePrefetch(this, addr + prefetchStride, size, flags);
                    prefetchOutstanding = true;
                } else {
                    DPRINTF(CribPrefetch, "Prefetch stride mismatch. "
                            "[addr:%#lx][stride:%#lx][predicted stride:%#lx].\n",
                            addr, addr-prefetchStride, prefetchStride);
                }
            }
            prefetchStrideValid = true;
            prefetchStride = addr - prefetchAddr;
        }
        prefetchAddrValid = true;
        prefetchAddr = addr;
    }

    if (fallthrough && prefetchDataValid) {
        if (prefetchDataStrideValid) {
            if (prefetchDataStride == addr - prefetchDataValue) {
                prefetchDataStrideVerified = true;
                DPRINTF(CribPrefetch, "Verified stride %#x\n", prefetchDataStride);

            } else {
                prefetchDataStrideVerified = false;
            }
        }

        prefetchDataStrideValid = true;
        prefetchDataStride = addr - prefetchDataValue;
    }



    if (!successful_prefetch) {
        cribEntry->lsq->initiateRead(this, addr, size, flags);
    }
    memAddr = addr;
    memSize = size;
    return NoFault;
}

Fault
CribDynInst::writeMem(uint8_t *data, unsigned size, Addr addr, unsigned flags,
        uint64_t *res)
{
    cribEntry->lsq->initiateWrite(this, addr, size, flags, data);
    memAddr = addr;
    memSize = size;
    return NoFault;
}

void
CribDynInst::syscall(int64_t callnum)
{
    threadState->syscall(callnum);
}

unsigned
CribDynInst::readStCondFailures()
{
    panic("Unimplemented function!");
    M5_DUMMY_RETURN
}

void
CribDynInst::setStCondFailures(unsigned sc_failures)
{
    panic("Unimplemented function!");
}

int
CribDynInst::contextId()
{
    panic("Unimplemented function!");
    M5_DUMMY_RETURN
}



/** Unimplemented function!/unused functionality */

IntReg
CribDynInst::readRegOtherThread(unsigned idx, ThreadID tid)
{
    panic("CribCPU does not support multithreaded register access");
    M5_DUMMY_RETURN
}

void
CribDynInst::setRegOtherThread(unsigned idx, IntReg &val, ThreadID tid)
{
    panic("CribCPU does not support multithreaded register access");
}

Addr
CribDynInst::getEA()
{
    panic("gem5 doesn't support split address/data issue - pointless stub");
    M5_DUMMY_RETURN
}

void
CribDynInst::setEA(Addr EA)
{
    panic("gem5 doesn't support split address/data issue - pointless stub");
}

Fault
CribDynInst::hwrei()
{
    panic("CribCPU currently doesn't support this Alpha operation");
    M5_DUMMY_RETURN
}

bool
CribDynInst::simPalCheck(int pal_func)
{
    panic("CribCPU currently doesn't support this Alpha operation");
    M5_DUMMY_RETURN
}

RegSet
CribDynInst::srcRegSet()
{
    RegSet ret;
    int8_t numSrcRegs = staticInst->numSrcRegs();
    for (int8_t i = 0; i < numSrcRegs; i++) {
        int reg = staticInst->srcRegIdx(i);
        if (reg < TheISA::FP_Base_DepTag) {
            reg = cpu->isa[threadNumber]->flattenIntIndex(reg);
        } else if (reg < TheISA::Ctrl_Base_DepTag) {
            // Float register
            reg = (reg - TheISA::FP_Base_DepTag) + TheISA::NumIntRegs;
        } else {
            // Control register, dont mark dependency
            continue;
        }
#if THE_ISA == ARM_ISA
        if (reg != TheISA::INTREG_PC) {
            ret.set(reg);
        }
#else
        ret.set(reg);
#endif
    }

    // ZeroReg always ready
    ret.reset(TheISA::ZeroReg);
    return ret;
}

RegSet
CribDynInst::destRegSet(unsigned *dest_map)
{
    RegSet ret;
    int8_t numDestRegs = staticInst->numDestRegs();
    int8_t written = 0;
    for (int8_t i = 0; i < numDestRegs; i++) {
        int reg = staticInst->destRegIdx(i);
        if (reg < TheISA::FP_Base_DepTag) {
            // Do nothing, int register
            reg = cpu->isa[threadNumber]->flattenIntIndex(reg);
        } else if (reg < TheISA::Ctrl_Base_DepTag) {
            // Float register
            reg = (reg - TheISA::FP_Base_DepTag) + TheISA::NumIntRegs;
        } else {
            // Control register, dont mark dependency
            continue;
        }

#if THE_ISA == ARM_ISA
        if (reg != TheISA::INTREG_PC) {
            ret.set(reg);
        }
#else
        ret.set(reg);
#endif
        dest_map[i] = written++;
    }

    // ZeroReg never written
    ret.reset(TheISA::ZeroReg);
    return ret;
}

RegSet
CribDynInst::destRegSet()
{
    RegSet ret;
    int8_t numDestRegs = staticInst->numDestRegs();
    for (int8_t i = 0; i < numDestRegs; i++) {
        int reg = staticInst->destRegIdx(i);
        if (reg < TheISA::FP_Base_DepTag) {
            // Do nothing, int register
            reg = cpu->isa[threadNumber]->flattenIntIndex(reg);
        } else if (reg < TheISA::Ctrl_Base_DepTag) {
            // Float register
            reg = (reg - TheISA::FP_Base_DepTag) + TheISA::NumIntRegs;
        } else {
            // Control register, dont mark dependency
            continue;
        }

#if THE_ISA == ARM_ISA
        if (reg != TheISA::INTREG_PC) {
            ret.set(reg);
        }
#else
        ret.set(reg);
#endif
    }

    // ZeroReg never written
    ret.reset(TheISA::ZeroReg);
    return ret;
}

Fault
CribDynInst::execute()
{
    bool no_squash_from_tc = threadState->noSquashFromTC;
    threadState->noSquashFromTC = true;

    fault = staticInst->execute(this, traceData);

    threadState->noSquashFromTC = no_squash_from_tc;

    return fault;
}

Fault
CribDynInst::initiateAcc()
{
    bool no_squash_from_tc = threadState->noSquashFromTC;
    threadState->noSquashFromTC = true;

    fault = staticInst->initiateAcc(this, traceData);

    threadState->noSquashFromTC = no_squash_from_tc;

    return fault;
}

Fault
CribDynInst::completeAcc(PacketPtr pkt)
{
    bool no_squash_from_tc = threadState->noSquashFromTC;
    threadState->noSquashFromTC = true;

    // Prefetch check for data ptr
    if (fallthrough && cpu->cribPointerPrefetchEnable && isLoad() && pkt->getSize() == 4)  {
        prefetchDataValid = true;
        prefetchDataValue = *(pkt->getPtr<uint32_t>());

        if (prefetchDataStrideVerified && !prefetchOutstanding) {
            cpu->pointerPrefetch++;
            cribEntry->lsq->initiatePrefetch(this, prefetchDataValue + prefetchDataStride, 4, pkt->req->getFlags());
            prefetchOutstanding = true;
        }
    }

    fault = staticInst->completeAcc(pkt, this, traceData);

    threadState->noSquashFromTC = no_squash_from_tc;

    cribEntry->markExecuted();

    return fault;
}

std::string
CribDynInst::name() const
{
    return cribEntry->name();
}

std::string
CribDynInst::print() const
{
    std::stringstream ss;
    ss << staticInst->disassemble(pcState().instAddr()).c_str() << std::endl
       << "isNop: " << isNop() << std::endl
       << "isMemRef: " << isMemRef() << std::endl
       << "isLoad: " << isLoad() << std::endl
       << "isStore: " << isStore() << std::endl
       << "isStoreConditional: " << isStoreConditional() << std::endl
       << "isInstPrefetch: " << isInstPrefetch() << std::endl
       << "isDataPrefetch: " << isDataPrefetch() << std::endl
       << "isInteger: " << isInteger() << std::endl
       << "isFloating: " << isFloating() << std::endl
       << "isControl: " << isControl() << std::endl
       << "isCall: " << isCall() << std::endl
       << "isReturn: " << isReturn() << std::endl
       << "isDirectCtrl: " << isDirectCtrl() << std::endl
       << "isIndirectCtrl: " << isIndirectCtrl() << std::endl
       << "isCondCtrl: " << isCondCtrl() << std::endl
       << "isUncondCtrl: " << isUncondCtrl() << std::endl
       << "isCondDlaySlot: " << isCondDelaySlot() << std::endl
       << "isThreadSync: " << isThreadSync() << std::endl
       << "isSerializing: " << isSerializing() << std::endl
       << "isSerializeBefore: " << isSerializeBefore() << std::endl
       << "isSerializeAfter: " << isSerializeAfter() << std::endl
       << "isSquashAfter: " << isSquashAfter() << std::endl
       << "isMemBarrier: " << isMemBarrier() << std::endl
       << "isWriteBarrier: " << isWriteBarrier() << std::endl
       << "isNonSpeculative: " << isNonSpeculative() << std::endl
       << "isQuiesce: " << isQuiesce() << std::endl
       << "isIprAccess: " << isIprAccess() << std::endl
       << "isUnverifiable: " << isUnverifiable() << std::endl
       << "isSyscall: " << isSyscall() << std::endl
       << "isMicroop: " << isMicroop() << std::endl
       << "isLastMicrop: " << isLastMicroop() << std::endl
       << "isDelayedCommit: " << isDelayedCommit() << std::endl;

    return ss.str();
}
