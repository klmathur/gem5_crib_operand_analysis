#ifndef __CPU_CRIB_DYN_INST_HH
#define __CPU_CRIB_DYN_INST_HH

#include <bitset>
#include <memory>

#include "arch/isa.hh"
#include "base/trace.hh"
#include "cpu/crib/regstate.hh"
#include "cpu/crib/thread_state.hh"
#include "cpu/inst_seq.hh"
#include "cpu/static_inst.hh"
#include "debug/PipeView.hh"

struct CribSeqNum {
    CribSeqNum(uint64_t color, uint64_t count, InstSeqNum seq_num)
        : loopColor(color), loopItrCount(count), seqNum(seq_num)
    {}

    CribSeqNum() : loopColor(0), loopItrCount(0), seqNum(0) { }

    uint64_t loopColor;
    uint64_t loopItrCount;
    InstSeqNum seqNum;

    bool operator>(const CribSeqNum &other) const {
        if (loopColor > other.loopColor) return true;
        else if (loopColor == other.loopColor) {
            if (loopItrCount > other.loopItrCount) return true;
            else if (loopItrCount == other.loopItrCount) {
                return seqNum > other.seqNum;
            }
        }
        return false;
    }

    bool operator<(const CribSeqNum &other) const {
        if (loopColor < other.loopColor) return true;
        else if (loopColor == other.loopColor) {
            if (loopItrCount < other.loopItrCount) return true;
            else if (loopItrCount == other.loopItrCount) {
                return seqNum < other.seqNum;
            }
        }
        return false;
    }
    bool operator==(const CribSeqNum &other) const {
        return (seqNum == other.seqNum) &&
               (loopItrCount == other.loopItrCount) &&
               (loopColor == other.loopColor);
    }
    bool operator!=(const CribSeqNum &other) const {
        return (seqNum != other.seqNum) ||
               (loopItrCount != other.loopItrCount) ||
               (loopColor != other.loopColor);
    }
};


class CribEntry;

class CribDynInst: public RefCounted
{
  public:
    // Required Functionality
    typedef TheISA::IntReg IntReg;
    typedef TheISA::FloatReg FloatReg;
    typedef TheISA::FloatRegBits FloatRegBits;
    typedef TheISA::MiscReg MiscReg;
    typedef TheISA::PCState PCState;

    CribDynInst(StaticInstPtr staticInst, StaticInstPtr macro_op,
            TheISA::PCState _pc, TheISA::PCState pred_pc,
            InstSeqNum seq_num, CribCPU* _cpu);
    CribDynInst(const CribDynInst& oi);

    IntReg readIntRegOperand(const StaticInst *si, int idx);
    FloatReg readFloatRegOperand(const StaticInst *si, int idx);
    FloatRegBits readFloatRegOperandBits(const StaticInst *si, int idx);
    void setIntRegOperand(const StaticInst *si, int idx, IntReg val);
    void setFloatRegOperand(const StaticInst *si, int idx, FloatReg val);
    void setFloatRegOperandBits(const StaticInst *si, int idx, FloatRegBits val);

    MiscReg readMiscReg(int misc_reg);
    MiscReg readMiscRegNoEffect(int misc_reg);
    MiscReg readMiscRegOperand(const StaticInst *si, int idx);
    MiscReg readMiscRegOperandNoEffect(const StaticInst *si, int idx);
    void setMiscReg(int misc_reg, const MiscReg& val);
    void setMiscRegNoEffect(int misc_reg, const MiscReg& val);
    void setMiscRegOperand(const StaticInst *si, int idx, const MiscReg &val);
    void setMiscRegOperandNoEffect(const StaticInst *si, int idx, const MiscReg &val);
    void setPredicate(bool val);


    const PCState &pcState() const;
    void pcState(const PCState &_pc);

    ThreadContext *tcBase();
    Fault readMem(Addr addr, uint8_t *data, unsigned size, unsigned flags);
    Fault writeMem(uint8_t *data, unsigned size, Addr addr, unsigned flags,
        uint64_t *res);

    unsigned readStCondFailures();
    void setStCondFailures(unsigned sc_failures);

    int contextId();

    /** Pointer to the actual instruction */
    StaticInstPtr staticInst;

    // Required for x86 (from the exec context, arm doesnt rely on this call path)
    void syscall(int64_t callnum);

    // Required for x86
    void demapPage(Addr addr, uint64_t asn) { }


    /** Unimplemented function!/unused functionality */
    IntReg readRegOtherThread(unsigned idx, ThreadID tid = InvalidThreadID);
    void setRegOtherThread(unsigned idx, IntReg &val, ThreadID tid = InvalidThreadID);
    Addr getEA();
    void setEA(Addr EA);
    Fault hwrei();
    bool simPalCheck(int pal_func);


    //////////////////////////////////////////////////////////////////////////
    //
    //  INSTRUCTION TYPES - Forward checks to StaticInst object.
    //
    //////////////////////////////////////////////////////////////////////////
    bool isNop()             const { return staticInst->isNop(); }
    bool isMemRef()          const { return staticInst->isMemRef(); }
    bool isLoad()            const { return staticInst->isLoad(); }
    bool isStore()           const { return staticInst->isStore(); }
    bool isStoreConditional() const
    { return staticInst->isStoreConditional(); }
    bool isInstPrefetch()    const { return staticInst->isInstPrefetch(); }
    bool isDataPrefetch()    const { return staticInst->isDataPrefetch(); }
    bool isInteger()         const { return staticInst->isInteger(); }
    bool isFloating()        const { return staticInst->isFloating(); }
    bool isControl()         const { return staticInst->isControl(); }
    bool isCall()            const { return staticInst->isCall(); }
    bool isReturn()          const { return staticInst->isReturn(); }
    bool isDirectCtrl()      const { return staticInst->isDirectCtrl(); }
    bool isIndirectCtrl()    const { return staticInst->isIndirectCtrl(); }
    bool isCondCtrl()        const { return staticInst->isCondCtrl(); }
    bool isUncondCtrl()      const { return staticInst->isUncondCtrl(); }
    bool isCondDelaySlot()   const { return staticInst->isCondDelaySlot(); }

    bool isThreadSync()      const { return staticInst->isThreadSync(); }
    bool isSerializing()     const { return staticInst->isSerializing(); }
    bool isSerializeBefore() const
    { return staticInst->isSerializeBefore() || flags[SerializeBefore]; }
    bool isSerializeAfter()  const
    { return staticInst->isSerializeAfter() || flags[SerializeAfter]; }
    bool isSquashAfter()     const { return staticInst->isSquashAfter(); }
    bool isMemBarrier()      const { return staticInst->isMemBarrier(); }
    bool isWriteBarrier()    const { return staticInst->isWriteBarrier(); }
    bool isNonSpeculative()  const { return staticInst->isNonSpeculative(); }
    bool isQuiesce()         const { return staticInst->isQuiesce(); }
    bool isIprAccess()       const { return staticInst->isIprAccess(); }
    bool isUnverifiable()    const { return staticInst->isUnverifiable(); }
    bool isSyscall()         const { return staticInst->isSyscall(); }
    bool isMicroop()         const { return staticInst->isMicroop(); }
    bool isLastMicroop()     const { return staticInst->isLastMicroop(); }
    bool isDelayedCommit()   const { return staticInst->isDelayedCommit(); }

    StaticInstPtr macroop;

    TheISA::PCState pc;
    TheISA::PCState origPC;
    TheISA::PCState predTarget;

    InstSeqNum seqNum;
    uint64_t loopColor;
    uint64_t loopItrCount;

    void setTid(ThreadID tid) { threadNumber = tid; }
    void setASID(ThreadID tid) { threadNumber = tid; }
    void setThreadState(CribThreadState* thread) { threadState = thread; }
    Trace::InstRecord *traceData;
    void setPredTarg(TheISA::PCState target) { predTarget = target; }
    void setPredTaken(bool taken) { flags[PredTaken] = taken; }
    bool readPredTaken() { return flags[PredTaken]; }
    TheISA::PCState readPredTarg() { return predTarget; }
    TheISA::PCState branchTarget() { return staticInst->branchTarget(pc); }

    Fault fault;
    ThreadID threadNumber;
    CribThreadState* threadState;
    // Things I need
    RegSet srcRegSet();
    RegSet destRegSet(unsigned *dest_map);
    RegSet destRegSet();
    CribCPU * cpu;

    enum Flags {
        PredTaken,
        SerializeBefore,
        SerializeAfter,
        Predicate,
        NumFlags
    };

    std::bitset<NumFlags> flags;

    void setCribEntry(CribEntry* crib_entry) { cribEntry = crib_entry; }
    void setStoreConditionalComplete() { storeConditionalComplete = true; }
    bool isStoreConditionalComplete() { return storeConditionalComplete; }

    void setFailedLoopBreak() {
        failedBreak = true;
    }

    void setFallthrough(Addr fallthrough_addr) {
        fallthrough = true;
        fallthroughAddr = fallthrough_addr;
    }

    void setStartAddr(Addr start) {
        startAddr = start;
    }

    Addr getStartAddr() const {
        return startAddr;
    }

    bool hasFallthrough() {
        return fallthrough;
    }

    Addr getFallthroughAddr() {
        return fallthroughAddr;
    }

    void setLoopStart() {
        loopStart = true;
    }

    bool isLoopStart() {
        return loopStart;
    }

    void setLoopEnd(const RegSet &w_set) {
        loopEnd = true;
        loopWriteSet = w_set;
    }

    void setLoopUnrollEnd() {
        loopUnrollEnd = true;
    }

    bool isLoopUnrollEnd() {
        return loopUnrollEnd;
    }

    bool isLoopEnd() {
        return loopEnd;
    }

    bool branchesToFallthrough() {
        if (!fallthrough) return false;
        else {
            TheISA::PCState nextPC = pc;
            TheISA::advancePC(nextPC, staticInst);
            return (nextPC.instAddr() == fallthroughAddr);
        }
    }

    bool isLoopBreak() {
        return mispredicted() && hasFallthrough() && !isLoopFail() &&
            getFallthroughAddr() == pcState().instNPC() && !failedBreak;
    }

    bool isLoopContinue() {
        return hasFallthrough() && !isLoopFail() &&
               getStartAddr() == pcState().instNPC();
    }

    bool isLoopFail() {
        return loopFail || !hasFallthrough();
    }
    void setLoopFail() {
        loopFail = true;
    }


    unsigned getUnrollFactor() const {
        return unrollFactor;
    }

    void setUnrollFactor(unsigned factor) {
        unrollFactor = factor;
    }

    CribSeqNum getCribSeqNum() {
        return CribSeqNum(loopColor, loopItrCount, seqNum);
    }

    CribEntry *cribEntry;

    Fault execute();
    Fault initiateAcc();
    Fault completeAcc(PacketPtr pkt);

    std::string name() const;

    bool mispredicted() const
    {
        TheISA::PCState tempPC = pc;
        TheISA::advancePC(tempPC, staticInst);
        return !(tempPC == predTarget);
    }

    bool readPredicate() const { return flags[Predicate]; }

    void printPipeView()
    {
#if TRACING_ON
        Tick val, fetch = this->fetchTick;
        // Print info needed by the pipeline activity viewer.
        DPRINTFR(PipeView, "PipeView:fetch:%llu:0x%08llx:%d:%llu:%s\n",
                fetch,
                pc.instAddr(),
                pc.microPC(),
                seqNum,
                staticInst->disassemble(pc.instAddr()));
        val = (this->decodeTick == -1) ? 0 : decodeTick;
        DPRINTFR(PipeView, "PipeView:decode:%llu\n", val);
        val = (this->dispatchTick == -1) ? 0 : dispatchTick;
        DPRINTFR(PipeView, "PipeView:dispatch:%llu\n", val);
        val = (this->issueTick == -1) ? 0 : issueTick;
        DPRINTFR(PipeView, "PipeView:issue:%llu\n", val);
        val = (this->completeTick == -1) ? 0 : completeTick;
        DPRINTFR(PipeView, "PipeView:complete:%llu\n", val);
        val = (this->retireTick == -1) ? 0 : retireTick;
        DPRINTFR(PipeView, "PipeView:retire:%llu\n", val);
#endif
    }

    std::string print() const;
    Addr memAddr;
    unsigned memSize;
    bool mispredictSignaled;
    bool loopMispredExtra;
    bool commitFault;
    unsigned commitFaultCycles;

    bool storeConditionalComplete;
    bool loopStart;
    RegSet loopWriteSet;
    bool loopEnd;
    bool loopUnrollEnd;
    bool fallthrough;
    Addr fallthroughAddr;
    Addr startAddr;
    bool loopFail;
    bool failedBreak;
    bool breakAcked;
    unsigned unrollFactor;
#if TRACING_ON
    Tick fetchTick;
    Tick decodeTick;
    Tick dispatchTick;
    Tick issueTick;
    Tick completeTick;
    Tick retireTick;
#endif

    bool prefetchOutstanding;
    bool prefetchAddrValid;
    Addr prefetchAddr;
    bool prefetchStrideValid;
    Addr prefetchStride;


    bool prefetchDataValid;
    Addr prefetchDataValue;

    bool prefetchDataStrideValid;
    Addr prefetchDataStride;

    bool prefetchDataStrideVerified;
    void reset(){
        mispredictSignaled = false; 
        commitFault = false;
        commitFaultCycles = 0;
        storeConditionalComplete = false;
        prefetchOutstanding = false;
        prefetchAddrValid = false;
        prefetchStrideValid = false;
        prefetchDataValid = false;
        prefetchDataStrideValid = false;
        setPredicate(true);
        pc = origPC;
    }
};

typedef RefCountingPtr<CribDynInst> CribDynInstPtr;
//typedef std::shared_ptr<CribDynInst> CribDynInstPtr;

#endif // __CPU_CRIB_DYN_INST_HH
