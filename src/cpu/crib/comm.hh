#ifndef __CPU_CRIB_COMM_HH__
#define __CPU_CRIB_COMM_HH__

#include "cpu/crib/const.hh"
#include "cpu/crib/dyn_inst.hh"
#include "cpu/inst_seq.hh"

struct FetchStruct {
    int size;

    CribDynInstPtr insts[CribConst::MaxFetchWidth];

    bool ackSquash[CribConst::MaxThreads];
    InstSeqNum ackSquashSeqNum[CribConst::MaxThreads];
    Addr ackPcAddr[CribConst::MaxThreads];
};


struct DecodeStruct {
    int size;
    CribDynInstPtr insts[CribConst::MaxDecodeWidth];

    bool ackSquash[CribConst::MaxThreads];
    InstSeqNum ackSquashSeqNum[CribConst::MaxThreads];
    Addr ackPcAddr[CribConst::MaxThreads];
};

struct DispatchStruct {
    int size;
    CribDynInstPtr insts[CribConst::MaxDispatchWidth];

    bool ackSquash[CribConst::MaxThreads];
    InstSeqNum ackSquashSeqNum[CribConst::MaxThreads];
    Addr ackPcAddr[CribConst::MaxThreads];
};


struct TimeStruct {
  struct decodeComm {
      InstSeqNum doneSeqNum;
      CribDynInstPtr squashInst;
      TheISA::PCState nextPC;
      bool squash;
      bool branchTaken;
  };
  decodeComm decodeInfo[CribConst::MaxThreads];

  struct dispatchComm {
      bool loopSizeFail;
      Addr loopAddr;
      unsigned unrollFactor;
  };
  dispatchComm dispatchInfo[CribConst::MaxThreads];

  struct cribComm {
      bool squash;

      InstSeqNum doneSeqNum;
      CribDynInstPtr mispredictInst;
      bool branchTaken;
      TheISA::PCState pc;
      CribDynInstPtr squashInst;
      bool drain;

      bool cribEmpty;

      bool interruptPending;
      bool clearInterrupt;

      // For dispatch
      unsigned dispatchPartition;
  };
  cribComm cribInfo[CribConst::MaxThreads];

  bool decodeBlock[CribConst::MaxThreads];
  bool decodeUnblock[CribConst::MaxThreads];
  bool dispatchBlock[CribConst::MaxThreads];
  bool dispatchUnblock[CribConst::MaxThreads];
  bool cribBlock[CribConst::MaxThreads];
  bool cribUnblock[CribConst::MaxThreads];
  bool commitBlock[CribConst::MaxThreads];
  bool commitUnblock[CribConst::MaxThreads];
};

#endif // __CPU_CRIB_COMM_HH__
