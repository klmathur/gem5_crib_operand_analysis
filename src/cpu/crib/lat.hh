#ifndef __CPU_CRIB_LAT_HH__
#define __CPU_CRIB_LAT_HH__

#include "cpu/crib/regstate.hh"

class LoopAddressTable
{
  public:
    LoopAddressTable(unsigned log_size);
    ~LoopAddressTable();

    void remove(Addr addr);

    bool lookup(Addr addr, Addr &end_addr, Addr &fallthrough_addr,
                RegSet &w_set,
                unsigned &num_insts, unsigned &num_partitions,
                unsigned &max_unroll);
    void update(Addr addr, Addr end_addr, Addr fallthrough_addr,
                const RegSet &w_set,
                unsigned num_insts, unsigned num_partitions);
    void updateMaxUnroll(Addr addr, unsigned max_unroll);
    void recordMispredict(Addr addr);
  private:
    unsigned getIndex(Addr addr) { return addr & indexMask; }

    unsigned indexMask;

    struct LoopEntry {
        LoopEntry() : valid(false), addr(0),
                      maxUnroll(std::numeric_limits<unsigned>::max()),
                      mispredCount(7)
        { }
        bool valid;
        Addr addr;
        Addr end_addr;
        Addr fallthrough_addr;
        RegSet writeSet;
        unsigned numInsts;
        unsigned numPartitions;
        unsigned maxUnroll;
        unsigned mispredCount;
    };

    LoopEntry *table;
};

#endif
