#include "cpu/crib/lat.hh"

LoopAddressTable::LoopAddressTable(unsigned log_size)
{
    table = new LoopEntry[1<<log_size];
    indexMask = (1<<log_size)-1;
}

void
LoopAddressTable::remove(Addr addr)
{
    LoopEntry& entry = table[getIndex(addr)];
    if (entry.valid && entry.addr == addr) {
        entry.valid = false;
    }
}

bool
LoopAddressTable::lookup(Addr addr, Addr &end_addr, Addr &fallthrough_addr,
    RegSet &w_set, unsigned &num_insts, unsigned &num_partitions,
    unsigned &max_unroll)
{
    LoopEntry& entry = table[getIndex(addr)];
    if (entry.valid && entry.addr == addr && entry.mispredCount != 0) {
        end_addr = entry.end_addr;
        fallthrough_addr = entry.fallthrough_addr;
        w_set = entry.writeSet;
        num_insts = entry.numInsts;
        num_partitions = entry.numPartitions;
        max_unroll = entry.maxUnroll;
        return true;
    }
    return false;
}

void
LoopAddressTable::update(Addr addr, Addr end_addr, Addr fallthrough_addr,
        const RegSet &w_set,
        unsigned num_insts, unsigned num_partitions)
{
    LoopEntry& entry = table[getIndex(addr)];
    if (!(entry.valid && entry.addr == addr && entry.mispredCount == 0)) {
        entry.valid = true;
        entry.addr = addr;
        entry.end_addr = end_addr;
        entry.fallthrough_addr = fallthrough_addr;
        entry.writeSet = w_set;
        entry.numInsts = num_insts;
        entry.maxUnroll = 1000;
        entry.numPartitions = num_partitions;
        entry.mispredCount = 7;
    }
}

void
LoopAddressTable::updateMaxUnroll(Addr addr, unsigned max_unroll)
{
    LoopEntry& entry = table[getIndex(addr)];
    if (entry.valid && entry.addr == addr) {
        entry.maxUnroll = max_unroll;
    }
}

void
LoopAddressTable::recordMispredict(Addr addr)
{
    LoopEntry& entry = table[getIndex(addr)];
    if (entry.valid && entry.addr == addr && entry.mispredCount != 0) {
        entry.mispredCount = 0;
    }

}

LoopAddressTable::~LoopAddressTable()
{
    delete[] table;
}
