#ifndef __CPU_CRIB_CRIB_HH__
#define __CPU_CRIB_CRIB_HH__

#include <bitset>
#include <fstream>
#include <iostream>
#include <queue>
#include <vector>
#include "arch/isa.hh"
#include "cpu/crib/comm.hh"
#include "cpu/crib/crib.hh"
#include "cpu/crib/dyn_inst.hh"
#include "cpu/crib/regstate.hh"
#include "cpu/op_class.hh"
#include "cpu/timebuf.hh"
#include "params/CribCPU.hh"

enum ResourceMap {
        INT_ALU_RESOURCE,
        INT_MUL_DIV_RESOURCE,
        SIMD_RESOURCE,
        FP_ALU_RESOURCE,
        FP_DIV_RESOURCE,
        NUM_RESOURCES
    };
unsigned mapOpToResource(unsigned op);

class CribCPU;
class CribLSQ;
class CribPartition;
class CribEntry;

enum CribFlags {
    Squash,
    Break,
    NumCribFlags
};

typedef std::bitset<NumCribFlags> FlagSet;
struct Propagated
{
    Propagated(const RegState &_regs)
        : regs(_regs)
    {
        ready.set();
    }
    Propagated(const RegSet &_ready, const RegSet &_rexec,
               const RegState &_regs)
        : ready(_ready), rexec(_rexec), regs(_regs)
    { }

    Propagated() {}
    RegSet ready;
    RegSet rexec;
    FlagSet flags;
    RegState regs;
};


class Crib
{
  public:
    // Interface to other cpu components
    Crib(CribCPU *_cpu, CribLSQ *_lsq, CribCPUParams *params);
    void startupStage();
    std::string name() const;

    void regStats();

    void setTimeBuffer(TimeBuffer<TimeStruct> *time_buffer);
    void setDispatchQueue(TimeBuffer<DispatchStruct> *dispatch_queue);
    void tick();

    void drain();
    void drainResume();
    bool isDrained() const;
    void takeOverFrom();

    bool empty() const;
    bool full() const;

    void squashFromTC(ThreadID tid);
  public:
    // Interface for thread context
    void setArchIntReg(RegIndex index, IntReg val, ThreadID tid);
    IntReg readArchIntReg(RegIndex index, ThreadID tid);
    //Keshav: Read and Incr read time and write timestamps
    void incReadTick(RegIndex index, ThreadID tid);
    int  getReadTick(RegIndex index, ThreadID tid);
    
    void setArchFloatReg(RegIndex index, FloatReg val, ThreadID tid);
    void setArchFloatRegBits(RegIndex index, FloatRegBits val, ThreadID tid);
    FloatReg readArchFloatReg(RegIndex index, ThreadID tid);
    FloatRegBits readArchFloatRegBits(RegIndex index, ThreadID tid);
    TheISA::PCState pcState(ThreadID tid);
    void pcState(const TheISA::PCState &val, ThreadID tid);
    InstSeqNum committedSeqNum(ThreadID tid);
    void CheckAndUpdateMispredPenalty(int crib_part);
    void RegisterMispredictionForPenaltyCheck(int crib_part);

  private:
    // Private helper functions
    void dispatchInstructions();
    void doTCSquash();
    void commitInstructions();

    void updateStatus();

    void disassemble();
    void resolveBranchMispredicts();
    void removeSquashedPartitions();
    void partitionPropagate();
  private:
    // Private members
    CribCPU *cpu;
    CribLSQ *lsq;
  public:
    unsigned numPartitions;
    unsigned cribCommitWidth;
    unsigned partitionSize;
    CribPartition *partitions;
  private:
    unsigned propagateDistance;
    unsigned dispatchToCribDelay;
    bool cribLoopEnable;

    bool squashTC;
  public:
    unsigned architectedPartition;
    unsigned allocPartition;
    unsigned retirePartition;
  private:
    enum Status {
        Idle,
        Running,
        Blocking,
        NumStatus
    };

    Status status;

    TimeBuffer<DispatchStruct>::wire fromDispatch;
    TimeBuffer<TimeStruct>::wire toDispatch;

    std::queue<CribDynInstPtr> skidBuffer;
    std::queue<CribDynInstPtr> instBuffer;

    bool squashing;
    bool squashed;
    InstSeqNum ackSquashSeqNum;
    Addr ackPcAddr;

    unsigned loopPartitions;

    bool drainPending;

    std::vector<Propagated*> pendingDelete;

    unsigned intALU;
    unsigned intMulDiv;
    unsigned simd;
    unsigned fpALU;
    unsigned fpDiv;

    std::string bpredFileName;
    std::ofstream bpredFile;
    bool dumpBpred;
    bool idealPred;
    bool printNextPc;
    Addr brAddr;


  public:
    unsigned resources[NUM_RESOURCES];
    Stats::Scalar cribLoadOps;
    Stats::Scalar cribStoreOps;
    Stats::Scalar cribIntOps;
    Stats::Scalar cribFPOps;
    /*** Public member array of ticks ***/ 
    unsigned int readStamp[16];
    unsigned int writeStamp[16];
    unsigned int commitCtr;
    unsigned int inst_rstamp[16];
    unsigned int inst_wstamp[16];
    Stats::Scalar cribArchIntRd;
    Stats::Scalar num_oper_1;	//Keshav
/*    Stats::Vector p0_write;   // Keshav
    Stats::Vector p0_read;    //Keshav
    Stats::Vector p1_write;   // Keshav
    Stats::Vector p1_read;    //Keshav
    Stats::Vector p2_write;   // Keshav
    Stats::Vector p2_read;    //Keshav
    Stats::Vector p3_write;   // Keshav
    Stats::Vector p3_read;    //Keshav
    Stats::Vector p4_write;   // Keshav
    Stats::Vector p4_read;    //Keshav
    Stats::Vector p5_write;   // Keshav
    Stats::Vector p5_read;    //Keshav
    Stats::Vector p6_write;   // Keshav
    Stats::Vector p6_read;    //Keshav
    Stats::Vector p7_write;   // Keshav
    Stats::Vector p7_read;  */  //Keshav
 //   Stats::Vector2d readops;
 //   Stats::Vector2d writeops;
    Stats::Vector wr_dist;
    Stats::Vector rr_dist;
    Stats::Vector int_fwd_dist;
    Stats::Vector fwd_reads;
    Stats::Vector raw_reads;
    Stats::Vector wr_sp;
    Stats::Vector rr_sp;
    Stats::Vector wr_pc;
    Stats::Vector rr_pc; 
    
    Stats::Scalar total_reads;
    Stats::Scalar external_reads;
    Stats::Scalar cribMultOps;
    Stats::Vector cribComLoadOps;
    Stats::Vector cribComStoreOps;
    Stats::Vector cribComIntOps;
    Stats::Vector cribComFPOps;

    Stats::Scalar cribLoopMispredicted;
    Stats::Scalar cribLoopBreaks;
    Stats::Scalar cribLoopContinues;
    Stats::Scalar totCommitInterval;
    Stats::Scalar totCommitDistance;
    Stats::Vector partCommitOps;
    Stats::Scalar nCommittedOps;
    Stats::Scalar nPipeFlushes;

    Stats::Scalar nMispredicts;
    Stats::Scalar mispredPenalty;
    
    Stats::Distribution bbSizeDist; 
    Stats::Scalar nExecutes; 
    Stats::Scalar cribMispred;
    Stats::Scalar cribRetMispred;
    Stats::Scalar cribPredRetMispred;

    bool enableCommitCheck;
    Tick lastCommitTick;
    Tick checkPointTick;
    Tick mispredTick;
    int mispredPart;
    int lastCommitEntry;
    uint64_t totalCommitUops;
    bool once;
    uint64_t lastBranchNum;
};

struct CribPartition
{
    void init(Crib *_crib, CribLSQ *_lsq, unsigned _size, CribCPUParams *params, unsigned _num);
    void intraPropagate();
    void interPropagate();
    void evaluate();
    unsigned allocate(std::queue<CribDynInstPtr>& insts, unsigned loop_partitions);
    void deallocate();
    void commit();
    void doTCSquash();
    bool isSquashed();

    bool instsSquashed();
    std::string name() const;
    void print() const;

    bool isAllocated();

    bool resolveBranchMispredicts(CribDynInstPtr &inst,
            CribEntry *&break_entry, bool &break_failed, bool &break_resolved);

    Crib *crib;
    CribLSQ *lsq;
    CribEntry *entries;
    unsigned partitionSize;
    bool architected;

    bool allocated;
    unsigned partitionNum;

    unsigned numInsts;
    unsigned numInstsDone;

    bool loopStart;
    bool loopEnd;
    bool loopFail;
    bool loopPartition;
    bool loopExecutions;
    unsigned loopNumPartitions;
    bool breakSquashed;

    InstSeqNum latchSeqNum;
    RegState latchRegs;
    TheISA::PCState latchPC;
    RegSet latchRexec;
    void disassemble();
};

struct CribEntry
{

    CribEntry();
    void init(CribPartition *_partition, CribLSQ* _lsq, unsigned entry_number,
               CribCPUParams *params);

    void evaluate();
    void intraPropagate();
    void reset();
    void squash();
    void allocate(CribDynInstPtr &_inst);
    void allocateNoop();
    std::string name() const;
    void markExecuted();

    void disassemble();
    CribDynInstPtr inst;

    RegSet regRead;
    RegSet regWrite;

    Propagated *inputs;
    Propagated *outputs;

    unsigned numDestRegs;
    RegResult results[TheISA::MaxInstDestRegs];
    unsigned destRegMap[TheISA::MaxInstDestRegs];

    FlagSet setFlags;

    CribPartition *partition;
    CribLSQ *lsq;
    unsigned entryNum;
    unsigned exeCycles;

    bool setRexec;
    bool firstPropagation;
    bool isSplit;
    bool splitLoDone;
    bool splitHiDone;

    bool execBlock;

    enum Status {
        Invalid, //0
        Noop, //1
        BreakSquashed, //2
        BreakExecutedSquashed, //3
        Squashed, //4
        Allocated, //5
        Executing, //6
        Executed, //7
        NumStatus //8
    };

    Status status;

    static unsigned exeLatencies[Num_OpClasses];
};


#endif //__CPU_CRIB_CRIB_HH__
