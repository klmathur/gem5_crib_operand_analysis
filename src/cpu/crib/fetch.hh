#ifndef __CRIB_FETCH_HH__
#define __CRIB_FETCH_HH__

#include <list>

#include "cpu/crib/comm.hh"
#include "cpu/crib/dyn_inst.hh"
#include "cpu/crib/lat.hh"
#include "cpu/pred/bpred_unit.hh"
#include "cpu/timebuf.hh"
#include "mem/packet.hh"
#include "params/CribCPU.hh"

class CribCPU;

struct IBP{
    Addr bPC;
    Addr tPC;
    uint8_t tFlg;
    uint8_t tNflg;
    uint8_t tIts;
    uint8_t tNits;
    uint8_t tSize;
    InstSeqNum isn;
    IBP(){ }
    IBP(Addr br_pc, Addr tgt_pc, uint8_t flg, uint8_t nflg, uint8_t its, uint8_t nits, uint8_t sz){
        bPC = br_pc;
        tPC = tgt_pc;
        tFlg = flg;
        tNflg = nflg;
        tIts = its;
        tNits = nits;
        tSize = sz;
    }
};

struct UopBuffer{
    /** Store the PC addresses */
    Addr* tags;
    /** Store the offset into buffer */
    int* offset;
    /** Store static instructions in this uop buffer.
     * Note this is not a cache
     */
    StaticInstPtr **buffer;
    /** Instruction size - required to correctly generate nextPC */
    int* instSize;
    /** IT state for each stored PC. */
    uint8_t* itBits;
    /** Book Keeping - Update tags and offsets when a buffer is written */
    int* bKeep;
    /** Size of the uop buffer */
    int size;
    /** Bit mask to calculate the index from PC */
    uint64_t  bmask;
    /** Head pointer to the buffer. */
    unsigned int bhead;
    /** If squashed in the middle of uop decoding */
    bool isSquashed;
    bool prnt;
    /** Constructors and Destructors */
    UopBuffer(){ };
    UopBuffer(int _size);
    ~UopBuffer();

    /** Initialize the buffer */
    void init(int _size);
    /** In case of a squashing event, empty the uop buffer */
    void squash();
    /** Lookup the input PC and return the offset into the uop buffer. */
    int lookup(Addr pc_in);
    /** Add a uop to the uop buffer. */
    void add(StaticInstPtr inp, const PCState &pc_in);
    /** Return the static uop present at the given index. */
    StaticInstPtr get(int index);
    /** Get the size of the instruction, given it's PC.
     * If not for this, we would have to do instruction synchronization to get the length of the instruction.
     */
    int getSize(int index);
    /** Get the next IT state for the given instruction */
    uint8_t getNextItstate(int index);
};

struct UopCacheEntry{
    std::deque<CribDynInstPtr> insts;
    uint64_t accessCycle;
    bool valid;
    UopCacheEntry(){
        valid = false;
        accessCycle =0;
    }
    ~UopCacheEntry(){
        insts.clear();
    };

    CribDynInstPtr match(TheISA::PCState pcs_in){
        if(!valid) return NULL;
        for(int i=0; i<insts.size(); i++){
            //if(insts[i]->pcState().match(pcs_in)){
            if(insts[i]->pcState()==pcs_in){
                return insts[i];
            }
        }
        return NULL;
    }
    bool matchPC(TheISA::PCState pcs_in){
        if(!valid) return NULL;
        assert(insts.size());
        if((insts.front()->pcState().instAddr() == pcs_in.instAddr()) &&
            (insts.front()->pcState().thumb() == pcs_in.thumb())){
            return true;
        }
        return false;
    }

    void accessed(){
        accessCycle = curTick();
    }

    void add(CribDynInstPtr inst_in){
        if(insts.size() && (insts.front()->pcState().instAddr() == inst_in->pcState().instAddr()) &&
            (insts.front()->pcState().thumb() == inst_in->pcState().thumb())){
            for(int i=0; i<insts.size(); i++){
                if(insts[i]->pcState().microPC() == inst_in->pcState().microPC()){
                    insts.erase(insts.begin() + i);
                    break;
                }
            }
        }else{
            insts.clear();
        }
        insts.push_back(inst_in);
        accessed();
        valid = true;
    }
};


struct UopCache{
    CribCPU *cpu;
    UopCacheEntry** table;
    unsigned nSets;
    unsigned nWays;
    unsigned mask;
    int nbits;

    UopCache(){ }
    ~UopCache(){ }
    void init(CribCPU *_cpu, unsigned num_sets, unsigned num_ways);
    Addr getIndex(Addr pc_in);
    int getReplWay(Addr set);
    CribDynInstPtr lookup(TheISA::PCState pcs_in, bool update);
    bool add(CribDynInstPtr inst_in);
};

class CribFetch
{
  public:
    CribFetch(CribCPU *_cpu, CribCPUParams *params);

    std::string name() const;

    void regStats();

    void setTimeBuffer(TimeBuffer<TimeStruct> *time_buffer);
    void setActiveThreads(std::list<ThreadID> *active_threads);
    void setFetchQueue(TimeBuffer<FetchStruct> *fetch_queue);

    void startupStage();
    void setICache();
    void processCacheCompletion(PacketPtr pkt);
    void recvRetry();

    bool isDrained() const;

    void takeOverFrom();
    void wakeFromQuiesce();

    void squash(const TheISA::PCState &newPC, const InstSeqNum seq_num,
                CribDynInstPtr squashInst, ThreadID tid);

    void tick();


    enum FetchStatus {
        Active,
        Inactive
    };


    TheISA::Decoder * getDecoderPtr(ThreadID tid)
    { return fetchState[tid].decoder; }

  private:
    void resetStage();

    /** Typedefs from ISA. */
    typedef TheISA::MachInst MachInst;
    typedef TheISA::ExtMachInst ExtMachInst;

    CribCPU *cpu;
    //UopBuffer uopBuffer;
    //unsigned int numUopBufferEntries;
    //bool useUopBuffer;
    int uopBufferIndex;
    //int uopBufferOffset;

    bool useUopCache;
    UopCache uopCache;
    unsigned ucSets;
    unsigned ucWays;
    
    BPredUnit *bpred;
    LoopAddressTable lat;

    unsigned numInst;
    unsigned fetchWidth;
    unsigned decodeToFetchDelay;
    unsigned dispatchToFetchDelay;
    unsigned cribToFetchDelay;
    unsigned commitToFetchDelay;

    void finishTranslation(Fault fault, RequestPtr mem_req);

    class FetchTranslation : public BaseTLB::Translation
    {
      public:
        FetchTranslation(CribFetch *_fetch) : fetch(_fetch) { }
        void markDelayed() { }
        void finish(Fault fault, RequestPtr req, ThreadContext *tc,
                    BaseTLB::Mode mode)
        {
            assert(mode == BaseTLB::Execute);
            fetch->finishTranslation(fault, req);
            delete this;
        }
      private:
        CribFetch *fetch;
    };

    class FinishTranslationEvent : public Event
    {
      public:
        FinishTranslationEvent(CribFetch *_fetch) : fetch(_fetch) { }
        void setFault(Fault _fault) { fault = _fault; }
        void setReq(RequestPtr _req) { req = _req; }
        void process()
        {
            assert(fetch->numInst < fetch->fetchWidth);
            fetch->finishTranslation(fault, req);
        }
        const char *description() const
        {
            return "CribCPU FetchFinishTranslation";
        }
      private:
        CribFetch *fetch;
        Fault fault;
        RequestPtr req;
    };

    enum ThreadStatus {
        Running,
        Idle,
        Squashing,
        Blocked,
        Fetching,
        TrapPending,
        QuiescePending,
        SwitchOut,
        ItlbWait,
        IcacheWaitResponse,
        IcacheWaitRetry,
        IcacheAccessComplete,
        NoGoodAddr
    };

    enum FetchPriority {
        SingleThread,
        RoundRobin
    };

    FetchStatus status;
    FetchPriority fetchPolicy;

    std::list<ThreadID> threadPriority;

    TimeBuffer<TimeStruct>::wire fromDecode;
    TimeBuffer<TimeStruct>::wire fromDispatch;
    TimeBuffer<TimeStruct>::wire fromCrib;
    TimeBuffer<TimeStruct>::wire fromCommit;

    TimeBuffer<FetchStruct> *fetchQueue;

    TimeBuffer<FetchStruct>::wire toDecode;

    struct Stalls {
        bool decode;
        bool dispatch;
        bool crib;
        bool commit;
        bool drain;
    };

    struct LoopTrainInfo {
        LoopTrainInfo() : valid(false) { }
        bool valid;
        Addr targetAddr;
        Addr branchAddr;
        unsigned numInsts;
        RegSet writeSet;
        RegSet readSet;
        unsigned requiredPartitions;
        unsigned curPartitionLoads;
        unsigned curPartitionStores;
        unsigned curPartitionInsts;
    };

    struct LoopExecInfo {
        LoopExecInfo() : valid(false), unrollCount(0), unrollFactor(0) { }
        bool valid;
        unsigned unrollCount;
        unsigned unrollFactor;
        TheISA::PCState startAddr;
        Addr endAddr;
        Addr fallthroughAddr;
        unsigned numInsts;
        unsigned numPartitions;
        RegSet writtenRegs;
        CribDynInstPtr loopStart;
        unsigned numInstsDispatched;
    };

    struct FetchState {
        ThreadStatus status;
        TheISA::PCState pc;
        Addr fetchOffset;
        StaticInstPtr macroop;
        // Can fetch redirect on interrupt on current instruction
        bool delayedCommit;
        RequestPtr memReq;
        uint8_t *cacheData;
        Addr cacheDataPC;
        bool cacheDataValid;
        Counter lastIcacheStall;
        bool issuePipelinedIfetch;
        TheISA::Decoder *decoder;
        Stalls stalls;

        // Loop training info
        LoopTrainInfo loopTrainInfo;

        LoopExecInfo loopExecInfo;
    };

    FetchState fetchState[CribConst::MaxThreads];
    bool cacheBlocked;
    PacketPtr retryPkt;
    ThreadID retryTid;
    ThreadID numThreads;

    bool interruptPending;
    bool drainPending;
    FinishTranslationEvent finishTranslationEvent;

    int instSize;
    int cacheBlkSize;
    Addr cacheBlkMask;

    TimeBuffer<TimeStruct> *timeBuffer;
    std::list<ThreadID> *activeThreads;
    InstSeqNum globalSeqNum;
    uint64_t globalLoopColor;

    unsigned cribPartitions;
    unsigned cribPartitionSize;
    unsigned cribLoadsPerPartition;
    unsigned cribStoresPerPartition;
    bool cribLoopEnable;
    bool cribLoopUnroll;
    bool cribLoopTrain;
    unsigned cribLoopDependencies;

    bool idealPred;
    bool dumpBpred;
    std::string bpredFileName;
    std::deque<IBP*> bpRecords;
    std::deque<IBP*> bkpBpr;
  private:
    // Helper functions
    inline void switchToActive();
    inline void switchToInactive();
    bool checkStall(ThreadID tid) const;
    bool wroteToTimeBuffer;
    inline void doSquash(const TheISA::PCState &newPC,
                         const CribDynInstPtr squashInst, ThreadID tid);

    bool checkSignalsAndUpdate(ThreadID tid);
    void squashFromDecode(const TheISA::PCState &newPC,
                          const CribDynInstPtr squashInst,
                          const InstSeqNum seq_num, ThreadID tid);
    void fetch(bool &status_change);
    FetchStatus updateFetchStatus();
    ThreadID getFetchingThread(FetchPriority priority);
    ThreadID fetchSingleThread();
    ThreadID fetchRoundRobin();
    Addr icacheBlockAlignPC(Addr addr) { return (addr & ~(cacheBlkMask)); }
    bool fetchCacheLine(Addr vaddr, ThreadID tid, Addr pc);
    inline bool checkInterrupt(Addr pc);
    CribDynInstPtr buildInst(ThreadID tid, StaticInstPtr staticInst,
                             StaticInstPtr curMacroop, TheISA::PCState thisPC,
                             TheISA::PCState nextPC, bool trace);
    bool lookupAndUpdateNextPC(ThreadID tid, CribDynInstPtr inst,
                               TheISA::PCState &nextPC, bool loop_end,
                               bool unroll_end);
    void pipelineIcacheAccess(ThreadID tid);
  private:
    // Statistics
    Stats::Scalar icacheStallCycles;
    Stats::Scalar fetchedInsts;
    Stats::Scalar uopBufferFetch;
    Stats::Scalar fetchedOps;
    Stats::Scalar fetchedBranches;
    Stats::Scalar predictedBranches;
    Stats::Scalar fetchCycles;
    Stats::Scalar fetchSquashCycles;
    Stats::Scalar fetchTlbCycles;
    Stats::Scalar fetchIdleCycles;
    Stats::Scalar fetchBlockedCycles;
    Stats::Scalar fetchMiscStallCycles;
    Stats::Scalar fetchPendingDrainCycles;
    Stats::Scalar fetchNoActiveThreadStallCycles;
    Stats::Scalar fetchPendingTrapStallCycles;
    Stats::Scalar fetchPendingQuiesceStallCycles;
    Stats::Scalar fetchIcacheWaitRetryStallCycles;
    Stats::Scalar fetchCacheLines;
    Stats::Scalar fetchIcacheSquashes;
    Stats::Scalar fetchTlbSquashes;

    Stats::Distribution fetchInstDist;
    Stats::Formula idleRate;
    Stats::Formula branchRate;
    Stats::Formula fetchRate;
};

inline bool
CribFetch::checkInterrupt(Addr pc) {
    return interruptPending && (THE_ISA != ALPHA_ISA || !(pc &0x3));
}

#endif // __CRIB_FETCH_HH__
