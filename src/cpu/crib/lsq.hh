#ifndef __CPU_CRIB_LSQ_HH__
#define __CPU_CRIB_LSQ_HH__

#include <map>
#include <set>

#include "cpu/crib/dyn_inst.hh"

class Crib;
class CribCPU;
class CribCPUParams;
class CribLSQBank;

struct LoadRequest
{
    LoadRequest(const CribDynInstPtr &_inst, Addr _addr, unsigned _size,
                unsigned _flags, bool _split, bool _split_hi, bool _prefetch)
        : inst(_inst), addr(_addr), size(_size), flags(_flags),
          split(_split), split_hi(_split_hi), prefetch(_prefetch)
    { }

    CribDynInstPtr inst;
    Addr addr;
    unsigned size;
    unsigned flags;
    bool split;
    bool split_hi;
    bool prefetch;
};

struct StoreRequest
{
    StoreRequest(const CribDynInstPtr &_inst, Addr _addr, unsigned _size,
                 unsigned _flags, uint8_t *_data, bool _split, bool _split_hi)
        : inst(_inst), addr(_addr), size(_size), flags(_flags),
          split(_split), split_hi(_split_hi)
    {
        data = new uint8_t[size];
        memcpy(data, _data, size);
    }

    StoreRequest(const StoreRequest &other)
        : inst(other.inst), addr(other.addr), size(other.size),
          flags(other.flags), split(other.split), split_hi(other.split_hi)
    {
        data = new uint8_t[size];
        memcpy(data, other.data, size);
    }

    ~StoreRequest()
    {
        delete[] data;
    }

    CribDynInstPtr inst;
    Addr addr;
    unsigned size;
    unsigned flags;
    uint8_t *data;
    bool split;
    bool split_hi;
};

class CribLSQ
{
  public:
    CribLSQ(CribCPU *_cpu, Crib *_crib, CribCPUParams *params);
    std::string name() const;

    void initiateRead(CribDynInstPtr inst, Addr addr, unsigned size,
            unsigned flags);
    void initiateWrite(CribDynInstPtr inst, Addr addr, unsigned size,
            unsigned flags, uint8_t *data);
    void initiatePrefetch(CribDynInstPtr inst, Addr addr, unsigned size,
            unsigned flags);
    bool validatePrefetch(const CribDynInstPtr &inst, Addr addr,
        unsigned size, unsigned flags);
    void deassertRequest(const CribDynInstPtr &inst,
            bool committed = false);
    void SquashYoungerMemoryOps(InstSeqNum seq_num);
    void deassertAllPrefetches(const CribDynInstPtr &inst);
    void makeSenior(const CribDynInstPtr &inst);

    void tick();
    bool isDrained() const;
    void startupStage();
    void takeOverFrom();
    bool recvTimingResp(PacketPtr pkt);
    void deassertPrefetchRequest(const CribDynInstPtr &inst);

    void regStats(); 

    Stats::Scalar sqFull;
    Stats::Scalar lqFull;
    
  private:
    void setDCache();
    unsigned addrToBankNum(const Addr& addr)
    { return (addr & bankMask) >> log2BlockSize; }


    CribCPU *cpu;
    Crib *crib;
    CribLSQBank *banks;
    unsigned numBanks;
    unsigned cacheBlkSize;
    unsigned log2BlockSize;
    Addr bankMask;
    Addr cacheBlkMask;
};

class CribLSQBank
{
  public:
    void init(CribCPU *_cpu, CribLSQ *_lsq, CribCPUParams *params,
              unsigned bank_num, CribLSQBank *prev, CribLSQBank *next);
    std::string name() const;

    void registerLoadRequest(const CribDynInstPtr &inst, Addr addr,
        unsigned size, unsigned flags, bool split = false,
        bool split_hi = false);
    void registerStoreRequest(const CribDynInstPtr &inst, Addr addr,
        unsigned size, unsigned flags, uint8_t *data, bool split = false,
        bool split_hi = false);
    void registerPrefetchRequest(const CribDynInstPtr &inst, Addr addr,
        unsigned size, unsigned flags);
    bool validatePrefetch(const CribDynInstPtr &inst, Addr addr,
        unsigned size, unsigned flags);
    void tick();
    void completeDataAccess(PacketPtr pkt);
    void deassertRequest(const CribDynInstPtr &inst, bool committed);
    void SquashYoungerMemoryOps(InstSeqNum seq_num);
    void deassertAllPrefetches(InstSeqNum seq_num);
    void makeSenior(const CribSeqNum &seq_num);
    bool isDrained() const;
  private:
    CribCPU *cpu;
    CribLSQ *lsq;
    unsigned bankNum;
    CribLSQBank *prevBank;
    CribLSQBank *nextBank;

    unsigned numLQEntries;
    unsigned numSQEntries;

    unsigned loadAllocBandwidth;
    unsigned storeAllocBandwidth;

    struct LQEntry {
        LQEntry(const CribDynInstPtr &_inst, Addr _addr, unsigned _size,
                unsigned _flags, bool _split, bool _split_hi, bool _prefetch)
            : inst(_inst), addr(_addr), size(_size), flags(_flags),
              split(_split), split_hi(_split_hi), prefetch(_prefetch),
              memReq(0),
              status(LQEntry::Status::Allocated),
              forwardStatus(LQEntry::ForwardStatus::NoForward)
        {
            data = new uint8_t[size];
        }

        ~LQEntry()
        {
            delete[] data;
        }

        CribDynInstPtr inst;
        Addr addr;
        Addr physAddr;
        unsigned size;
        unsigned flags;
        uint8_t *data;
        bool split;
        bool split_hi;
        bool prefetch;
        RequestPtr memReq;

        enum Status {
            Allocated,
            IssuedTranslation,
            IssuedLoad,
            Executed
        };
        Status status;

        enum ForwardStatus {
            NoForward,
            FullForward,
            PartialForward
        };
        ForwardStatus forwardStatus;
        std::vector<bool> forwardBytes;
    };

    struct SQEntry {
        SQEntry(const CribDynInstPtr &_inst, Addr _addr, unsigned _size,
                unsigned _flags, uint8_t *_data, bool _split, bool _split_hi)
            : inst(_inst), addr(_addr), size(_size), flags(_flags),
              split(_split), split_hi(_split_hi), memReq(0), pkt(0),
              status(SQEntry::Status::Allocated)
        {
            data = new uint8_t[size];
            memcpy(data, _data, size);
        }

        ~SQEntry()
        {
            delete[] data;
        }

        void recordLoadForward(const CribSeqNum &seq_num)
        {
            fwdLoadSeq.insert(seq_num);
        }

        CribDynInstPtr inst;
        Addr addr;
        Addr physAddr;
        unsigned size;
        unsigned flags;
        uint8_t *data;
        bool split;
        bool split_hi;
        RequestPtr memReq;
        PacketPtr pkt;
        std::set<CribSeqNum> fwdLoadSeq;

        enum Status {
            Allocated,
            IssuedTranslation,
            Executed,
            Senior,
            SeniorDispatched,
            SeniorComplete
        };
        Status status;
    };


    std::map<CribSeqNum, LQEntry*> loadQueue;
    std::map<CribSeqNum, SQEntry*> storeQueue;
    std::map<CribSeqNum, LoadRequest> loadRequests;
    std::map<CribSeqNum, StoreRequest> storeRequests;
    std::map<CribSeqNum, LQEntry*> unissuedLoads;
    std::map<CribSeqNum, SQEntry*> unissuedStores;

    class StoreTranslation : public BaseTLB::Translation
    {
      public:
        StoreTranslation(CribLSQBank *bank, const CribSeqNum &seq_num)
            : lsqBank(bank), cribSeqNum(seq_num) { }
        void markDelayed() { }
        void finish(Fault fault, RequestPtr req, ThreadContext *tc,
                    BaseTLB::Mode mode)
        {
            assert(mode == BaseTLB::Write);
            lsqBank->finishStoreTranslation(fault, req, cribSeqNum);
            delete this;
        }
      private:
        CribLSQBank *lsqBank;
        CribSeqNum cribSeqNum;
    };

    class LoadTranslation : public BaseTLB::Translation
    {
      public:
        LoadTranslation(CribLSQBank *bank, const CribSeqNum &seq_num)
            : lsqBank(bank), cribSeqNum(seq_num) { }
        void markDelayed() { }
        void finish(Fault fault, RequestPtr req, ThreadContext *tc,
                    BaseTLB::Mode mode)
        {
            assert(mode == BaseTLB::Read);
            lsqBank->finishLoadTranslation(fault, req, cribSeqNum);
            delete this;
        }
      private:
        CribLSQBank *lsqBank;
        CribSeqNum cribSeqNum;
    };

    void finishStoreTranslation(const Fault &fault, RequestPtr req, const CribSeqNum &seq_num);
    void finishLoadTranslation(const Fault &fault, RequestPtr req, const CribSeqNum &seq_num);

    struct LQSenderState : public Packet::SenderState
    {
        LQSenderState(const CribSeqNum &seq_num) : seqNum(seq_num) { }
        CribLSQBank *lsqBank;
        CribSeqNum seqNum;
    };

    struct SQSenderState : public Packet::SenderState
    {
        SQSenderState(const CribSeqNum &seq_num) : seqNum(seq_num) { }
        CribLSQBank *lsqBank;
        CribSeqNum seqNum;
    };
  private:
    // Private functions
    void parseRequests();
    void issueStores();
    void issueLoads();
    void writebackStores();

    void allocateLoad(const CribSeqNum &sn, const LoadRequest &load_req);
    void allocateStore(const CribSeqNum &sn, const StoreRequest &store_req);

    void forwardData(const CribSeqNum &seq_num, LQEntry& lqe);
    void probeLoadQueue(Addr addr, unsigned size, const CribSeqNum &seq_num);

    void deassertStoreForwardedLoads(const CribSeqNum& seq_num,
        const SQEntry &sqe);
    void handleSplitLoad(const CribSeqNum &seq_num, const LQEntry &lqe);
    LQEntry *getLQEntry(const CribSeqNum &seq_num);

    void handleSplitStore(const CribSeqNum &seq_num, const SQEntry &sqe);
    SQEntry *getSQEntry(const CribSeqNum &seq_num);
};

#endif // __CPU_CRIB_LSQ_HH__
