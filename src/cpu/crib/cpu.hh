#ifndef __CPU_CRIB_CPU_HH__
#define __CPU_CRIB_CPU_HH__

#include <list>
#include <vector>

#include "arch/isa.hh"
#include "base/statistics.hh"
#include "cpu/crib/comm.hh"
#include "cpu/crib/const.hh"
#include "cpu/crib/crib.hh"
#include "cpu/crib/decode.hh"
#include "cpu/crib/dispatch.hh"
#include "cpu/crib/fetch.hh"
#include "cpu/crib/lsq.hh"
#include "cpu/crib/regstate.hh"
#include "cpu/crib/thread_state.hh"
#include "cpu/activity.hh"
#include "cpu/base.hh"
#include "cpu/timebuf.hh"
#include "params/CribCPU.hh"

class CribCPU : public BaseCPU
{
  public:
    /** Required interface */
    CribCPU(CribCPUParams *params);
    ~CribCPU();

    MasterPort &getDataPort() { return dcachePort; }
    MasterPort &getInstPort() { return icachePort; }

    void wakeCPU();
    void wakeup();
    void activateContext(ThreadID tid, Cycles delay);
    void suspendContext(ThreadID tid);
    void deallocateContext(ThreadID tid);
    void haltContext(ThreadID tid);

    void init();
    void startup();
    unsigned int drain(DrainManager *drain_manager);
    void drainResume();

    void regStats();

    void activateWhenReady(ThreadID tid);
    void switchOut();
    void takeOverFrom(BaseCPU* cpu);
    void serializeThread(std::ostream &os, ThreadID tid);
    void unserializeThread(Checkpoint *cp, const std::string &section,
                           ThreadID tid);

    void commitDrainInitiated() { commitDrainInProgress = true; }
    void commitDrainFinished() { commitDrainInProgress = false; }
    Counter totalInsts() const;
    Counter totalOps() const;

    /** Interface for thread_context to access architected state */
    TheISA::TLB *getITBPtr() { return itb; }
    TheISA::TLB *getDTBPtr() { return dtb; }

    TheISA::PCState pcState(ThreadID tid);
    void pcState(const TheISA::PCState &val, ThreadID tid);
    Addr instAddr(ThreadID tid);
    Addr nextInstAddr(ThreadID tid);
    MicroPC microPC(ThreadID tid);

    MiscReg readMiscRegNoEffect(int misc_reg, ThreadID tid);
    MiscReg readMiscReg(int misc_reg, ThreadID tid);
    void setMiscRegNoEffect(int misc_reg, const MiscReg& val, ThreadID tid);
    void setMiscReg(int misc_reg, const MiscReg& val, ThreadID tid);

    IntReg readArchIntReg(int reg_idx, ThreadID tid);
    FloatReg readArchFloatReg(int reg_idx, ThreadID tid);
    FloatRegBits readArchFloatRegBits(int reg_idx, ThreadID tid);
    void setArchIntReg(int reg_idx, IntReg val, ThreadID tid);
    void setArchFloatReg(int reg_idx, FloatReg val, ThreadID tid);
    void setArchFloatRegBits(int reg_idx, FloatRegBits val, ThreadID tid);

    //void syscall(int64_t callnum, ThreadID tid);

    void squashFromTC(ThreadID tid);
    std::vector<TheISA::ISA *> isa;

    enum StageIdx {
        FetchIdx,
        DecodeIdx,
        DispatchIdx,
        CribIdx,
        NumStages
    };

    void activateStage(const StageIdx idx);
    void deactivateStage(const StageIdx idx);
    DrainManager *drainManager;

    bool contextSwitch;
    bool cribStridePrefetchEnable;
    bool cribPointerPrefetchEnable;

    void recordActivity() { activityRec.activity(); }
    CribThreadState* getThreadState(ThreadID tid) { return thread[tid]; }
    ThreadContext* tcBase(ThreadID tid) { return thread[tid]->getTC(); }
    TheISA::Decoder *getDecoderPtr(ThreadID tid)
    { return fetch.getDecoderPtr(tid); }

    void instDone(const CribDynInstPtr &inst, ThreadID tid);

  private:
    enum Status {
        Running,
        Idle,
        Halted,
        Blocked,
        SwitchedOut
    };

    Status _status;
    Status _threadStatus[CribConst::MaxThreads];

    // Execute related
    void tick();

    void tryDrain();
    bool isDrained() const;

    ActivityRecorder activityRec;
    Cycles lastRunningCycle;

    /** Thread/Context helper functions */
    class ActivateThreadEvent : public Event
    {
      public:
        ActivateThreadEvent();
        void init(ThreadID _tid, CribCPU *_cpu);
        void process();
        const char *description() const;
      private:
        ThreadID tid;
        CribCPU *cpu;
    };

    ActivateThreadEvent activateThreadEvent[CribConst::MaxThreads];

    void scheduleActivateThreadEvent(ThreadID tid, Cycles delay);
    void activateThread(ThreadID tid);


    std::list<ThreadID> activeThreads;
    Tick lastActivatedCycle;
    bool commitDrainInProgress;
    class DeallocateContextEvent : public Event
    {
      public:
        DeallocateContextEvent();
        void init(ThreadID _tid, CribCPU *_cpu);
        void process();
        void setRemove(bool _remove);
        const char *description() const;
      private:
        ThreadID tid;
        bool remove;
        CribCPU *cpu;
    };

    DeallocateContextEvent deallocateContextEvent[CribConst::MaxThreads];

    void deactivateThread(ThreadID tid);
    void removeThread(ThreadID tid);

    class TickEvent : public Event
    {
      public:
        TickEvent(CribCPU *_cpu);
        void process();
        const char *description() const;
      private:
        CribCPU *cpu;
    };

    TickEvent tickEvent;
    void scheduleTickEvent(Cycles delay);

  private:

    class ICachePort : public MasterPort
    {
      public:
        ICachePort(CribFetch *_fetch, CribCPU* _cpu);
      private:
        bool recvTimingResp(PacketPtr pkt);
        void recvTimingSnoopReq(PacketPtr pkt);
        void recvRetry();

        CribFetch *fetch;
    };

    class DCachePort : public MasterPort
    {
      public:
        DCachePort(CribLSQ *_lsq, CribCPU* _cpu);
      private:
        bool recvTimingResp(PacketPtr pkt);
        void recvTimingSnoopReq(PacketPtr pkt);
        void recvRetry();

        CribLSQ *lsq;
    };

    TheISA::TLB *itb;
    TheISA::TLB *dtb;

    CribFetch fetch;
    CribDecode decode;
    CribDispatch dispatch;
    Crib crib;
    CribLSQ lsq;

    ICachePort icachePort;
    DCachePort dcachePort;

    std::vector<CribThreadState*> thread;

    /** Stats */
    Stats::Scalar idleCycles;
    Stats::Scalar quiesceCycles;
    Stats::Vector committedInsts;
    Stats::Vector committedOps;
    Stats::Formula cpi;
    Stats::Formula ipc;
    Stats::Formula upc;
  public:
    Stats::Scalar stridePrefetch;
    Stats::Scalar constantPrefetch;
    Stats::Scalar pointerPrefetch;
  private:
    TimeBuffer<TimeStruct> timeBuffer;
    TimeBuffer<FetchStruct> fetchQueue;
    TimeBuffer<DecodeStruct> decodeQueue;
    TimeBuffer<DispatchStruct> dispatchQueue;
};

#endif // __CPU_CRIB_CPU_HH__
