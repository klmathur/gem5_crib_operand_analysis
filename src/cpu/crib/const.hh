#ifndef __CPU_CRIB_CONST_HH__
#define __CPU_CRIB_CONST_HH__

namespace CribConst {
  const unsigned int MaxThreads = 1;
  const unsigned int MaxFetchWidth = 8;
  const unsigned int MaxDecodeWidth= 8;
  const unsigned int MaxDispatchWidth= 8;
}

#endif
