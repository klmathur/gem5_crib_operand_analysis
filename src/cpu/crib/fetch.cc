#include "arch/decoder.hh"
#include "cpu/crib/cpu.hh"
#include "cpu/crib/fetch.hh"
#include "debug/Activity.hh"
#include "debug/CribFetch.hh"
#include "debug/CribLoop.hh"
#include "sim/system.hh"
#include <fstream>


CribFetch::CribFetch(CribCPU *_cpu, CribCPUParams *params)
: cpu(_cpu),
    //numUopBufferEntries(params->numUopBufferEntries),
    bpred(params->branchPred),
    lat(params->cribLogLoopHistSize),
    numInst(0),
    fetchWidth(params->fetchWidth),
    decodeToFetchDelay(params->decodeToFetchDelay),
    dispatchToFetchDelay(params->dispatchToFetchDelay),
    cribToFetchDelay(params->cribToFetchDelay),
    commitToFetchDelay(params->commitToFetchDelay),
    cacheBlocked(false),
    retryPkt(NULL),
    retryTid(InvalidThreadID),
    numThreads(params->numThreads),
    interruptPending(false),
    drainPending(false),
    finishTranslationEvent(this),
    globalSeqNum(0),
    globalLoopColor(0),
    cribPartitions(params->cribPartitions),
    cribPartitionSize(params->cribPartitionSize),
    cribLoadsPerPartition(params->cribLoadsPerPartition),
    cribStoresPerPartition(params->cribStoresPerPartition),
    cribLoopEnable(params->cribLoopEnable),
    cribLoopUnroll(params->cribLoopUnroll),
    cribLoopTrain(params->cribLoopTrain),
    cribLoopDependencies(params->cribLoopDependencies)
{
    if (numThreads > CribConst::MaxThreads) {
        fatal("numThreads (%d) is larger than the compiled limit (%d).\n",
              numThreads, CribConst::MaxThreads);
    }
    if (fetchWidth > CribConst::MaxFetchWidth) {
        fatal("fetchWidth (%d) is larger than the compiled limit (%d).\n",
              fetchWidth, CribConst::MaxFetchWidth);
    }

    status = Inactive;

    std::string policy = params->smtFetchPolicy;
    std::transform(policy.begin(), policy.end(), policy.begin(), ::tolower);

    if (policy == "singlethread") {
        fetchPolicy = SingleThread;
        if (numThreads > 1) {
            panic("Invalid fetch policy for SMT workload");
        }
    } else if (policy == "roundrobin") {
        fetchPolicy = RoundRobin;
    } else {
        fatal("Invalid fetch policy.");
    }

    instSize = sizeof(TheISA::MachInst);

    for (int i = 0; i < CribConst::MaxThreads; i++) {
        fetchState[i].cacheData = NULL;
        fetchState[i].decoder = new TheISA::Decoder;
    }
    //useUopBuffer = false;
    uopBufferIndex = -1;
    /*
    uopBufferOffset = 0;
    if (numUopBufferEntries > 0) {
        uopBuffer.init(numUopBufferEntries);
        useUopBuffer = true;
    }
    */
    ucSets = params->ucSets;
    ucWays = params->ucWays;
    useUopCache = false;
    if(ucSets > 0){
        uopCache.init(cpu, ucSets, ucWays);
        useUopCache = true;
    }

    dumpBpred = params->dumpBpred;
    idealPred = params->idealPred;
    bpredFileName = params->bpredFileName;
    if(!dumpBpred && idealPred){
        assert(bpredFileName.length());
        std::ifstream ifs(bpredFileName.c_str());
        while(ifs.good()){
            Addr ba;
            Addr tga;
            unsigned flg;
            unsigned nflg;
            unsigned its;
            unsigned nits;
            unsigned sz;
            char a;
            ifs >>std::hex>> ba;
            ifs >> a;
            ifs >>std::hex>> tga;
            ifs >> a;
            ifs >> flg;
            ifs >> a;
            ifs >> nflg;
            ifs >> a;
            ifs >> its;
            ifs >> a;
            ifs >> nits;
            ifs >> a;
            ifs >> sz;

            bpRecords.push_back(new IBP(ba,tga,(uint8_t)(flg),(uint8_t)(nflg),(uint8_t)(its),(uint8_t)(nits),(uint8_t)(sz)));
        }
        ifs.close();
    }
}

inline std::string
CribFetch::name() const
{
    return cpu->name() + ".fetch";
}

void
CribFetch::regStats()
{
    icacheStallCycles
        .name(name() + ".icacheStallCycles")
        .desc("Number of cycles fetch is stalled on an Icache miss")
        .prereq(icacheStallCycles);

    fetchedInsts
        .name(name() + ".Insts")
        .desc("Number of instructions fetch has processed")
        .prereq(fetchedInsts);

    fetchedOps
        .name(name() + ".Ops")
        .desc("Number of micro ops fetch has processed")
        .prereq(fetchedOps);

    fetchedBranches
        .name(name() + ".Branches")
        .desc("Number of branches that fetch encountered")
        .prereq(fetchedBranches);

    uopBufferFetch
        .name(name() + ".uopBufferFetch")
        .desc("number of uops serviced from the uop buffer")
        .prereq(uopBufferFetch);

    predictedBranches
        .name(name() + ".predictedBranches")
        .desc("Number of branches that fetch has predicted taken")
        .prereq(predictedBranches);

    fetchCycles
        .name(name() + ".Cycles")
        .desc("Number of cycles fetch has run and was not squashing or"
              " blocked")
        .prereq(fetchCycles);

    fetchSquashCycles
        .name(name() + ".SquashCycles")
        .desc("Number of cycles fetch has spent squashing")
        .prereq(fetchSquashCycles);

    fetchTlbCycles
        .name(name() + ".TlbCycles")
        .desc("Number of cycles fetch has spent waiting for tlb")
        .prereq(fetchTlbCycles);

    fetchIdleCycles
        .name(name() + ".IdleCycles")
        .desc("Number of cycles fetch was idle")
        .prereq(fetchIdleCycles);

    fetchBlockedCycles
        .name(name() + ".BlockedCycles")
        .desc("Number of cycles fetch has spent blocked")
        .prereq(fetchBlockedCycles);

    fetchMiscStallCycles
        .name(name() + ".MiscStallCycles")
        .desc("Number of cycles fetch has spent waiting on interrupts, or "
              "bad addresses, or out of MSHRs")
        .prereq(fetchMiscStallCycles);

    fetchPendingDrainCycles
        .name(name() + ".PendingDrainCycles")
        .desc("Number of cycles fetch has spent waiting on pipes to drain")
        .prereq(fetchPendingDrainCycles);

    fetchNoActiveThreadStallCycles
        .name(name() + ".NoActiveThreadStallCycles")
        .desc("Number of stall cycles due to no active thread to fetch from")
        .prereq(fetchNoActiveThreadStallCycles);

    fetchPendingTrapStallCycles
        .name(name() + ".PendingTrapStallCycles")
        .desc("Number of stall cycles due to pending traps")
        .prereq(fetchPendingTrapStallCycles);

    fetchPendingQuiesceStallCycles
        .name(name() + ".PendingQuiesceStallCycles")
        .desc("Number of stall cycles due to pending quiesce instructions")
        .prereq(fetchPendingQuiesceStallCycles);

    fetchIcacheWaitRetryStallCycles
        .name(name() + ".IcacheWaitRetryStallCycles")
        .desc("Number of stall cycles due to full MSHR")
        .prereq(fetchIcacheWaitRetryStallCycles);

    fetchCacheLines
        .name(name() + ".CacheLines")
        .desc("Number of cache lines fetched")
        .prereq(fetchCacheLines);

    fetchIcacheSquashes
        .name(name() + ".IcacheSquashes")
        .desc("Number of outstanding Icache misses that were squashed")
        .prereq(fetchIcacheSquashes);

    fetchTlbSquashes
        .name(name() + ".ItlbSquashes")
        .desc("Number of outstanding ITLB misses that were squashed")
        .prereq(fetchTlbSquashes);

    fetchInstDist
        .init(0, fetchWidth, 1)
        .name(name() + ".instDist")
        .desc("Number of instructions fetched each cycle (Total)")
        .flags(Stats::pdf);

    idleRate
        .name(name() + ".idleRate")
        .desc("Percent of cycles fetch was idle")
        .prereq(idleRate);
    idleRate = fetchIdleCycles * 100 / cpu->numCycles;

    branchRate
        .name(name() + ".branchRate")
        .desc("Number of branch fetches per cycle")
        .flags(Stats::total);
    branchRate = fetchedBranches / cpu->numCycles;

    fetchRate
        .name(name() + ".rate")
        .desc("Number of inst fetches per cycle")
        .flags(Stats::total);
    fetchRate = fetchedInsts / cpu->numCycles;
}


void
CribFetch::setTimeBuffer(TimeBuffer<TimeStruct> *time_buffer)
{
    timeBuffer = time_buffer;

    fromDecode = timeBuffer->getWire(-decodeToFetchDelay);
    fromDispatch = timeBuffer->getWire(-dispatchToFetchDelay);
    fromCrib = timeBuffer->getWire(-cribToFetchDelay);
    fromCommit = timeBuffer->getWire(-commitToFetchDelay);
}

void
CribFetch::setActiveThreads(std::list<ThreadID> *active_threads)
{
    activeThreads = active_threads;
}

void
CribFetch::setFetchQueue(TimeBuffer<FetchStruct> *fetch_queue)
{
    fetchQueue = fetch_queue;
    toDecode = fetchQueue->getWire(0);
}

void
CribFetch::startupStage()
{
    assert(threadPriority.empty());

    resetStage();
    switchToActive();
}

void
CribFetch::resetStage()
{
    numInst = 0;
    drainPending = false;
    interruptPending = false;
    cacheBlocked = false;

    threadPriority.clear();

    for (ThreadID tid = 0; tid < numThreads; tid++) {
        fetchState[tid].status = Running;
        fetchState[tid].pc = cpu->pcState(tid);
        fetchState[tid].fetchOffset = 0;
        fetchState[tid].macroop = NULL;
        fetchState[tid].delayedCommit = false;
        fetchState[tid].memReq = NULL;
        fetchState[tid].stalls.decode = false;
        fetchState[tid].stalls.dispatch = false;
        fetchState[tid].stalls.crib = false;
        fetchState[tid].stalls.commit = false;
        fetchState[tid].loopTrainInfo.valid = false;
        fetchState[tid].loopExecInfo.valid = false;

        threadPriority.push_back(tid);

    }
    wroteToTimeBuffer = false;
    status = Inactive;

    if (cpu->getInstPort().isConnected()) {
        setICache();
    }
}

void
CribFetch::setICache()
{
    assert(cpu->getInstPort().isConnected());

    cacheBlkSize = cpu->getInstPort().peerBlockSize();
    cacheBlkMask = cacheBlkSize - 1;

    for (ThreadID tid = 0; tid < numThreads; tid++) {
        if (!fetchState[tid].cacheData)  {
           fetchState[tid].cacheData = new uint8_t[cacheBlkSize];
        }
        fetchState[tid].cacheDataPC = 0;
        fetchState[tid].cacheDataValid = false;
    }
}

void
CribFetch::processCacheCompletion(PacketPtr pkt)
{
    ThreadID tid = pkt->req->threadId();

    DPRINTF(CribFetch, "[tid:%i]: Waking up for cache miss.\n", tid);
    assert(!cpu->switchedOut());

    // Detect the need to squash icache response
    if (fetchState[tid].status != IcacheWaitResponse ||
        pkt->req != fetchState[tid].memReq) {

        ++fetchIcacheSquashes;
        delete pkt->req;
        delete pkt;
        return;
    }

    memcpy(fetchState[tid].cacheData, pkt->getPtr<uint8_t>(), cacheBlkSize);
    fetchState[tid].cacheDataValid = true;

    if (!drainPending) {
        // Wake cpu if it went to sleep during icache miss
        cpu->wakeCPU();

        DPRINTF(Activity, "[tid:%i]: Activating fetch due to cache "
                "completion.\n", tid);

        switchToActive();
    }

    // Only switch to IcacheAccessComplete if we're not stalled
    if (checkStall(tid)) {
        fetchState[tid].status = Blocked;
    } else {
        fetchState[tid].status = IcacheAccessComplete;
    }

    delete pkt->req;
    delete pkt;
    fetchState[tid].memReq = NULL;
}

void
CribFetch::recvRetry()
{
    if (retryPkt != NULL) {
        assert(cacheBlocked);
        assert(retryTid != InvalidThreadID);
        assert(fetchState[retryTid].status == IcacheWaitRetry);

        if (cpu->getInstPort().sendTimingReq(retryPkt)) {
            fetchState[retryTid].status = IcacheWaitResponse;
            retryPkt = NULL;
            retryTid = InvalidThreadID;
            cacheBlocked = false;
        }
    } else {
        assert(retryTid == InvalidThreadID);
        // Access has been squashed.  Clear blocked status.
        cacheBlocked = false;
    }
}

bool
CribFetch::isDrained() const
{
    for (ThreadID i = 0; i < numThreads; i++) {
        if (!(fetchState[i].status == Idle ||
                (fetchState[i].status == Blocked &&
                 fetchState[i].stalls.drain))) {
            return false;
        }
    }

    return !finishTranslationEvent.scheduled();
}

void
CribFetch::takeOverFrom()
{
    resetStage();
}

void
CribFetch::wakeFromQuiesce()
{
    DPRINTF(CribFetch, "Waking up from quiesce.\n");

    if (numThreads != 1) {
        panic("Hello Vignyan, SMT has issues becasue of instructions like "
              "WFI/Halt/Quiesc.  Fullsystem needs to know what thread context "
              "on the CPU to wake up.  This doesnt seem to be tracked.\n");
    }

    // If SMT, this should actually be delivered to the proper thread
    fetchState[0].status = Running;
}

inline void
CribFetch::squash(const TheISA::PCState &newPC, const InstSeqNum seq_num,
        CribDynInstPtr squashInst, ThreadID tid)
{
    DPRINTF(CribFetch, "[tid:%i]: Squash from commit.\n", tid);

    doSquash(newPC, squashInst, tid);
    toDecode->ackSquash[tid] = true;
    toDecode->ackSquashSeqNum[tid] = seq_num;
    toDecode->ackPcAddr[tid] = newPC.instAddr();
    // We may have faulted and sent a page-fault nop to decode before
    // receiving this signal.  Don't send it.
    toDecode->size = 0;
}


void
CribFetch::tick()
{
    std::list<ThreadID>::iterator thread_itr = activeThreads->begin();
    std::list<ThreadID>::iterator thread_end = activeThreads->end();
    bool status_change = false;

    wroteToTimeBuffer = false;

    for (ThreadID tid = 0; tid < CribConst::MaxThreads; tid++) {
        fetchState[tid].issuePipelinedIfetch = false;
    }
    // Check the signals for each thread to determine their proper state
    // Squashes performed here.
    while (thread_itr != thread_end) {
        ThreadID tid = *thread_itr++;
        bool updated_status = checkSignalsAndUpdate(tid);
        status_change = status_change || updated_status;
    }

    DPRINTF(CribFetch, "Running stage.\n");

    // Interrupt signalling
    if (FullSystem) {
        if (fromCommit->cribInfo[0].interruptPending) {
            interruptPending = true;
        }
        if (fromCommit->cribInfo[0].clearInterrupt) {
            interruptPending = false;
        }
    }

    // Actually fetch
    fetch(status_change);

    fetchInstDist.sample(numInst);

    // Perform activity related actions
    if (status_change) {
        status = updateFetchStatus();
    }

    if (wroteToTimeBuffer || cpu->contextSwitch) {
        DPRINTF(Activity, "Activity this cycle.\n");
        cpu->recordActivity();
    }

    for (ThreadID tid = 0; tid < CribConst::MaxThreads; tid++) {
        if (fetchState[tid].issuePipelinedIfetch) {
            pipelineIcacheAccess(tid);
        }
    }

    numInst = 0;
}

void
CribFetch::finishTranslation(Fault fault, RequestPtr mem_req)
{
    ThreadID tid = mem_req->threadId();
    Addr block_pc = mem_req->getVaddr();

    assert(!cpu->switchedOut());

    cpu->wakeCPU();

    if (fetchState[tid].status != ItlbWait ||
        mem_req != fetchState[tid].memReq ||
        mem_req->getVaddr() != fetchState[tid].memReq->getVaddr()) {
        DPRINTF(CribFetch, "[tid:%i] Ignoring itlb completed after squash.\n",
                tid);
        delete mem_req;
        return;
    }

    // If translation sucessful, attempt to read cache block
    if (fault == NoFault) {
        if (!cpu->system->isMemAddr(mem_req->getPaddr())) {
            warn("Invalid memory address at fetch.  Out of physical address "
                  "range. [addr:%#x].\n", mem_req->getPaddr());
            fetchState[tid].status = NoGoodAddr;
            delete mem_req;
            fetchState[tid].memReq = NULL;
        }
        // Build packet
        PacketPtr data_pkt = new Packet(mem_req, MemCmd::ReadReq);
        data_pkt->dataDynamicArray(new uint8_t[cacheBlkSize]);

        fetchState[tid].cacheDataPC = block_pc;
        fetchState[tid].cacheDataValid = false;
        DPRINTF(CribFetch, "[tid:%i]: Doing icache read.\n", tid);

        if (!cpu->getInstPort().sendTimingReq(data_pkt)) {
            assert(retryPkt == NULL);
            assert(retryTid == InvalidThreadID);
            DPRINTF(CribFetch, "[tid:%i]: Out of MSHRS.\n", tid);

            fetchState[tid].status = IcacheWaitRetry;
            retryPkt = data_pkt;
            retryTid = tid;
            cacheBlocked = true;
        } else {
            DPRINTF(CribFetch, "[tid:%i]: Doing icache access.\n", tid);
            DPRINTF(Activity, "[tid:%i]: Waiting on icache response.\n", tid);

            fetchState[tid].status = IcacheWaitResponse;
        }
    } else {
        if (!(numInst < fetchWidth)) {
           assert(!finishTranslationEvent.scheduled());
           finishTranslationEvent.setFault(fault);
           finishTranslationEvent.setReq(mem_req);
           cpu->schedule(finishTranslationEvent, cpu->clockEdge(Cycles(1)));
           return;
        }

        DPRINTF(CribFetch, "[tid:%i]: Got icache translation address %#x, "
                "expected %#x.\n", tid, mem_req->getVaddr(),
                fetchState[tid].memReq->getVaddr());
        // Translation faulted, icache request wont be sent
        delete mem_req;
        fetchState[tid].memReq = NULL;

        if (fetchState[tid].status != Squashing) {
            // Send the fault to commit.  This thread will not do anything until
            // commit handls the fault.  Only other way we can wake up is if
            // this thread is squashed before the fault reaches commit.
            TheISA::PCState fetch_pc = fetchState[tid].pc;

            DPRINTF(CribFetch, "[tid:%i]: Translation faulted, building no-op.\n",
                    tid);
            CribDynInstPtr instruction = buildInst(tid,
                    fetchState[tid].decoder->decode(TheISA::NoopMachInst,
                        fetch_pc.instAddr()), NULL, fetch_pc, fetch_pc, false);
            instruction->setPredTarg(fetch_pc);
            instruction->fault = fault;
            wroteToTimeBuffer = true;

            DPRINTF(Activity, "Activity this cycle.\n");
            cpu->recordActivity();

            fetchState[tid].status = TrapPending;

            DPRINTF(CribFetch, "[tid:%i]: Blocked, need to handle trap.\n", tid);
            DPRINTF(CribFetch, "[tid:%i]: Fault (%s) detected [pc:%s].\n",
                    tid, fault->name(), fetchState[tid].pc);
        }
    }

    status = updateFetchStatus();
}

inline void
CribFetch::switchToActive()
{
    if (status == Inactive) {
        DPRINTF(Activity, "Activating stage.\n");
        cpu->activateStage(CribCPU::FetchIdx);
        status = Active;
    }
    numInst = 0;
}

inline void
CribFetch::switchToInactive()
{
    if (status == Active) {
        DPRINTF(Activity, "Deactivating stage.\n");
        cpu->deactivateStage(CribCPU::FetchIdx);
        status = Inactive;
    }
}

inline bool
CribFetch::checkStall(ThreadID tid) const
{
    bool ret_val = false;

    if (cpu->contextSwitch) {
        DPRINTF(CribFetch, "[tid:%i]: Stalling for a context switch.\n", tid);
        ret_val = true;
    } /*else if (fetchState[tid].stalls.decode) {
        DPRINTF(CribFetch, "[tid:%i]: Stall from Decode stage detected.\n", tid);
        ret_val = true;
    } else if (fetchState[tid].stalls.dispatch) {
        DPRINTF(CribFetch, "[tid:%i]: Stall from Dispatch stage detected.\n", tid);
        ret_val = true;
    }*/ else if (fetchState[tid].stalls.crib) {
        DPRINTF(CribFetch, "[tid:%i]: Stall from Crib stage detected.\n", tid);
        ret_val = true;
    } else if (fetchState[tid].stalls.commit) {
        DPRINTF(CribFetch, "[tid:%i]: Stall from Commit stage detected.\n", tid);
        ret_val = true;
    }

    return ret_val;
}


inline void
CribFetch::doSquash(const TheISA::PCState &newPC,
        const CribDynInstPtr squashInst, ThreadID tid)
{
    //if(squashInst){
    //    fprintf(stdout,"%lx Mispredicted. Actual:%lx\n", squashInst->pcState().instAddr(), newPC.instAddr());
    //}
    DPRINTF(CribFetch, "[tid:%i]: Squashing, setting PC to %s.\n",
            tid, newPC);

    fetchState[tid].pc = newPC;
    fetchState[tid].fetchOffset = 0;
    fetchState[tid].loopTrainInfo.valid = false;
    fetchState[tid].loopExecInfo.valid = false;

    if (squashInst && squashInst->pcState().instAddr() == newPC.instAddr()) {
        fetchState[tid].macroop = squashInst->macroop;
    } else {
        fetchState[tid].macroop = NULL;
    }

    fetchState[tid].decoder->reset();

    // Clear icache miss if its outstanding
    if (fetchState[tid].status == IcacheWaitResponse) {
        DPRINTF(CribFetch, "[tid:%i]: Squashing outstanding Icache miss.\n",
                tid);
        fetchState[tid].memReq = NULL;
    } else if (fetchState[tid].status == ItlbWait) {
        DPRINTF(CribFetch, "[tid:%i]: Squashing outstanding ITLB miss.\n",
                tid);
        fetchState[tid].memReq = NULL;
    }

    // Delete retrying packet if it was for this thread
    if (retryTid == tid) {
        assert(cacheBlocked);
        if (retryPkt) {
            delete retryPkt->req;
            delete retryPkt;
        }
        retryPkt = NULL;
        retryTid = InvalidThreadID;
    }

    fetchState[tid].status = Squashing;

    // microops are being squashed, it is not known wheather the
    // youngest non-squashed microop was  marked delayed commit
    // or not. Setting the flag to true ensures that the
    // interrupts are not handled when they cannot be, though
    // some opportunities to handle interrupts may be missed.
    fetchState[tid].delayedCommit = true;

    ++fetchSquashCycles;
    /*
    if (useUopBuffer) {
        uopBuffer.squash();
        uopBufferIndex = -1;
        uopBufferOffset = 0;
    }
    */
    if(useUopCache){
        CribDynInstPtr minst = uopCache.lookup(newPC, false);
        if(minst) uopBufferIndex = 1;
        else uopBufferIndex = -1;
    }

}

inline bool
CribFetch::checkSignalsAndUpdate(ThreadID tid)
{
    // Handle loop training info
    if (fromDispatch->dispatchInfo[tid].loopSizeFail) {
        lat.updateMaxUnroll(fromDispatch->dispatchInfo[tid].loopAddr,
                fromDispatch->dispatchInfo[tid].unrollFactor > 0 ?
                fromDispatch->dispatchInfo[tid].unrollFactor - 1 : 0);
    }

    // Update the per thread stall statuses
    if (fromDecode->decodeBlock[tid]) {
        fetchState[tid].stalls.decode = true;
    }

    if (fromDecode->decodeUnblock[tid] ) {
        assert(fetchState[tid].stalls.decode);
        assert(!fromDecode->decodeBlock[tid]);
        fetchState[tid].stalls.decode = false;
    }

    if (fromCrib->cribBlock[tid]) {
        fetchState[tid].stalls.crib = true;
    }

    if (fromCrib->cribUnblock[tid] ) {
        assert(fetchState[tid].stalls.crib);
        assert(!fromDecode->cribBlock[tid]);
        fetchState[tid].stalls.crib = false;
    }

    if (fromCommit ->commitBlock[tid]) {
        fetchState[tid].stalls.commit = true;
    }

    if (fromCommit->commitUnblock[tid] ) {
        assert(fetchState[tid].stalls.commit);
        assert(!fromDecode->commitBlock[tid]);
        fetchState[tid].stalls.commit = false;
    }

    // Check squash signals from commit
    if (fromCommit->cribInfo[tid].squash) {
        DPRINTF(CribFetch, "[tid:%i]: Squashing instructions due to squash "
                "from commit.\n", tid);

        while(bkpBpr.size()){
            if(bkpBpr.front()->isn > fromCommit->cribInfo[tid].doneSeqNum){
                //fprintf(stdout,"Restoring br %lx ==> %lx\n", bkpBpr.front()->bPC, bkpBpr.front()->tPC);
                bpRecords.push_front(bkpBpr.front());
                bkpBpr.pop_front();
            }else{
                break;
            }
        }
        /*
        if(bkpBpr.size()){
            for(int i=(bkpBpr.size()-1); i>=0; i--){
                if(bkpBpr[i]->bPC == fromCommit->cribInfo[tid].pc.instAddr()){
                    for(int j=0; j<i; j++){
                        fprintf(stdout,"Restoring br %lx ==> %lx\n", bkpBpr.front()->bPC, bkpBpr.front()->tPC);
                        bpRecords.push_front(bkpBpr.front());
                        bkpBpr.pop_front();
                    }
                    break;
                }
            }
        }
        */

        // In any case squash
        squash(fromCommit->cribInfo[tid].pc,
               fromCommit->cribInfo[tid].doneSeqNum,
               fromCommit->cribInfo[tid].squashInst, tid);

        // If it was a branch mispredict on a control instruction, update the
        // branch predictor with that instruction, otherwise just kill the
        // invalid state we generated in after the sequence number
        if (fromCommit->cribInfo[tid].mispredictInst &&
            fromCommit->cribInfo[tid].mispredictInst->isControl() ) {
            bpred->squash(fromCommit->cribInfo[tid].doneSeqNum,
                          fromCommit->cribInfo[tid].pc,
                          fromCommit->cribInfo[tid].branchTaken,
                          tid);
            //printf("%lu Predict Miss:%lx\n", fromCommit->cribInfo[tid].doneSeqNum, fromCommit->cribInfo[tid].pc.instAddr()); 

            if (fromCommit->cribInfo[tid].mispredictInst->hasFallthrough()) {
                lat.remove(fromCommit->cribInfo[tid].mispredictInst->getStartAddr());
                //lat.recordMispredict(fromCommit->cribInfo[tid].mispredictInst->getStartAddr());
            }
        } else {
            bpred->squash(fromCommit->cribInfo[tid].doneSeqNum, tid);
        }

        if (fromCommit->cribInfo[tid].drain) {
            cpu->commitDrainFinished();
            drainPending = true;
            fetchState[tid].status = Idle;
        }

        return true;
    } else if (fromCommit->cribInfo[tid].doneSeqNum) {
        // Tell predictor an instruction finished properly
        bpred->update(fromCommit->cribInfo[tid].doneSeqNum, tid);
        while(bkpBpr.size() && (bkpBpr.back()->isn <= fromCommit->cribInfo[tid].doneSeqNum)){
            //fprintf(stdout,"Deleting br %lx ==> %lx\n", bkpBpr.back()->bPC, bkpBpr.back()->tPC);
            delete bkpBpr.back();
            bkpBpr.pop_back();
        }
        //printf("%lu Predict update:\n", fromCommit->cribInfo[tid].doneSeqNum); 
    }

    if (fromDecode->decodeInfo[tid].squash) {
        DPRINTF(CribFetch, "[tid:%i]: Squashing instructions due to squash "
                "from decode.\n", tid);

        // Update branch predictor
        if (fromDecode->decodeInfo[tid].squash) {
            bpred->squash(fromDecode->decodeInfo[tid].doneSeqNum,
                          fromDecode->decodeInfo[tid].nextPC,
                          fromDecode->decodeInfo[tid].branchTaken, tid);
        } else {
            bpred->squash(fromDecode->decodeInfo[tid].doneSeqNum, tid);
        }

        if (fetchState[tid].status != Squashing) {
            DPRINTF(CribFetch, "[tid:%i]: Squashing from decode with PC "
                    "%s.\n", tid, fromDecode->decodeInfo[tid].nextPC);
            // Squash unless we're already squashing
            squashFromDecode(fromDecode->decodeInfo[tid].nextPC,
                             fromDecode->decodeInfo[tid].squashInst,
                             fromDecode->decodeInfo[tid].doneSeqNum, tid);
            return true;
        }
    }

    if (checkStall(tid) &&
        fetchState[tid].status != IcacheWaitResponse &&
        fetchState[tid].status != IcacheWaitRetry &&
        fetchState[tid].status != ItlbWait) {

        DPRINTF(CribFetch, "[tid:%i]: Setting to blocked.\n", tid);
        fetchState[tid].status = Blocked;
        return true;
    }

    if (fetchState[tid].status == Blocked ||
        fetchState[tid].status == Squashing) {

        DPRINTF(CribFetch, "[tid:%i]: Done squashing, switching to running.\n",
                tid);
        fetchState[tid].status = Running;
        return true;
    }

    // If we've reached this point, we have not gotten any signals that cause
    // fetch to change its status.  Fetch remains the same as before.
    return false;
}


inline void
CribFetch::squashFromDecode(const TheISA::PCState &newPC,
                            const CribDynInstPtr squashInst,
                            const InstSeqNum seq_num, ThreadID tid)
{
    DPRINTF(CribFetch, "[tid:%i]: Squashing from decode.\n", tid);
    doSquash(newPC, squashInst, tid);
    toDecode->ackSquash[tid] = true;
    toDecode->ackSquashSeqNum[tid] = seq_num;
    toDecode->ackPcAddr[tid] = newPC.instAddr();
    toDecode->size = 0;
}

inline void
CribFetch::fetch(bool &status_change)
{
    ThreadID tid = getFetchingThread(fetchPolicy);

    assert(!cpu->switchedOut());

    if (tid == InvalidThreadID || drainPending ||
        fetchState[tid].status == Squashing) {
        return;
    }

    DPRINTF(CribFetch, "[tid:%i]: Attempting to fetch.\n", tid);

    TheISA::PCState thisPC = fetchState[tid].pc;
    Addr pcOffset = fetchState[tid].fetchOffset;
    Addr fetchAddr = (thisPC.instAddr() + pcOffset) & BaseCPU::PCMask;

    bool inRom = isRomMicroPC(thisPC.microPC());
    //if (!useUopBuffer || uopBufferIndex == -1)
    if (!useUopCache || uopBufferIndex == -1)
    {

        // Update status to running if icache miss complete
        if (fetchState[tid].status == IcacheAccessComplete) {
            DPRINTF(CribFetch, "[tid:%i]: Icache miss complete.\n", tid);

            fetchState[tid].status = Running;
            status_change = true;
        } else if (fetchState[tid].status == Running) {
            Addr block_pc = icacheBlockAlignPC(fetchAddr);

            // If buffer is no longer valid or fetchAddr has moved point to the
            // next cache block, AND we have no remaining ucode from a macro-op,
            // then start a fetch from the icache
            if (!(fetchState[tid].cacheDataValid &&
                block_pc == fetchState[tid].cacheDataPC)
                && !inRom && !fetchState[tid].macroop) {
                DPRINTF(CribFetch, "[tid:%i]: Attempting to translate and read "
                        "instruction starting at [pc:%s].\n", tid, thisPC);

                fetchCacheLine(fetchAddr, tid, thisPC.instAddr());

                if (fetchState[tid].status == IcacheWaitResponse) {
                    ++icacheStallCycles;
                } else if (fetchState[tid].status == ItlbWait) {
                    ++fetchTlbCycles;
                } else {
                    ++fetchMiscStallCycles;
                }

                return;
            } else if ((checkInterrupt(thisPC.instAddr())
                        && !fetchState[tid].delayedCommit)) {
                // Stall CPU if an interrupt is posted and we're not issuing a
                // delayed commit micro-op currently (delayed commit instructions
                // are not interruptable by interrupts, only faults).
                ++fetchMiscStallCycles;
                DPRINTF(CribFetch, "[tid:%i]: Fetch stalled.\n", tid);

                return;
            }
        } else {
            if (fetchState[tid].status == Idle) {
                ++fetchIdleCycles;
                DPRINTF(CribFetch, "[tid:%i]: Fetch idle.\n", tid);
            }
            // Status is idle, do nothing
            return;
        }
    }
    ++fetchCycles;
    TheISA::PCState nextPC = thisPC;

    StaticInstPtr staticInst = NULL;
    StaticInstPtr curMacroop = fetchState[tid].macroop;

    // If the read of the first instruction was sucessful, then grab the
    // instructions from the rest of the cacheline and put them into the
    // queue heading to decode.

    DPRINTF(CribFetch, "[tid:%i]: Adding instructions to queue to decode.\n",
            tid);

    // Need to keep track if a predicted branch ended this fetch block
    bool predictedBranch = false;

    TheISA::MachInst *cacheInsts =
        reinterpret_cast<TheISA::MachInst *>(fetchState[tid].cacheData);

    const unsigned numInsts = cacheBlkSize / instSize;
    unsigned blkOffset = (fetchAddr - fetchState[tid].cacheDataPC) / instSize;

    // Loop through instruction memory from the cache.
    // Keep issuing while fetchWidth is avaiable and branch is not
    // predited taken.
    while (numInst < fetchWidth && !predictedBranch) {
        // We need to process more memory if we aren't going to get a
        // StaticInst from the rom, current macroop, or what's already in
        // the decoder.
        
        if (!useUopCache || uopBufferIndex == -1) {
            bool needMem = !inRom && !curMacroop &&
                           !fetchState[tid].decoder->instReady();

            fetchAddr = (thisPC.instAddr() + pcOffset) & BaseCPU::PCMask;
            Addr block_pc = icacheBlockAlignPC(fetchAddr);

            if (needMem) {
                // If buffer is no longer valid or fetchAddr has moved to point
                // to the next cache block then start fetch from icache
                if (!fetchState[tid].cacheDataValid ||
                    block_pc != fetchState[tid].cacheDataPC) {
                    break;
                }

                if (blkOffset >= numInsts) {
                    // We need to process more memory, but we've run out of the
                    // current block
                    break;
                }

                if (ISA_HAS_DELAY_SLOT && pcOffset == 0) {
                    // Walk past any annuled delay slot instructions
                    Addr pcAddr = thisPC.instAddr() & BaseCPU::PCMask;
                    while (fetchAddr != pcAddr && blkOffset < numInsts) {
                        blkOffset++;
                        fetchAddr += instSize;
                    }
                    if (blkOffset >= numInsts) break;
                }

                MachInst inst = TheISA::gtoh(cacheInsts[blkOffset]);

                fetchState[tid].decoder->moreBytes(thisPC, fetchAddr, inst);

                if (fetchState[tid].decoder->needMoreBytes()) {
                    blkOffset++;
                    fetchAddr += instSize;
                    pcOffset += instSize;
                }
            }
        }
        // Extract as many instructions and/or microops as we can from the
        // memory we've processed so far
        do {
            bool loop_end = false;
            bool unroll_end = false;

            if (useUopCache && uopBufferIndex != -1) {
                CribDynInstPtr minst = uopCache.lookup(thisPC, true);
                assert(minst);
                staticInst = minst->staticInst;
                thisPC = minst->pcState();
                ++uopBufferFetch;
                DPRINTF(CribFetch, "Servicing %lx,%d from uopCache\n",thisPC.instAddr(), thisPC.microPC());
            } else if (!(curMacroop || inRom)) {
                if (fetchState[tid].decoder->instReady()) {
                    staticInst = fetchState[tid].decoder->decode(thisPC);

                    ++fetchedInsts;

                    if (staticInst->isMacroop()) {
                        curMacroop = staticInst;
                    } else {
                        pcOffset = 0;
                    }
                } else {
                    // We need more bytes for this instruction so blkOffset and
                    // pcOffset will be updated
                    break;
                }
            }

            // Whether we're moving to a new macroop because we're at the end
            // of the current one, or the branch predictor incorrectly thinks
            // we are
            bool newMacro = false;
            //if (useUopBuffer && uopBufferIndex != -1)
            if (useUopCache && uopBufferIndex != -1)
            {
                newMacro |= staticInst->isLastMicroop();
            } else if (curMacroop || inRom) {
                if (inRom) {
                    staticInst = cpu->microcodeRom.fetchMicroop(
                            thisPC.microPC(), curMacroop);
                } else {
                    staticInst = curMacroop->fetchMicroop(thisPC.microPC());
                }
                newMacro |= staticInst->isLastMicroop();
            }

            CribDynInstPtr instruction =
                buildInst(tid, staticInst, curMacroop, thisPC, nextPC, true);

            if(useUopCache){
                uopCache.add(instruction);
            }

            // Perform loop analysis
            Addr inst_addr = thisPC.instAddr();
            Addr end_addr;
            Addr fallthrough_addr;
            RegSet loop_w_set;
            unsigned loop_num_insts;
            unsigned loop_num_partitions;
            unsigned max_unroll;
            if (cribLoopEnable &&
                !fetchState[tid].loopExecInfo.valid &&
                lat.lookup(inst_addr, end_addr, fallthrough_addr, loop_w_set, loop_num_insts,
                           loop_num_partitions, max_unroll) &&
                           loop_num_insts <= cribPartitionSize * cribPartitions &&
                           loop_num_partitions <= cribPartitions ) {
                DPRINTF(CribLoop, "[sn:%lli]: Lookup hit: [%#x->%#x] (insts:%d, parts:%d).\n",
                        instruction->seqNum,
                        inst_addr, end_addr, loop_num_insts, loop_num_partitions);
                fetchState[tid].loopExecInfo.valid = true;
                fetchState[tid].loopExecInfo.startAddr = instruction->pcState();
                fetchState[tid].loopExecInfo.endAddr = end_addr;
                fetchState[tid].loopExecInfo.fallthroughAddr = fallthrough_addr;
                fetchState[tid].loopExecInfo.numInsts = loop_num_insts;
                fetchState[tid].loopExecInfo.numPartitions = loop_num_partitions;
                fetchState[tid].loopExecInfo.writtenRegs = loop_w_set;
                fetchState[tid].loopExecInfo.loopStart = instruction;
                fetchState[tid].loopExecInfo.numInstsDispatched = 0;
                fetchState[tid].loopExecInfo.unrollFactor = 0;

                globalLoopColor++;
                instruction->setLoopStart();

                if (cribLoopUnroll && loop_num_partitions <= cribPartitions/2) {
                    DPRINTF(CribLoop, "[sn:%lli]: Found candidate for unrolling [insts:%d][parts:%d].\n",
                            instruction->seqNum, loop_num_insts, loop_num_partitions);
                    unsigned unroll_factor = std::min(cribPartitions/loop_num_partitions - 1, max_unroll);
                    fetchState[tid].loopExecInfo.unrollCount = unroll_factor;
                    fetchState[tid].loopExecInfo.unrollFactor = unroll_factor;
                 } else {
                    fetchState[tid].loopExecInfo.unrollCount = 0;
                 }

                fetchState[tid].loopTrainInfo.valid = false;
            }

            instruction->loopColor = globalLoopColor;

            if (fetchState[tid].loopExecInfo.valid) {
                instruction->setFallthrough(fetchState[tid].loopExecInfo.fallthroughAddr);
                instruction->setStartAddr(fetchState[tid].loopExecInfo.startAddr.instAddr());
                instruction->setUnrollFactor(fetchState[tid].loopExecInfo.unrollFactor);
                fetchState[tid].loopExecInfo.numInstsDispatched++;
                DPRINTF(CribLoop, "[sn:%lli]: Loop Inst Dispatched: %i. [pc:%s]\n",
                        instruction->seqNum, fetchState[tid].loopExecInfo.numInstsDispatched,
                        instruction->pcState());

                if (fetchState[tid].loopExecInfo.numInstsDispatched ==
                    fetchState[tid].loopExecInfo.numInsts) {
                    if (fetchState[tid].loopExecInfo.endAddr == inst_addr) {
                        DPRINTF(CribLoop, "[sn:%lli]: Sucessful loop dispatch [insts:%d][partitions:%d].\n",
                                instruction->seqNum,
                                fetchState[tid].loopExecInfo.numInsts,
                                fetchState[tid].loopExecInfo.numPartitions);

                        loop_end = true;

                        if (fetchState[tid].loopExecInfo.unrollCount != 0) {
                            fetchState[tid].loopExecInfo.unrollCount--;
                            instruction->setLoopUnrollEnd();
                            fetchState[tid].loopExecInfo.numInstsDispatched = 0;
                            unroll_end = true;
                        } else {
                            globalLoopColor++;
                            instruction->setLoopEnd(fetchState[tid].loopExecInfo.writtenRegs);
                        }
                    } else {
                        DPRINTF(CribLoop, "[sn:%lli]: Failed loop dispatch.\n",
                                instruction->seqNum);
                        instruction->setLoopFail();
                        lat.remove(fetchState[tid].loopExecInfo.startAddr.instAddr());
                        fetchState[tid].loopExecInfo.valid = false;
                    }
                }
            }

            if (fetchState[tid].loopTrainInfo.valid) {
                if (instruction->isThreadSync() ||
                    instruction->isSerializing()  ||
                    instruction->isMemBarrier() ||
                    instruction->isStoreConditional() ||
                    instruction->isNonSpeculative() ||
                    instruction->isWriteBarrier() ||
                    instruction->isQuiesce() ||
                    instruction->isIprAccess() ||
                    instruction->isSyscall()) {
                    fetchState[tid].loopTrainInfo.valid = false;
                    DPRINTF(CribLoop, "[sn:%lli]: Disabling loop training.\n",
                            instruction->seqNum);
                } else {

                    fetchState[tid].loopTrainInfo.readSet = fetchState[tid].loopTrainInfo.readSet |
                        (instruction->srcRegSet() & ~fetchState[tid].loopTrainInfo.writeSet);
                    fetchState[tid].loopTrainInfo.writeSet |= instruction->destRegSet();

                    if (instruction->isStore() && fetchState[tid].loopTrainInfo.curPartitionStores >= cribStoresPerPartition) {
                        fetchState[tid].loopTrainInfo.requiredPartitions++;
                        fetchState[tid].loopTrainInfo.curPartitionStores = 0;
                        fetchState[tid].loopTrainInfo.curPartitionLoads = 0;
                        fetchState[tid].loopTrainInfo.curPartitionInsts = 0;
                    }

                    if (instruction->isLoad() && fetchState[tid].loopTrainInfo.curPartitionLoads >= cribLoadsPerPartition) {
                        fetchState[tid].loopTrainInfo.requiredPartitions++;
                        fetchState[tid].loopTrainInfo.curPartitionStores = 0;
                        fetchState[tid].loopTrainInfo.curPartitionLoads = 0;
                        fetchState[tid].loopTrainInfo.curPartitionInsts = 0;
                    }
                    fetchState[tid].loopTrainInfo.curPartitionInsts++;

                    if (fetchState[tid].loopTrainInfo.curPartitionInsts > cribPartitionSize) {
                        fetchState[tid].loopTrainInfo.requiredPartitions++;
                        fetchState[tid].loopTrainInfo.curPartitionStores = 0;
                        fetchState[tid].loopTrainInfo.curPartitionLoads = 0;
                        fetchState[tid].loopTrainInfo.curPartitionInsts = 0;
                    }

                    if (instruction->isStore()) {
                        fetchState[tid].loopTrainInfo.curPartitionStores++;
                    }
                    if (instruction->isLoad()) {
                        fetchState[tid].loopTrainInfo.curPartitionLoads++;
                    }

                    fetchState[tid].loopTrainInfo.numInsts++;
                }
            }

            numInst++;
#if TRACING_ON
            instruction->fetchTick = curTick();
#endif
            nextPC = thisPC;

            // If we're branching after this instruction, quit fetching from
            // the same block
            predictedBranch |= thisPC.branching();
            bool br_taken = lookupAndUpdateNextPC(tid, instruction, nextPC, loop_end, unroll_end);
            predictedBranch |= br_taken;

            // Test for a pc-relative branch
            if (predictedBranch && instruction->isDirectCtrl() && /*instruction->isCondCtrl() &&*/ !fetchState[tid].loopExecInfo.valid) {
                // Verify this is a backwards branch
                Addr branch_target = instruction->branchTarget().instAddr();
                if (branch_target < inst_addr) {
                    DPRINTF(CribLoop, "[sn:%lli]: Possible loop branch.\n", instruction->seqNum);

                    if (fetchState[tid].loopTrainInfo.valid &&
                        fetchState[tid].loopTrainInfo.branchAddr == inst_addr) {
                        RegSet loop_carried = fetchState[tid].loopTrainInfo.readSet & fetchState[tid].loopTrainInfo.writeSet;
                        RegSet loop_carried_omit = loop_carried;
                        loop_carried_omit.reset(TheISA::INTREG_CONDCODES_NZ);
                        loop_carried_omit.reset(TheISA::INTREG_CONDCODES_C);
                        loop_carried_omit.reset(TheISA::INTREG_CONDCODES_V);
                        loop_carried_omit.reset(TheISA::INTREG_CONDCODES_GE);
                        loop_carried_omit.reset(TheISA::INTREG_FPCONDCODES);

                        DPRINTF(CribLoop, "[sn:%lli]: Hit training address [%#x->%#x] %d. Carried=%d\n",
                                instruction->seqNum,
                                fetchState[tid].loopTrainInfo.targetAddr,
                                fetchState[tid].loopTrainInfo.branchAddr,
                                fetchState[tid].loopTrainInfo.numInsts,
                                loop_carried_omit.count());
                        if (loop_carried_omit.count() <= cribLoopDependencies) {
                            if (fetchState[tid].loopTrainInfo.requiredPartitions > cribPartitions) {
                                DPRINTF(CribLoop, "[%#x->%#x] Not training, too many partitions (%d).\n",
                                        fetchState[tid].loopTrainInfo.targetAddr,
                                        fetchState[tid].loopTrainInfo.branchAddr,
                                        fetchState[tid].loopTrainInfo.requiredPartitions);
                            } else if (fetchState[tid].loopTrainInfo.requiredPartitions == 1 && !cribLoopUnroll) {
                                DPRINTF(CribLoop, "[%#x->%#x] Not training, too few partitions (%d).\n",
                                        fetchState[tid].loopTrainInfo.targetAddr,
                                        fetchState[tid].loopTrainInfo.branchAddr,
                                        fetchState[tid].loopTrainInfo.requiredPartitions);
                            } else {
                                DPRINTF(CribLoop, "[%#x->%#x] Storing loop training data [partitions:%d].\n",
                                        fetchState[tid].loopTrainInfo.targetAddr,
                                        fetchState[tid].loopTrainInfo.branchAddr,
                                        fetchState[tid].loopTrainInfo.requiredPartitions);
                                lat.update(branch_target,
                                           inst_addr,
                                           inst_addr + instruction->pcState().size(),
                                           fetchState[tid].loopTrainInfo.writeSet,
                                           fetchState[tid].loopTrainInfo.numInsts,
                                           fetchState[tid].loopTrainInfo.requiredPartitions);
                                fetchState[tid].loopTrainInfo.valid = false;
                            }
                        } else {
                            DPRINTF(CribLoop, "[%#x->%#x] Not training, too many loop carried dependencies (%d).\n",
                                    fetchState[tid].loopTrainInfo.targetAddr,
                                    fetchState[tid].loopTrainInfo.branchAddr,
                                    loop_carried_omit.count());
                            fetchState[tid].loopTrainInfo.branchAddr = inst_addr;
                            fetchState[tid].loopTrainInfo.targetAddr = branch_target;
                            fetchState[tid].loopTrainInfo.writeSet.reset();
                            fetchState[tid].loopTrainInfo.readSet.reset();
                            fetchState[tid].loopTrainInfo.requiredPartitions = 1;
                            fetchState[tid].loopTrainInfo.curPartitionLoads = 0;
                            fetchState[tid].loopTrainInfo.curPartitionStores = 0;
                            fetchState[tid].loopTrainInfo.curPartitionInsts = 0;
                            DPRINTF(CribLoop, "[sn:%lli]: Training address [%#x->%#x].\n",
                                    instruction->seqNum,
                                    fetchState[tid].loopTrainInfo.branchAddr,
                                    fetchState[tid].loopTrainInfo.targetAddr);

                        }

                    } else {
                        if (cribLoopEnable) {
                            fetchState[tid].loopTrainInfo.valid = true;
                            fetchState[tid].loopTrainInfo.branchAddr = inst_addr;
                            fetchState[tid].loopTrainInfo.targetAddr = branch_target;
                            fetchState[tid].loopTrainInfo.writeSet.reset();
                            fetchState[tid].loopTrainInfo.readSet.reset();
                            fetchState[tid].loopTrainInfo.requiredPartitions = 1;
                            fetchState[tid].loopTrainInfo.curPartitionLoads = 0;
                            fetchState[tid].loopTrainInfo.curPartitionStores = 0;
                            fetchState[tid].loopTrainInfo.curPartitionInsts = 0;
                            DPRINTF(CribLoop, "[sn:%lli]: Training address [%#x->%#x].\n",
                                    instruction->seqNum,
                                    fetchState[tid].loopTrainInfo.branchAddr,
                                    fetchState[tid].loopTrainInfo.targetAddr);
                        }
                    }
                    fetchState[tid].loopTrainInfo.numInsts = 0;
                }
            }

            if (loop_end && !unroll_end) {
                fetchState[tid].loopExecInfo.valid = false;
            }

            if (predictedBranch) {
                DPRINTF(CribFetch, "[tid:%i]: Branch detected [pc:%s].\n",
                        tid, thisPC);
            }

            newMacro |= thisPC.instAddr() != nextPC.instAddr();

            // Move to the next instruction, unless we branch
            thisPC = nextPC;
            inRom = isRomMicroPC(thisPC.microPC());

            if (newMacro) {
                fetchAddr = thisPC.instAddr() & BaseCPU::PCMask;
                blkOffset = (fetchAddr - fetchState[tid].cacheDataPC)
                            / instSize;
                pcOffset = 0;
                curMacroop = NULL;
            }
            if (useUopCache) {
                int prev_ubi = uopBufferIndex;
                CribDynInstPtr minst= uopCache.lookup(thisPC, false);
                if(minst) uopBufferIndex = 1;
                else uopBufferIndex = -1;
                if((prev_ubi==1) && (instruction->isControl() || (uopBufferIndex==-1))) break;
            }

            if (instruction->isQuiesce()) {
                DPRINTF(CribFetch, "Quiesce instruction.  Halting fetch!\n");
                fetchState[tid].status = QuiescePending;
                status_change = true;
                break;
            }
        } while (((useUopCache && uopBufferIndex!=-1) ||   (curMacroop || fetchState[tid].decoder->instReady())) &&
                 numInst < fetchWidth);
    }

    if (predictedBranch) {
        DPRINTF(CribFetch, "[tid:%i]: Done fetching, predicted branch "
                "instruction encountered.\n", tid);
    } else if (numInst >= fetchWidth) {
        DPRINTF(CribFetch, "[tid:%i]: Done fetching, reached fetch "
                "bandwidth for this cycle.\n", tid);
    } else if (blkOffset > cacheBlkSize) {
        DPRINTF(CribFetch, "[tid:%i]: Done fetching, reached end of cache"
                "block.\n", tid);
    }

    fetchState[tid].macroop = curMacroop;
    fetchState[tid].fetchOffset = pcOffset;

    if (numInst > 0) wroteToTimeBuffer = true;

    fetchState[tid].pc = thisPC;

    // pipeline a fetch if we're crossing a cache boundary and not in a
    // state that would preclude fetching
    fetchAddr = (thisPC.instAddr() + pcOffset) & BaseCPU::PCMask;
    Addr block_pc = icacheBlockAlignPC(fetchAddr);
    fetchState[tid].issuePipelinedIfetch =
        block_pc != fetchState[tid].cacheDataPC &&
        fetchState[tid].status != IcacheWaitResponse &&
        fetchState[tid].status != ItlbWait &&
        fetchState[tid].status != IcacheWaitRetry &&
        fetchState[tid].status != QuiescePending &&
        !curMacroop;
}

inline CribFetch::FetchStatus
CribFetch::updateFetchStatus()
{
    std::list<ThreadID>::iterator thread_itr = activeThreads->begin();
    std::list<ThreadID>::iterator thread_end = activeThreads->end();

    while (thread_itr != thread_end) {
        ThreadID tid = *thread_itr++;

        if(fetchState[tid].status == Running ||
           fetchState[tid].status == Squashing ||
           fetchState[tid].status == IcacheAccessComplete) {
            if (status == Inactive) {
                DPRINTF(Activity, "[tid:%i]: Activating stage.\n", tid);
                if (fetchState[tid].status == IcacheAccessComplete) {
                    DPRINTF(Activity, "[tid:%i]: Activating fetch due to cache "
                            "completion.\n", tid);
                }
                cpu->activateStage(CribCPU::FetchIdx);
            }
        }
        return Active;
    }

    if (status == Active) {
        DPRINTF(Activity, "Deactivating stage.\n");
        cpu->deactivateStage(CribCPU::FetchIdx);
    }

    return Inactive;
}

inline ThreadID
CribFetch::getFetchingThread(FetchPriority priority)
{
    switch (priority) {
      case SingleThread: return fetchSingleThread();
      case RoundRobin:   return fetchRoundRobin();
      default: return InvalidThreadID;
    }
}

inline ThreadID
CribFetch::fetchSingleThread()
{
    std::list<ThreadID>::iterator thread = activeThreads->begin();
    if (thread == activeThreads->end()) {
        return InvalidThreadID;
    }

    ThreadID tid = *thread;
    if (fetchState[tid].status == Running ||
        fetchState[tid].status == IcacheAccessComplete ||
        fetchState[tid].status == Idle) {
        return tid;
    } else {
        return InvalidThreadID;
    }
}

inline ThreadID
CribFetch::fetchRoundRobin()
{
    std::list<ThreadID>::iterator pri_iter = threadPriority.begin();
    std::list<ThreadID>::iterator end      = threadPriority.end();

    ThreadID high_pri;

    while (pri_iter != end) {
        high_pri = *pri_iter;

        assert(high_pri <= numThreads);

        if (fetchState[high_pri].status == Running ||
            fetchState[high_pri].status == IcacheAccessComplete ||
            fetchState[high_pri].status == Idle) {

            threadPriority.erase(pri_iter);
            threadPriority.push_back(high_pri);

            return high_pri;
        }

        pri_iter++;
    }
    return InvalidThreadID;
}


inline bool
CribFetch::fetchCacheLine(Addr vaddr, ThreadID tid, Addr pc)
{
    Fault fault = NoFault;

    assert(!cpu->switchedOut());

    if (cacheBlocked) {
        DPRINTF(CribFetch, "[tid:%i]: Fetch stalled - cache blocked.\n", tid);
        return false;
    } else if (checkInterrupt(pc) && !fetchState[tid].delayedCommit) {
        DPRINTF(CribFetch, "[tid:%i]: Fetch stalled - interrupt pending.\n",
                tid);
        return false;
    }

    Addr block_pc = icacheBlockAlignPC(vaddr);

    DPRINTF(CribFetch, "[tid:%i]: Fetching cache line for %#x for addr %#x.\n",
            tid, block_pc, vaddr);

    // Setup the memReq to do a read of the first instruction's address.
    // Set the appropriate read size and flags as well.
    RequestPtr mem_req =
        new Request(tid, block_pc, cacheBlkSize, Request::INST_FETCH,
                    cpu->instMasterId(), pc, cpu->getThreadState(tid)->contextId(),
                    tid);

    fetchState[tid].memReq = mem_req;
    fetchState[tid].status = ItlbWait;
    FetchTranslation *trans = new FetchTranslation(this);
    cpu->getITBPtr()->translateTiming(mem_req, cpu->getThreadState(tid)->getTC(), trans,
                              BaseTLB::Execute);
    return true;
}

inline CribDynInstPtr
CribFetch::buildInst(ThreadID tid, StaticInstPtr staticInst,
                     StaticInstPtr curMacroop, TheISA::PCState thisPC,
                     TheISA::PCState nextPC, bool trace)
{
    InstSeqNum seq = globalSeqNum++;
    fetchedOps++;

    CribDynInstPtr instruction(new CribDynInst(staticInst, curMacroop, thisPC, nextPC, seq, cpu));

    instruction->setTid(tid);
    instruction->setASID(tid);
    instruction->setThreadState(cpu->getThreadState(tid));

    DPRINTF(CribFetch, "[tid:%i]: Instruction [pc:%#x (%d)] created. ThumbMode(%d)"
            "[sn:%lli].\n", tid, thisPC.instAddr(), thisPC.microPC(), thisPC.thumb(),seq);
    //DPRINTF(CribFetch, "[tid:%i][sn:%lli]: Instruction - %s.\n", tid, seq,
    //        instruction->staticInst->disassemble(thisPC.instAddr()));

#if TRACING_ON
    if (trace) {
        instruction->traceData =
            cpu->getTracer()->getInstRecord(curTick(), cpu->tcBase(tid),
                    instruction->staticInst, thisPC, curMacroop);
    }
#else
    instruction->traceData = NULL;
#endif

    // Write the instruction to the first slot in the queue to decode
    assert(numInst < fetchWidth);
    toDecode->insts[toDecode->size++] = instruction;
    /*
    if(instruction->pcState().instAddr() == 0x12c10){
        fprintf(stdout, "Adding 0x12c10 to decode\n");
        for (int8_t i = 0; i < instruction->staticInst->numSrcRegs(); i++) {
            int reg = instruction->staticInst->srcRegIdx(i);
            if(reg < TheISA::FP_Base_DepTag){
                reg = cpu->isa[0]->flattenIntIndex(reg);
                fprintf(stdout, "srcs %d: %d\n", i, reg);
            }
        }
    }
    */

    // Keep track of if we can take an interrupt at this boundary
    fetchState[tid].delayedCommit = instruction->isDelayedCommit();

    return instruction;
}

inline bool
CribFetch::lookupAndUpdateNextPC(ThreadID tid, CribDynInstPtr inst,
                                 TheISA::PCState &nextPC, bool loop_end,
                                 bool unroll_end)
{
    // Do branch prediction here.
    // nextPC = current pc, update it to point to the proper nextPC
    if (!inst->isControl()) {
        TheISA::advancePC(nextPC, inst->staticInst);
        inst->setPredTarg(nextPC);
        inst->setPredTaken(false);
        return false;
    }

    if (loop_end) {
        if (!unroll_end) {
            DPRINTF(CribLoop, "[sn:%lli]: Forcing frontend to fallthrough on loop"
                    " end.\n", inst->seqNum);
        } else if (unroll_end) {
            DPRINTF(CribLoop, "[sn:%lli]: Forcing frontend to branch on "
                    "loop unrolling end.\n", inst->seqNum);
        }
    }
    bool predict_taken;
    predict_taken = bpred->predict(inst->staticInst, inst->seqNum,
                                       nextPC, tid, loop_end, unroll_end,
                                       fetchState[tid].loopExecInfo.startAddr);
    if(!dumpBpred && idealPred){
        //fprintf(stderr, "Actual BPRED: %lx==>%lx(%d)==>%lx\n", inst->pcState().instAddr(), nextPC.instAddr(), nextPC.thumb(),nextPC.npc());
        //if(inst->pcState().instAddr()==0x46574) {
        //    printf(".");
        //}
        if(bpRecords.size() && (bpRecords.front()->bPC == inst->pcState().instAddr())){
            if (inst->isDirectCtrl() && inst->isUncondCtrl()) {
                nextPC = inst->branchTarget();
                predict_taken = true;
            }else if(inst->isDirectCtrl() && (inst->branchTarget().instAddr() == bpRecords.front()->tPC)){
                nextPC = inst->branchTarget();
                predict_taken = true;
            }else{
                TheISA::PCState npc = inst->pcState();
                TheISA::advancePC(npc, inst->staticInst);
                nextPC = npc;
                nextPC.writeFlags(bpRecords.front()->tFlg);
                nextPC.writeNextFlags(bpRecords.front()->tNflg);
                nextPC.itstate(bpRecords.front()->tIts);
                nextPC.nextItstate(bpRecords.front()->tNits);
                nextPC.size(bpRecords.front()->tSize);
                nextPC.set(bpRecords.front()->tPC);
                if(npc.instAddr() != nextPC.instAddr()){
                    predict_taken = true;
                }else{
                    predict_taken = false;
                }
            }
            //fprintf(stderr, "Ideal BPRED: %lx==>%lx(%d)==>%lx\n", bpRecords.front()->bPC, nextPC.instAddr(), nextPC.thumb(),nextPC.npc());
            //fprintf(stdout,"%lx predicts==>%lx\n", bpRecords.front()->bPC, nextPC.instAddr());
            bpRecords.front()->isn = inst->seqNum;
            bkpBpr.push_front(bpRecords.front());
            bpRecords.pop_front();
        }else if (bpRecords.size()){
            //fprintf(stdout,"did not find %lx in the sequence. Next branch in queue:%lx\n", inst->pcState().instAddr(), bpRecords.front()->bPC);
        }
    }
    //printf("%lu Predict:%lx\n",inst->seqNum, nextPC.instAddr()); 
    /*
    if(inst->isCall()){
        printf("%lu:%lx FE call :%lx\n",inst->seqNum, inst->pcState().instAddr(), nextPC.instAddr()); 
    }else if(inst->isReturn()){
        printf("%lu:%lx FE ret :%lx\n",inst->seqNum, inst->pcState().instAddr(), nextPC.instAddr()); 
    }
    */

    if (predict_taken) {
        DPRINTF(CribFetch, "[tid:%i][sn:%i]: Predicted taken branch "
                "[npc:%s].\n", tid, inst->seqNum, nextPC);
    } else {
        DPRINTF(CribFetch, "[tid:%i][sn:%i]: Predicted not taken branch "
                "[npc:%s].\n", tid, inst->seqNum, nextPC);
    }

    inst->setPredTarg(nextPC);
    inst->setPredTaken(predict_taken);

    ++fetchedBranches;

    if (predict_taken) {
        ++predictedBranches;
    }

    return predict_taken;
}


inline void
CribFetch::pipelineIcacheAccess(ThreadID tid)
{
    if (!fetchState[tid].issuePipelinedIfetch) {
        return;
    }

    TheISA::PCState thisPC = fetchState[tid].pc;

    if (isRomMicroPC(thisPC.microPC())) {
        return;
    }

    Addr pcOffset = fetchState[tid].fetchOffset;
    Addr fetchAddr = (thisPC.instAddr() + pcOffset) & BaseCPU::PCMask;

    Addr block_pc = icacheBlockAlignPC(fetchAddr);

    // Unless buffer already has the block, fetch from icache
    if (!(fetchState[tid].cacheDataValid &&
          block_pc == fetchState[tid].cacheDataPC)) {
        DPRINTF(CribFetch, "[tid:%i]: Issuing a pipelined I-cache access, "
                "starting at [pc:%s].\n", tid, thisPC);
        fetchCacheLine(fetchAddr, tid, thisPC.instAddr());
    }
}


UopBuffer::UopBuffer(int _size){
    tags = (Addr*) malloc (_size * sizeof(Addr));
    offset = (int*) malloc (_size * sizeof(int));
    buffer = (StaticInstPtr **) malloc (_size *sizeof(StaticInstPtr*));
    size = _size;
    bmask = size - 1ull;
    bhead =0;

    for(int i=0; i<size; i++){
        tags[i] = 0ull;
        offset[i] = -1;
        buffer[i] = new StaticInstPtr();
    }
}

UopBuffer::~UopBuffer(){
    squash();
    delete tags;
    delete offset;
    for(int i=0; i<size; i++){
        delete buffer[i];
    }
    delete buffer;
}
    
void UopBuffer::init(int _size){
    tags = (Addr*) malloc (_size * sizeof(Addr));
    offset = (int*) malloc (_size * sizeof(int));
    buffer =  (StaticInstPtr **) malloc (_size *sizeof(StaticInstPtr*));
    instSize = new int[_size];
    itBits = new uint8_t[_size];
    bKeep = new int[_size];
    size = _size;
    bmask = size - 1ull;
    bhead =0;
    isSquashed=true;
    prnt = false;
    for(int i=0; i<size; i++){
        tags[i] = 0ull;
        offset[i] = -1;
        buffer[i] = new StaticInstPtr();
        instSize[i] = -1;
        itBits[i] = 0;
        bKeep[i] = -1;
    }
}
    
void UopBuffer::squash(){
    for(int i=0; i<size; i++){
        tags[i] = 0ull;
        offset[i] = -1;
        instSize[i] = -1;
        itBits[i] = 0;
        bKeep[i] = -1;
    }
    bhead=0;
    isSquashed=true;
    //if(prnt)
    //printf("Uop buffer Squashed\n");
}

int UopBuffer::lookup(Addr pc_in){
    int tag_idx = (int)(pc_in & bmask);
    if(tags[tag_idx] == pc_in){
        //if(prnt)
        //printf("Uop buffer lookup of address 0x%lx is successful and is present at offset:%d\n", pc_in, offset[tag_idx]);
        return offset[tag_idx];
    }
    else{
        return -1;
    }
}

void UopBuffer::add(StaticInstPtr inp,const PCState &pc){
    Addr pc_in = pc.instAddr();
    int tag_idx = (int)(pc_in & bmask);
    int clearTagIndex = bKeep[bhead];
    if(clearTagIndex >=0 && tags[clearTagIndex]!=pc_in){
        //if(prnt)
        //printf("Clearing PC 0x%lx from index : %d\n", tags[clearTagIndex], bhead);
        tags[clearTagIndex] = 0;
        offset[clearTagIndex] = -1;
    }
    if(tags[tag_idx] != pc_in){
        //if(prnt)
        //printf("Updating tag entries from 0x%lx to 0x%lx\n", tags[tag_idx], pc_in);
        tags[tag_idx] = pc_in;
        offset[tag_idx] = bhead;
    }
    bKeep[bhead] = tag_idx;
    instSize[bhead] = pc.instNPC() - pc_in;
    assert(instSize[bhead] > 0);
    itBits[bhead] = pc.nextItstate();
    *(buffer[bhead]) = inp;
    //if(prnt)
    //printf("Added instr at 0x%lx to buffer at index: %d\n", pc_in, bhead);
    bhead = (bhead + 1) % size;
    isSquashed = false;
}

StaticInstPtr UopBuffer::get(int index){
    return *buffer[index];
}

int UopBuffer::getSize(int index){
    return instSize[index];
}

uint8_t UopBuffer::getNextItstate(int index){
    return itBits[index];
}

void
UopCache::init(CribCPU *_cpu, unsigned num_sets, unsigned num_ways){
    cpu = _cpu;
    table = (UopCacheEntry **) malloc(num_sets*sizeof(UopCacheEntry*));
    for(unsigned i=0; i<num_sets; i++){
        table[i] = new UopCacheEntry[num_ways];
    }

    nWays = num_ways;
    nSets= num_sets;
    mask = nSets-1;
}

inline int
UopCache::getReplWay(Addr set){
    int repl_index = 0;
    uint64_t repl_acc_cy = table[set][0].accessCycle;
    for(int i=1; i<nWays; i++){
        if(table[set][i].accessCycle<repl_acc_cy){
            repl_index = i;
            repl_acc_cy = table[set][i].accessCycle;
        }
        if(!table[set][i].valid){
            repl_index = i;
            break;
        }
    }
    return repl_index;

}

inline Addr
UopCache::getIndex(Addr pc_in){
    return (pc_in>>2 & mask);
}

CribDynInstPtr
UopCache::lookup(TheISA::PCState pcs_in, bool update){
    Addr idx = getIndex(pcs_in.instAddr());

    for(unsigned a=0; a<nWays; a++){
        CribDynInstPtr minst = table[idx][a].match(pcs_in);
        if(minst){
            if(update) table[idx][a].accessed();
            CribDynInstPtr rinst(new CribDynInst(minst->staticInst, minst->macroop, minst->pc, minst->predTarget, minst->seqNum, minst->cpu));
            rinst->setThreadState(cpu->getThreadState(0));
            rinst->setTid(0);
            rinst->setASID(0);
            StaticInstPtr curMacroop = NULL;
            if(rinst->staticInst->isMacroop()){
                curMacroop = rinst->staticInst;
            }
            rinst->traceData = cpu->getTracer()->getInstRecord(curTick(), cpu->tcBase(0), rinst->staticInst, rinst->pcState(), curMacroop);
            /*
            if(rinst->pcState().instAddr() == 0x12c10){
                fprintf(stdout, "Sourcing 0x12c10 from uop cache\n");
                for (int8_t i = 0; i < rinst->staticInst->numSrcRegs(); i++) {
                    int reg = rinst->staticInst->srcRegIdx(i);
                    if(reg < TheISA::FP_Base_DepTag){
                        reg = cpu->isa[0]->flattenIntIndex(reg);
                        fprintf(stdout, "srcs %d: %d\n", i, reg);
                    }
                }
            }
            */
            return rinst;
        }
    }
    return NULL;
}

bool
UopCache::add(CribDynInstPtr inst_in){
    bool retval = false;
    TheISA::PCState mpcs = inst_in->pcState();
    unsigned set = getIndex(mpcs.instAddr());
    int way = getReplWay(set);

    //if found, just update access cycle
    for(unsigned a=0; a<nWays; a++){
        CribDynInstPtr minst = table[set][a].match(mpcs);
        if(minst){
            table[set][a].accessed();
            return retval;
        }else if(table[set][a].matchPC(mpcs)){
            way = a;
        }
    }
    CribDynInstPtr rinst(new CribDynInst(inst_in->staticInst, inst_in->macroop, inst_in->pc, inst_in->predTarget, inst_in->seqNum, inst_in->cpu));
    rinst->setThreadState(cpu->getThreadState(0));
    rinst->setTid(0);
    rinst->setASID(0);
    StaticInstPtr curMacroop = NULL;
    if(rinst->staticInst->isMacroop()){
        curMacroop = rinst->staticInst;
    }
    rinst->traceData = cpu->getTracer()->getInstRecord(curTick(), cpu->tcBase(0), rinst->staticInst, rinst->pcState(), curMacroop);
    /*
    if(rinst->pcState().instAddr() == 0x12c10){
        fprintf(stdout, "Adding 0x12c10 to uop cache\n");
        for (int8_t i = 0; i < rinst->staticInst->numSrcRegs(); i++) {
            int reg = rinst->staticInst->srcRegIdx(i);
            if(reg < TheISA::FP_Base_DepTag){
                reg = cpu->isa[0]->flattenIntIndex(reg);
                fprintf(stdout, "srcs %d: %d\n", i, reg);
            }
        }
    }
    */
    //This instr is not in the table; add it
    table[set][way].add(rinst);
    return retval;
}




