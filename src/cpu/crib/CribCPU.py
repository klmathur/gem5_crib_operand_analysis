from m5.params import *
from m5.proxy import *
from BaseCPU import BaseCPU
from BranchPredictor import BranchPredictor


#class Crib_BP(BranchPredictor):
#    predType = "ltage"
#    predType = "tournament"
#    localCtrBits = 2
#    localHistoryTableSize = 64
#    localHistoryBits = 6
#    globalPredictorSize = 8192
#    globalCtrBits = 2
#    globalHistoryBits = 13
#    choicePredictorSize = 8192
#    choiceCtrBits = 2
#    BTBEntries = 2048
#    BTBTagSize = 18
#    RASSize = 16
#    instShiftAmt = 2

class CribCPU(BaseCPU):
    type = 'CribCPU'
    cxx_header = 'cpu/crib/cpu.hh'

    @classmethod
    def memory_mode(cls):
        return 'timing'

    @classmethod
    def require_caches(cls):
        return True

    @classmethod
    def support_take_over(cls):
        return True

    activity = Param.Unsigned(0, "Initial count")
    backComSize = Param.Unsigned(10, "Timebuffer backwards communication size")
    fwdComSize = Param.Unsigned(10, "Timebuffer forwards communication size")
    # Fetch parameters
    fetchWidth = Param.Unsigned(4, "Fetch width")
    decodeToFetchDelay = Param.Unsigned(1, "Decode to fetch delay")
    dispatchToFetchDelay = Param.Unsigned(1, "Dispatch to fetch delay")
    cribToFetchDelay = Param.Unsigned(1, "Crib to fetch delay")
    commitToFetchDelay = Param.Unsigned(1, "Commit to fetch delay")
    smtFetchPolicy = Param.String("SingleThread", "SMT fetch policy")
    numUopBufferEntries = Param.Unsigned(0, "Uop Buffer entries")
    ucSets= Param.Unsigned(0, "Uop cache sets")
    ucWays= Param.Unsigned(8, "Uop cache ways")
    cribCommitWidth= Param.Unsigned(1,"Number of partitions to commit per cycle");

    dumpBpred = Param.Unsigned(0, "dump branch prediction outcomes"); 
    idealPred = Param.Unsigned(0, "dump ideal predictions"); 
    bpredFileName = Param.String("bpred.txt", "File name to dump branch prediction outcomes"); 

    # Predictor parameters
    #branchPred = Crib_BP()
    branchPred = BranchPredictor(numThreads = Parent.numThreads);

    # Decode parameters
    decodeWidth = Param.Unsigned(4, "Decode width")
    fetchToDecodeDelay = Param.Unsigned(2, "Fetch to decode delay")
    dispatchToDecodeDelay = Param.Unsigned(1, "Dispatch to decode delay")
    cribToDecodeDelay = Param.Unsigned(1, "Crib to decode delay")
    commitToDecodeDelay = Param.Unsigned(1, "Commit to decode delay")

    # Dispatch parameters
    cribToDispatchDelay = Param.Unsigned(1, "Crib to dispatch delay")
    commitToDispatchDelay = Param.Unsigned(1, "Commit to dispatch delay")
    decodeToDispatchDelay = Param.Unsigned(1, "Decode to dispatch delay")

    # CRIB parameters
    cribPartitions = Param.Unsigned(8, "Number of CRIB partitions")
    cribPartitionSize = Param.Unsigned(4, "Number of entries in partition")
    cribLoadsPerPartition = Param.Unsigned(4, "Maximum loads per partition")
    cribStoresPerPartition = Param.Unsigned(4, "Maximum stores per partition")
    cribPropagateDistance = Param.Unsigned(8, "Propagation distance of signals")
    dispatchToCribDelay = Param.Unsigned(1, "Dispatch to Crib delay")

    cribLoopEnable = Param.Bool(False, "Enable loop execution")
    cribLoopUnroll = Param.Bool(False, "Unroll loops if possible")
    cribLoopTrain = Param.Bool(False, "Disable loop mode for misbehaving loops")
    cribStridePrefetchEnable = Param.Bool(False, "Enable loop stride prefetching")
    cribPointerPrefetchEnable = Param.Bool(False, "Enable loop pointer prefetching")
    cribLoopDependencies = Param.Unsigned(4, "Maximum of loop carried dependencies")
    cribLogLoopHistSize = Param.Unsigned(8, "Log2(HistEntries) for saved loops")
    # Execute parameters
    # TODO: Adjust these parameters
    NoOpClassLatency     = Param.Unsigned(1,  "Number of cycles to execute")
    IntAluLatency        = Param.Unsigned(1,  "Number of cycles to execute")
    IntMultLatency       = Param.Unsigned(3,  "Number of cycles to execute")
    IntDivLatency        = Param.Unsigned(20, "Number of cycles to execute")
    FloatAddLatency      = Param.Unsigned(5,  "Number of cycles to execute")
    FloatCmpLatency      = Param.Unsigned(5,  "Number of cycles to execute")
    FloatCvtLatency      = Param.Unsigned(3,  "Number of cycles to execute")
    FloatMultLatency     = Param.Unsigned(5,  "Number of cycles to execute")
    FloatDivLatency      = Param.Unsigned(10,  "Number of cycles to execute")
    FloatSqrtLatency     = Param.Unsigned(33, "Number of cycles to execute")
    SimdAddLatency       = Param.Unsigned(4,  "Number of cycles to execute")
    SimdAddAccLatency    = Param.Unsigned(4,  "Number of cycles to execute")
    SimdAluLatency       = Param.Unsigned(4,  "Number of cycles to execute")
    SimdCmpLatency       = Param.Unsigned(4,  "Number of cycles to execute")
    SimdCvtLatency       = Param.Unsigned(3,  "Number of cycles to execute")
    SimdMiscLatency      = Param.Unsigned(3,  "Number of cycles to execute")
    SimdMultLatency      = Param.Unsigned(5,  "Number of cycles to execute")
    SimdMultAccLatency   = Param.Unsigned(5,  "Number of cycles to execute")
    SimdShiftLatency     = Param.Unsigned(3,  "Number of cycles to execute")
    SimdShiftAccLatency  = Param.Unsigned(3,  "Number of cycles to execute")
    SimdSqrtLatency      = Param.Unsigned(9,  "Number of cycles to execute")
    SimdFloatAddLatency  = Param.Unsigned(5,  "Number of cycles to execute")
    SimdFloatAluLatency  = Param.Unsigned(5,  "Number of cycles to execute")
    SimdFloatCmpLatency  = Param.Unsigned(3,  "Number of cycles to execute")
    SimdFloatCvtLatency  = Param.Unsigned(3,  "Number of cycles to execute")
    SimdFloatDivLatency  = Param.Unsigned(10,  "Number of cycles to execute")
    SimdFloatMiscLatency = Param.Unsigned(3,  "Number of cycles to execute")
    SimdFloatMultLatency = Param.Unsigned(5,  "Number of cycles to execute")
    SimdFloatMultAccLatency = Param.Unsigned(6,  "Number of cycles to execute")
    SimdFloatSqrtLatency = Param.Unsigned(10,  "Number of cycles to execute")
    MemReadLatency       = Param.Unsigned(1,  "Number of cycles to execute")
    MemWriteLatency      = Param.Unsigned(1,  "Number of cycles to execute")
    IprAccessLatency     = Param.Unsigned(3,  "Number of cycles to execute")
    InstPrefetchLatency  = Param.Unsigned(1,  "Number of cycles to execute")


    cribIntALU = Param.Unsigned(2, "Number of parallel integer executions")
    cribMulDiv = Param.Unsigned(1, "Number of parallel interer mul/div")
    cribSIMD = Param.Unsigned(2, "Number of simd units")
    cribFPALU = Param.Unsigned(2, "Number of fp alu")
    cribFPDiv = Param.Unsigned(1, "Number of fp div")

    # Commit parameters
    commitWidth = Param.Unsigned(4, "Commit width")

    # LSQ parameters
    cacheBanks = Param.Unsigned(2, "L1 cache banks")

    LQEntries = Param.Unsigned(8, "Number of load queue entries (per bank)")
    SQEntries = Param.Unsigned(4, "Number of store queue entries (per bank)")

    LQAllocBandwidth = Param.Unsigned(2, "Number of loads allocated per cycle")
    SQAllocBandwidth = Param.Unsigned(2, "Number of loads allocated per cycle")

