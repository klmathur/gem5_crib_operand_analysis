#ifndef __CPU_CRIB_REGSTATE_HH__
#define __CPU_CRIB_REGSTATE_HH__

#include "arch/isa.hh"
#include "arch/registers.hh"
#include "arch/utility.hh"
#include "config/the_isa.hh"

typedef std::bitset<TheISA::NumIntRegs+TheISA::NumFloatRegs> RegSet;

union PhysReg {
    TheISA::IntReg i;
    TheISA::FloatReg f;
    TheISA::FloatRegBits fb;
    int readTick;
    int writeTick;
};

enum RegType {
    ResultInt,
    ResultFloat,
    ResultFloatBits
};

struct RegResult
{
    RegIndex index;
    RegType type;
    PhysReg reg;
};

class RegState
{
    public:
        RegState() { reset(); 
                     
                    }

        void reset();
       
        std::string print() const;
        std::string printLine() const;
        TheISA::IntReg readIntReg(int idx) const;
        TheISA::FloatReg readFloatReg(int idx) const;
        TheISA::FloatRegBits readFloatRegBits(int idx) const;

        void setIntReg(int idx, TheISA::IntReg val);
        void setFloatReg(int idx, TheISA::FloatReg val);
        void setFloatRegBits(int idx, TheISA::FloatRegBits val);
        void set(const RegResult &val);

        /* klm: Setters and getters for read and write tick */
        void incReadTick(int idx);
        int  getReadTick(int idx);
    private:
    PhysReg regs[TheISA::NumIntRegs+TheISA::NumFloatRegs];

};

inline void
RegState::reset()
{
    for(int idx=0; idx<TheISA::NumIntRegs+TheISA::NumFloatRegs; idx++) {
       regs[idx].i = 0;
       regs[idx].readTick = 0;
    }
}

inline TheISA::IntReg
RegState::readIntReg(int idx) const
{
    return regs[idx].i;
}

inline TheISA::FloatReg
RegState::readFloatReg(int idx) const
{
    return regs[idx].f;
}

inline TheISA::FloatRegBits
RegState::readFloatRegBits(int idx) const
{
    return regs[idx].fb;
}

inline void
RegState::setIntReg(int idx, TheISA::IntReg val)
{
    regs[idx].i = val;
}

inline void
RegState::setFloatReg(int idx, TheISA::FloatReg val)
{
    regs[idx].f = val;
}

inline void
RegState::setFloatRegBits(int idx, TheISA::FloatRegBits val)
{
    regs[idx].fb = val;
}

inline void
RegState::set(const RegResult &reg)
{
    switch(reg.type)
    {
        case ResultInt: setIntReg(reg.index, reg.reg.i); break;
        case ResultFloat: setFloatReg(reg.index, reg.reg.f); break;
        case ResultFloatBits: setFloatRegBits(reg.index, reg.reg.fb); break;
    }
}


inline std::string
RegState::print() const
{
    std::stringstream ss;
    for (unsigned i = 0; i < TheISA::NumIntRegs; i++) {
        ss << "IntReg[" << std::dec << i << "]: 0x" << std::hex << regs[i].i << std::endl;
    }

    return ss.str();
}


inline std::string
RegState::printLine() const
{
    std::stringstream ss;
    for (unsigned i = 0; i < TheISA::NumIntRegs; i++) {
        char str[16];
        sprintf(str, "%#08lx ", regs[i].i);
        ss << str;
    }

    return ss.str();
}

inline void
RegState::incReadTick(int idx)
{
    regs[idx].readTick ++; 
}

inline int
RegState::getReadTick(int idx)
{
    return regs[idx].readTick;
}
#endif //__CPU_CRIB_REGSTATE_HH__
