#include "cpu/crib/cpu.hh"
#include "cpu/crib/lsq.hh"
#include "debug/CribLSQ.hh"
#include "debug/CribMem.hh"
#include "params/CribCPU.hh"

CribLSQ::CribLSQ(CribCPU *_cpu, Crib *_crib, CribCPUParams *params)
    : cpu(_cpu), crib(_crib), numBanks(params->cacheBanks)
{
    assert(isPowerOf2(numBanks));
    banks = new CribLSQBank[numBanks];
    for (unsigned i = 0; i < numBanks; i++) {
        CribLSQBank *prev = &banks[(i + numBanks - 1) % numBanks];
        CribLSQBank *next = &banks[(i + 1) % numBanks];
        banks[i].init(cpu, this, params, i, prev, next);
    }
}

std::string
CribLSQ::name() const
{
    return cpu->name() + ".lsq";
}

void
CribLSQ::regStats(){
    sqFull
        .name(name() + ".sqFulls")
        .desc("Num of SQ fulls encountered")
        .prereq(sqFull); 

    lqFull
        .name(name() + ".lqFulls")
        .desc("Num of LQ fulls encountered")
        .prereq(lqFull); 
}

void
CribLSQ::setDCache()
{
    assert(cpu->getDataPort().isConnected());
    cacheBlkSize = cpu->getDataPort().peerBlockSize();
    log2BlockSize = floorLog2(cacheBlkSize);
    bankMask = (numBanks - 1) << log2BlockSize;
    cacheBlkMask = cacheBlkSize - 1;
}

void
CribLSQ::initiateRead(CribDynInstPtr inst, Addr addr, unsigned size,
    unsigned flags)
{
    DPRINTF(CribMem, "[sn:%i]: Initiating read [%#x-%#x].\n",
            inst->seqNum, addr, addr + size - 1);

    unsigned lo_bank = addrToBankNum(addr);
    unsigned hi_bank = addrToBankNum(addr + size - 1);

   if (lo_bank == hi_bank) {
        banks[lo_bank].registerLoadRequest(inst, addr, size, flags);
   } else {
        inst->cribEntry->isSplit = true;

        unsigned lo_size = cacheBlkMask - (addr & cacheBlkMask) + 1;
        unsigned hi_size = size - lo_size;
        Addr hi_addr = addr + lo_size;
        banks[lo_bank].registerLoadRequest(inst, addr, lo_size, flags,
                                           true, false);
        banks[hi_bank].registerLoadRequest(inst, hi_addr, hi_size, flags,
                                           true, true);
   }
}

void
CribLSQ::initiateWrite(CribDynInstPtr inst, Addr addr, unsigned size,
    unsigned flags, uint8_t *data)
{
    DPRINTF(CribMem, "[sn:%i]: Initiating Write [%#x-%#x].\n",
            inst->seqNum, addr, addr + size - 1);

    unsigned lo_bank = addrToBankNum(addr);
    unsigned hi_bank = addrToBankNum(addr + size - 1);

    if (lo_bank == hi_bank) {
        banks[lo_bank].registerStoreRequest(inst, addr, size, flags, data);
    } else {
        inst->cribEntry->isSplit = true;

        unsigned lo_size = cacheBlkMask - (addr & cacheBlkMask) + 1;
        unsigned hi_size = size - lo_size;
        Addr hi_addr = addr + lo_size;

        banks[lo_bank].registerStoreRequest(inst, addr, lo_size, flags, data,
                                            true, false);
        banks[hi_bank].registerStoreRequest(inst, hi_addr, hi_size, flags,
                                            data + lo_size, true, true);
    }
}

void
CribLSQ::initiatePrefetch(CribDynInstPtr inst, Addr addr, unsigned size,
    unsigned flags)
{
    DPRINTF(CribLSQ, "[sn:%i]: Initiating Prefetch [%#x-%#x].\n",
            inst->seqNum, addr, addr + size - 1);

    unsigned lo_bank = addrToBankNum(addr);
    unsigned hi_bank = addrToBankNum(addr + size - 1);

    if (lo_bank == hi_bank) {
        banks[lo_bank].registerPrefetchRequest(inst, addr, size, flags);
    } else {
        DPRINTF(CribLSQ, "[sn:%i]: Ignoring split prefetch [%#x-%#x].\n",
                inst->seqNum, addr, addr + size - 1);
        inst->prefetchOutstanding = false;
    }
}

bool
CribLSQ::validatePrefetch(const CribDynInstPtr &inst, Addr addr,
    unsigned size, unsigned flags)
{
    DPRINTF(CribLSQ, "[sn:%i]: Validating Prefetch [%#x-%#x].\n",
            inst->seqNum, addr, addr + size - 1);

    unsigned lo_bank = addrToBankNum(addr);
    unsigned hi_bank = addrToBankNum(addr + size - 1);

    if (lo_bank == hi_bank) {
        if (!banks[lo_bank].validatePrefetch(inst, addr, size, flags)) {
            DPRINTF(CribLSQ, "[sn:%i]: Validation failed.\n", inst->seqNum);
            deassertPrefetchRequest(inst);
            return false;
        }
    } else {
        DPRINTF(CribLSQ, "[sn:%i]: Validation failed.  Split load.\n");
        deassertPrefetchRequest(inst);
        return false;
    }

    return true;
}

void
CribLSQ::deassertRequest(const CribDynInstPtr &inst, bool committed)
{
    DPRINTF(CribLSQ, "[sn:%i]: Deasserting request.\n", inst->seqNum);
    unsigned lo_bank = addrToBankNum(inst->memAddr);
    unsigned hi_bank = addrToBankNum(inst->memAddr + inst->memSize - 1);

    banks[lo_bank].deassertRequest(inst, committed);
    if (lo_bank != hi_bank) {
        banks[hi_bank].deassertRequest(inst, committed);
    }
}

void
CribLSQ::SquashYoungerMemoryOps(InstSeqNum seq_num){
    for (unsigned i = 0; i < numBanks; i++) {
        banks[i].SquashYoungerMemoryOps(seq_num);
    }
}

void
CribLSQ::deassertPrefetchRequest(const CribDynInstPtr &inst)
{
    DPRINTF(CribLSQ, "[sn:%i]: Deasserting prefetch request.\n", inst->seqNum);
    for (unsigned b = 0; b < numBanks; b++) {
        banks[b].deassertRequest(inst, false);
    }
}

void
CribLSQ::deassertAllPrefetches(const CribDynInstPtr &inst)
{
    assert(inst->isLoad());

    DPRINTF(CribLSQ, "[sn:%i]: Deasserting all prefetches.\n", inst->seqNum);
    //CribSeqNum seq_lo = inst->getCribSeqNum();
    //CribSeqNum seq_hi = seq_lo;
    //seq_lo.loopItrCount = 0;
    //seq_hi.loopItrCount = std::numeric_limits<uint64_t>::max();
    for (unsigned b = 0; b < numBanks; b++) {
        banks[b].deassertAllPrefetches(inst->seqNum);
    }
    inst->prefetchOutstanding = false;
}

void
CribLSQ::makeSenior(const CribDynInstPtr &inst)
{
    DPRINTF(CribLSQ, "[sn:%i]: Store transitioning to senior.\n",
            inst->seqNum);
    unsigned lo_bank = addrToBankNum(inst->memAddr);
    unsigned hi_bank = addrToBankNum(inst->memAddr + inst->memSize - 1);

    CribSeqNum seq_num = inst->getCribSeqNum();
    banks[lo_bank].makeSenior(seq_num);
    if (lo_bank != hi_bank) {
        banks[hi_bank].makeSenior(seq_num);
    }
}

void
CribLSQ::tick()
{
    for (unsigned i = 0; i < numBanks; i++) {
        banks[i].tick();
    }
}

bool
CribLSQ::isDrained() const
{
    for (unsigned i = 0; i < numBanks; i++) {
        if (!banks[i].isDrained()) {
            DPRINTF(CribLSQ, "Bank %i not drained.\n", i);
            return false;
        }
    }

    return true;
}

void
CribLSQ::startupStage()
{
    if (cpu->getDataPort().isConnected()) {
        setDCache();
    }
}

void
CribLSQ::takeOverFrom()
{
    if (cpu->getDataPort().isConnected()) {
        setDCache();
    }
}

bool
CribLSQ::recvTimingResp(PacketPtr pkt)
{
    if (pkt->isError()) {
        DPRINTF(CribLSQ, "Got an error packet back for address %#x.\n",
                pkt->getAddr());
        panic("Error packet received.\n");
    }

    banks[addrToBankNum(pkt->req->getVaddr())].completeDataAccess(pkt);
    return true;
}

inline void
CribLSQBank::init(CribCPU *_cpu, CribLSQ *_lsq, CribCPUParams *params,
    unsigned bank_num, CribLSQBank *prev, CribLSQBank *next)
{
    cpu = _cpu;
    lsq = _lsq;
    bankNum = bank_num;
    prevBank = prev;
    nextBank = next;
    numLQEntries = params->LQEntries;
    numSQEntries = params->SQEntries;
    loadAllocBandwidth = params->LQAllocBandwidth;
    storeAllocBandwidth = params->SQAllocBandwidth;
}

std::string
CribLSQBank::name() const
{
    std::stringstream ss;
    ss << lsq->name() << ".bank[" << bankNum << "]";
    return ss.str();
}

inline void
CribLSQBank::registerLoadRequest(const CribDynInstPtr &inst, Addr addr,
    unsigned size, unsigned flags, bool split, bool split_hi)
{
    DPRINTF(CribLSQ, "[sn:%i]: Registering load request [%#x-%#x].\n",
            inst->seqNum, addr, addr + size - 1);

    loadRequests.insert(std::make_pair(inst->getCribSeqNum(),
                        LoadRequest(inst, addr, size, flags,
                                    split, split_hi, false)));
}

inline void
CribLSQBank::registerStoreRequest(const CribDynInstPtr &inst, Addr addr,
    unsigned size, unsigned flags, uint8_t *data, bool split, bool split_hi)
{
    DPRINTF(CribLSQ, "[sn:%i]: Registering store request [%#x-%#x].\n",
            inst->seqNum, addr, addr + size - 1);

    storeRequests.insert(std::make_pair(inst->getCribSeqNum(),
                         StoreRequest(inst, addr, size, flags, data,
                                      split, split_hi)));
}

inline void
CribLSQBank::registerPrefetchRequest(const CribDynInstPtr &inst, Addr addr,
    unsigned size, unsigned flags)
{
    DPRINTF(CribLSQ, "[sn:%i]: Registering prefetch request [%#x-%#x].\n",
            inst->seqNum, addr, addr + size - 1);

    CribSeqNum seq_num = inst->getCribSeqNum();
    seq_num.loopItrCount++;
    loadRequests.insert(std::make_pair(seq_num,
                        LoadRequest(inst, addr, size, flags,
                                    false, //split
                                    false, //split_hi,
                                    true)));  // prefetch
}

inline bool
CribLSQBank::validatePrefetch(const CribDynInstPtr &inst, Addr addr,
    unsigned size, unsigned flags)
{
    const CribSeqNum seq_num = inst->getCribSeqNum();
    auto load_itr = loadQueue.find(seq_num);
    if (load_itr != loadQueue.end()) {
        LQEntry &lqe = *(load_itr->second);

        // Validate load
        if (lqe.addr == addr &&
            lqe.size == size &&
            lqe.flags == flags) {
            DPRINTF(CribLSQ, "[sn:%i]: Prefetch successfuly validated [%#x-%#x].\n",
                    inst->seqNum, addr, addr + size - 1);

            if (lqe.status == LQEntry::Status::Executed) {
                DPRINTF(CribLSQ, "[sn:%i]: Prefetched load fully executed.\n",
                        inst->seqNum);
                Request mem_req =
                    Request(0, // asid
                            lqe.addr, // addr (aligned)
                            lqe.size, // request size (full line)
                            lqe.flags, // Flags
                            cpu->dataMasterId(),  // ID for bus access
                            inst->pcState().instAddr(), // PC
                            cpu->getThreadState(0)->contextId(), // cid
                            0); // tid

                Packet data_pkt = Packet(&mem_req,
                        mem_req.isLLSC() ? MemCmd::LoadLockedReq : MemCmd::ReadReq);
                data_pkt.dataStatic(lqe.data);
                inst->completeAcc(&data_pkt);
                inst->prefetchOutstanding = false;
            } else {
                DPRINTF(CribLSQ, "[sn:%i]: Prefetch load unexecuted. "
                        "Taking ownership.\n", inst->seqNum);
            }
            lqe.prefetch = false;
            return true;
        } else {
            return false;
        }
    }

    auto req_itr = loadRequests.find(seq_num);
    if (req_itr != loadRequests.end()) {
        LoadRequest &ld_req = req_itr->second;

        if (ld_req.addr == addr &&
            ld_req.size == size &&
            ld_req.flags == flags) {
            DPRINTF(CribLSQ, "[sn:%i]: Prefetch successfuly validated [%#x-%#x].\n",
                    inst->seqNum, addr, addr + size - 1);

            inst->prefetchOutstanding = false;
            ld_req.prefetch = false;
            return true;
        } else {
            return false;
        }
    }

    return false;
}

inline void
CribLSQBank::tick()
{
    auto store_itr = storeQueue.begin();
    while(store_itr != storeQueue.end()){
        if(store_itr->second->status == SQEntry::Status::SeniorComplete){
            delete store_itr->second;
            storeQueue.erase(store_itr);
        }else{
            break;
        }
        store_itr++;
    }

    // Parse new requests
    parseRequests();

    // Issue
    issueStores();
    issueLoads();

    // Writeback
    writebackStores();
}

inline bool
CribLSQBank::isDrained() const
{
    if (!(loadQueue.empty() && storeQueue.empty() &&
           loadRequests.empty() && storeRequests.empty())) {
        DPRINTF(CribLSQ, "Not drained. %i %i %i %i\n",
                    loadQueue.empty(), storeQueue.empty(),
                    loadRequests.empty(), storeRequests.empty());
        DPRINTF(CribLSQ, "lqsize = %i\n", loadQueue.size());
        for (auto lq_itr = loadQueue.begin(); lq_itr != loadQueue.end(); lq_itr++) {
            DPRINTF(CribLSQ, "lq_sn:%i\n", lq_itr->first.seqNum);
        }
        return false;
    }
    return true;
}

inline void
CribLSQBank::parseRequests()
{
    // Handle load allocation
    unsigned loads = 0;
    while (!loadRequests.empty() && loads < loadAllocBandwidth) {
        loads++;
        auto load_itr = loadRequests.begin();
        if (loadQueue.size() < numLQEntries) {
            DPRINTF(CribLSQ, "[sn:%i %i %i]: Allocating load.\n",
                    load_itr->first.loopColor,
                    load_itr->first.loopItrCount,
                    load_itr->first.seqNum);
            allocateLoad(load_itr->first, load_itr->second);
            loadRequests.erase(load_itr);
        } else {
            DPRINTF(CribLSQ, "[sn:%i]: Unable to allocate load. "
                    "Attempting to reclaim.\n", load_itr->first.seqNum);
            auto rep_itr = loadQueue.rbegin();
            // If rep_itr has a younger sequence number, replace it
            if (rep_itr->first > load_itr->first) {
                // Deallocate current load instruction
                DPRINTF(CribLSQ, "[sn:%i]: Reclaiming LQ entry. "
                        "Evicted load [sn:%i].\n",
                        load_itr->first.seqNum, rep_itr->first.seqNum);

                // Allocate new request
                const LQEntry &rep_lqe = *(rep_itr->second);
                loadRequests.insert(std::make_pair(rep_itr->first,
                                    LoadRequest(rep_lqe.inst,
                                                rep_lqe.addr,
                                                rep_lqe.size,
                                                rep_lqe.flags,
                                                rep_lqe.split,
                                                rep_lqe.split_hi,
                                                rep_lqe.prefetch)));
                if (!rep_lqe.prefetch) {
                    if (rep_lqe.split) {
                        if (rep_lqe.split_hi) {
                            rep_lqe.inst->cribEntry->splitHiDone = false;
                        } else {
                            rep_lqe.inst->cribEntry->splitLoDone = false;
                        }
                    }
                }

                if (!rep_lqe.prefetch) {
                    rep_lqe.inst->cribEntry->status = CribEntry::Status::Executing;
                }
                // Remove LQ entry
                delete rep_itr->second;
                unissuedLoads.erase(rep_itr->first);
                loadQueue.erase((++rep_itr).base());

                // Allocate the load request
                allocateLoad(load_itr->first, load_itr->second);
                loadRequests.erase(load_itr);
            }else{
                lsq->lqFull++; 
            }
        }
    }

    // Handle store allocation
    unsigned stores = 0;
    while (!storeRequests.empty() && stores < storeAllocBandwidth) {
        stores++;
        auto store_itr = storeRequests.begin();
        if (storeQueue.size() < numSQEntries) {
            DPRINTF(CribLSQ, "[sn:%i]: Allocating store.\n",
                    store_itr->first.seqNum);
            // Tag overlapping loads for re-execution
            probeLoadQueue(store_itr->second.addr,
                           store_itr->second.size,
                           store_itr->first);
            allocateStore(store_itr->first, store_itr->second);
            storeRequests.erase(store_itr);
        } else {
            DPRINTF(CribLSQ, "[sn:%i]: Unable to allocate store. "
                    "Attempting to reclaim.\n", store_itr->first.seqNum);
            auto rep_itr = storeQueue.rbegin();
            // If rep_itr has a younger sequence number, replace it
            if (rep_itr->first > store_itr->first) {
                DPRINTF(CribLSQ, "[sn:%i]: Reclaiming SQ entry. "
                        "Evicted store [sn:%i].\n",
                        store_itr->first.seqNum, rep_itr->first.seqNum);
                // Allocate new request
                const SQEntry &rep_sqe = *(rep_itr->second);
                storeRequests.insert(std::make_pair(rep_itr->first,
                                        StoreRequest(rep_sqe.inst,
                                                     rep_sqe.addr,
                                                     rep_sqe.size,
                                                     rep_sqe.flags,
                                                     rep_sqe.data,
                                                     rep_sqe.split,
                                                     rep_sqe.split_hi)));
                if (rep_sqe.split) {
                    if (rep_sqe.split_hi) {
                        rep_sqe.inst->cribEntry->splitHiDone = false;
                    } else {
                        rep_sqe.inst->cribEntry->splitLoDone = false;
                    }
                }
                rep_sqe.inst->cribEntry->status = CribEntry::Status::Executing;
                // Remove SQ entry
                deassertStoreForwardedLoads(rep_itr->first, rep_sqe);
                if (rep_sqe.status == SQEntry::Status::Executed) {
                    delete rep_sqe.memReq;
                    delete rep_sqe.pkt;
                }
                delete rep_itr->second;
                unissuedStores.erase(rep_itr->first);
                storeQueue.erase((++rep_itr).base());

                // Allocate the store request
                probeLoadQueue(store_itr->second.addr,
                               store_itr->second.size,
                               store_itr->first);
                allocateStore(store_itr->first, store_itr->second);
                storeRequests.erase(store_itr);
            } else {
                lsq->sqFull++; 
                DPRINTF(CribLSQ,"StoreQ is full\n"); 
                auto wb = storeQueue.begin();
                while (wb != storeQueue.end()){
                    DPRINTF(CribLSQ,"SeqNum:%i --> %i\n", 
                            wb->first.seqNum,
                            wb->second->status);
                    wb++;
                }

                DPRINTF(CribLSQ, "Could not allocate [sn:%i] because of "
                                 "older store [sn:%i].\n",
                                 store_itr->first.seqNum,
                                 rep_itr->first.seqNum);
            }
        }
    }
}

inline void
CribLSQBank::allocateLoad(const CribSeqNum &sn, const LoadRequest &req)
{
    LQEntry *lqe = new LQEntry(req.inst, req.addr, req.size, req.flags,
                               req.split, req.split_hi, req.prefetch);
    loadQueue.insert(std::make_pair(sn, lqe));
    unissuedLoads.insert(std::make_pair(sn, lqe));

    DPRINTF(CribLSQ, "[sn:%i]: Load queue entry [%i/%i] allocated.\n",
            sn.seqNum, loadQueue.size(), numLQEntries);
}

inline void
CribLSQBank::allocateStore(const CribSeqNum &sn, const StoreRequest &req)
{
    SQEntry *sqe = new SQEntry(req.inst, req.addr, req.size, req.flags,
                               req.data, req.split, req.split_hi);
    storeQueue.insert(std::make_pair(sn, sqe));
    unissuedStores.insert(std::make_pair(sn, sqe));

    DPRINTF(CribLSQ, "[sn:%i]: Store queue entry [%i/%i] allocated.\n",
            sn.seqNum, storeQueue.size(), numSQEntries);
}

inline void
CribLSQBank::issueStores()
{
    unsigned stores = 0;
    while (!unissuedStores.empty() && stores < storeAllocBandwidth) {
        auto store_itr = unissuedStores.begin();
        SQEntry &sqe = *(store_itr->second);

        DPRINTF(CribLSQ, "[sn:%i]: Issuing store [%#x-%#x].\n",
                sqe.inst->seqNum, sqe.addr, sqe.addr + sqe.size - 1);

        RequestPtr mem_req =
            new Request(0, // asid
                        sqe.addr, // addr (aligned)
                        sqe.size, // request size (full line)
                        sqe.flags, // Flags (will need to modify this later)
                        cpu->dataMasterId(),  // ID for bus access
                        sqe.inst->pcState().instAddr(), // PC
                        cpu->getThreadState(0)->contextId(), // cid
                        0); // tid
        sqe.memReq = mem_req;
        StoreTranslation *trans = new StoreTranslation(this, store_itr->first);

        sqe.status = SQEntry::Status::IssuedTranslation;

        cpu->getDTBPtr()->translateTiming(mem_req,
                cpu->getThreadState(0)->getTC(),
                trans,
                BaseTLB::Write);

        unissuedStores.erase(store_itr);
        ++stores;
    }
}

void
CribLSQBank::finishStoreTranslation(const Fault &fault, RequestPtr req,
    const CribSeqNum &seq_num)
{
    auto store_itr = storeQueue.find(seq_num);
    if (store_itr == storeQueue.end() || req != store_itr->second->memReq) {
        DPRINTF(CribLSQ, "[sn:%i]: Ignoring dtlb completed after squash.\n",
                seq_num.seqNum);
        delete req;
        return;
    }

    SQEntry &sqe = *(store_itr->second);
    if (fault == NoFault) {
        sqe.physAddr = req->getPaddr();
        sqe.status = SQEntry::Status::Executed;
        DPRINTF(CribLSQ, "[sn:%i]: Translation succeded.  Store executed. "
                "[vaddr:%#x-%#x][paddr:%#x-%#x].\n",
                seq_num.seqNum,
                sqe.addr, sqe.addr + sqe.size -1,
                sqe.physAddr, sqe.physAddr + sqe.size - 1);

        PacketPtr data_pkt = new Packet(req,
                (req->isLLSC() ? MemCmd::StoreCondReq : MemCmd::WriteReq));
        data_pkt->dataStatic(sqe.data);
        sqe.pkt = data_pkt;

        if (sqe.split) {
            if (sqe.split_hi) {
                sqe.inst->cribEntry->splitHiDone = true;
            } else {
                sqe.inst->cribEntry->splitLoDone = true;
            }

            if (sqe.inst->cribEntry->splitLoDone &&
                sqe.inst->cribEntry->splitHiDone) {
                if (!sqe.inst->isStoreConditional()) {
                    handleSplitStore(store_itr->first, sqe);
                } else {
                    panic("Split store conditionals currently unsupported");
                }
            }
        } else {
            if (!sqe.inst->isStoreConditional()) sqe.inst->completeAcc(data_pkt);
            else sqe.inst->cribEntry->status = CribEntry::Status::Executed;
        }
    } else {
        DPRINTF(CribLSQ, "[sn:%i]: Translation faulted.\n", seq_num.seqNum);
        sqe.inst->cribEntry->markExecuted();
        sqe.inst->fault = fault;
        delete req;
    }
}

inline void
CribLSQBank::issueLoads()
{
    unsigned loads = 0;
    while (!unissuedLoads.empty() && loads < loadAllocBandwidth) {
        auto load_itr = unissuedLoads.begin();
        LQEntry &lqe = *(load_itr->second);

        DPRINTF(CribLSQ, "[sn:%i]: Issuing load [%#x-%#x].\n",
                lqe.inst->seqNum, lqe.addr, lqe.addr + lqe.size - 1);

        RequestPtr mem_req =
            new Request(0, // asid
                        lqe.addr, // addr (aligned)
                        lqe.size, // request size (full line)
                        lqe.flags, // Flags (will need to modify this later)
                        cpu->dataMasterId(),  // ID for bus access
                        lqe.inst->pcState().instAddr(), // PC
                        cpu->getThreadState(0)->contextId(), // cid
                        0); // tid

        lqe.memReq = mem_req;
        LoadTranslation *trans = new LoadTranslation(this, load_itr->first);

        lqe.status = LQEntry::Status::IssuedTranslation;
        unissuedLoads.erase(load_itr);

        cpu->getDTBPtr()->translateTiming(mem_req,
                cpu->getThreadState(0)->getTC(),
                trans,
                BaseTLB::Read);

        ++loads;
    }
}

void
CribLSQBank::finishLoadTranslation(const Fault &fault, RequestPtr req,
    const CribSeqNum &seq_num)
{
    auto load_itr = loadQueue.find(seq_num);
    if (load_itr == loadQueue.end() || req != load_itr->second->memReq) {
        DPRINTF(CribLSQ, "[sn:%i]: Ignoring dtlb completed after squash.\n",
                seq_num.seqNum);
        delete req;
        return;
    }

    LQEntry &lqe = *(load_itr->second);
    if (fault == NoFault) {
        LQEntry &lqe = *(load_itr->second);
        lqe.physAddr = req->getPaddr();

        forwardData(load_itr->first, lqe);

        if (lqe.forwardStatus == LQEntry::ForwardStatus::FullForward && !req->isLLSC()) {

            if (!lqe.prefetch) {
                if (lqe.split) {
                    if (lqe.split_hi) {
                        lqe.inst->cribEntry->splitHiDone = true;
                    } else {
                        lqe.inst->cribEntry->splitLoDone = true;
                    }
                    if (lqe.inst->cribEntry->splitLoDone &&
                            lqe.inst->cribEntry->splitHiDone) {
                        handleSplitLoad(load_itr->first, lqe);
                    }
                } else {
                    Packet data_pkt = Packet(req,
                        req->isLLSC() ? MemCmd::LoadLockedReq : MemCmd::ReadReq);
                    data_pkt.dataStatic(lqe.data);
                    lqe.inst->completeAcc(&data_pkt);
                }
            }
            delete req;
            lqe.status = LQEntry::Status::Executed;
        } else {
            // Partial or no foward
            PacketPtr data_pkt = new Packet(req,
                    req->isLLSC() ? MemCmd::LoadLockedReq : MemCmd::ReadReq);
            data_pkt->senderState = new LQSenderState(seq_num);
            data_pkt->dataDynamicArray(new uint8_t[lqe.size]);
            DPRINTF(CribLSQ, "[sn:%i]: Translation succeeded. Performing "
                    "dcache read. [vaddr:%#x-%#x][paddr:%#x-%#x].\n",
                    seq_num.seqNum,
                    lqe.addr, lqe.addr + lqe.size - 1,
                    lqe.physAddr, lqe.physAddr + lqe.size -1);

            if (!cpu->getDataPort().sendTimingReq(data_pkt)) {
                DPRINTF(CribLSQ, "[sn:%i]: Dcache read failed.  Will retry.\n",
                        seq_num.seqNum);
                delete data_pkt->senderState;
                delete req;
                delete data_pkt;
                lqe.status = LQEntry::Status::Allocated;
                unissuedLoads.insert(std::make_pair(seq_num,
                                     load_itr->second));
                return;
            } else {
                DPRINTF(CribLSQ, "[sn:%i]: Doing dcache access.\n",
                        seq_num.seqNum);
                lqe.status = LQEntry::Status::IssuedLoad;
            }
        }
    } else {
        DPRINTF(CribLSQ, "[sn:%i]: Translation faulted.\n", seq_num.seqNum);
        if (!lqe.prefetch) {
            lqe.inst->cribEntry->markExecuted();
            lqe.inst->fault = fault;
            lqe.status = LQEntry::Status::Executed;
        } else {
            DPRINTF(CribLSQ, "[sn:%i]: Ignoring prefetch fault.\n", seq_num.seqNum);
            lqe.inst->prefetchOutstanding = false;
            delete load_itr->second;
            loadQueue.erase(load_itr);
        }
        delete req;
    }
}

inline void
CribLSQBank::forwardData(const CribSeqNum &seq_num, LQEntry &lqe)
{
    auto upper_bound = storeQueue.upper_bound(seq_num);
    Addr load_lower = lqe.addr;
    Addr load_upper = lqe.addr + lqe.size - 1;
    lqe.forwardStatus = LQEntry::ForwardStatus::NoForward;
    std::map<unsigned, SQEntry*> byte_fwds;

    for (auto st_itr = storeQueue.begin(); st_itr != upper_bound; ++st_itr) {
        SQEntry &sqe = *(st_itr->second);
        Addr store_lower = sqe.addr;
        Addr store_upper = sqe.addr + sqe.size -1;

        if (store_lower <= load_upper && store_upper >= load_lower) {
            // Store overlaps part of load
            DPRINTF(CribLSQ, "[sn:%i]: Forwarding data from overlapping store "
                    "[ld:%#x-%#x] [st:%#x-%#x] [st sn:%i].\n",
                    seq_num.seqNum,
                    load_lower, load_upper,
                    store_lower, store_upper,
                    st_itr->first.seqNum);

            Addr load_offset;
            Addr store_offset;
            unsigned bytes;
            if (load_lower < store_lower) {
                load_offset = store_lower - load_lower;
                store_offset = 0;
                bytes = std::min(load_upper, store_upper) - store_lower + 1;
            } else {
                load_offset = 0;
                store_offset = load_lower - store_lower;
                bytes = std::min(load_upper, store_upper) - load_lower + 1;
            }
            memcpy(&lqe.data[load_offset], &sqe.data[store_offset], bytes);

            while (bytes) {
                --bytes;
                byte_fwds[load_offset + bytes] = st_itr->second;
            }
        }
    }

    if (!byte_fwds.empty()) {
        if (byte_fwds.size() == lqe.size) {
            DPRINTF(CribLSQ, "[sn:%i]: Data fully forwarded.\n",
                    seq_num.seqNum);
            lqe.forwardStatus = LQEntry::ForwardStatus::FullForward;
        } else {
            DPRINTF(CribLSQ, "[sn:%i]: Data partially forwarded.\n", seq_num.seqNum);
            lqe.forwardStatus = LQEntry::ForwardStatus::PartialForward;
            lqe.forwardBytes.resize(lqe.size, false);
            for (auto fwd_itr = byte_fwds.begin(); fwd_itr != byte_fwds.end();
                    ++fwd_itr) {
                lqe.forwardBytes[fwd_itr->first] = true;
            }
        }

        for (auto fwd_itr = byte_fwds.begin(); fwd_itr != byte_fwds.end();
             ++fwd_itr) {
            fwd_itr->second->recordLoadForward(seq_num);
        }
    }
}

inline void
CribLSQBank::writebackStores()
{
    unsigned stores = 0;
    auto wb = storeQueue.begin();

    while (wb != storeQueue.end() &&
           (wb->second->status == SQEntry::Status::Senior ||
            wb->second->status == SQEntry::Status::SeniorDispatched ||
            wb->second->status == SQEntry::Status::SeniorComplete) &&
           stores < storeAllocBandwidth) {

        SQEntry &wbe = *(wb->second);
        if (wbe.status == SQEntry::Status::SeniorDispatched ||
            wbe.status == SQEntry::Status::SeniorComplete) {
            ++wb;
            continue;
        }

        wbe.pkt->senderState = new SQSenderState(wb->first);
        if (!cpu->getDataPort().sendTimingReq(wbe.pkt)) {
            delete wbe.pkt->senderState;
            break;
        } else {
            wbe.status = SQEntry::Status::SeniorDispatched;
            DPRINTF(CribLSQ, "[sn:%i]: Dispatching senior store.\n",
                    wb->first.seqNum);
        }

        ++wb;
        ++stores;
    }
}

inline void
CribLSQBank::probeLoadQueue(Addr addr, unsigned size,
    const CribSeqNum &seq_num)
{
    auto load_itr = loadQueue.lower_bound(seq_num);
    Addr store_lower = addr;
    Addr store_upper = addr + size - 1;

    while (load_itr != loadQueue.end()) {
        LQEntry &lqe = *(load_itr->second);
        Addr load_lower = lqe.addr;
        Addr load_upper = lqe.addr + lqe.size -1;
        if (lqe.status == LQEntry::Status::Executed ||
            lqe.status == LQEntry::Status::IssuedLoad) {

            if (store_lower <= load_upper && store_upper >= load_lower) {
                DPRINTF(CribLSQ, "[sn:%i]: Overlapping load.  Invalidating. "
                        "[ld:%#x-%#x][st:%#x-%#x][ld sn:%i].\n",
                        seq_num.seqNum,
                        load_lower, load_upper,
                        store_lower, store_upper,
                        load_itr->first.seqNum);

                // Delete the load & mark instruction as executing
                loadRequests.insert(std::make_pair(load_itr->first,
                                    LoadRequest(lqe.inst,
                                                lqe.addr,
                                                lqe.size,
                                                lqe.flags,
                                                lqe.split,
                                                lqe.split_hi,
                                                lqe.prefetch)));
                if (!lqe.prefetch) {
                    if (lqe.split) {
                        if (lqe.split_hi) {
                            lqe.inst->cribEntry->splitHiDone = false;
                        } else {
                            lqe.inst->cribEntry->splitLoDone = false;
                        }
                    }
                    lqe.inst->cribEntry->status = CribEntry::Status::Executing;
                }
                // Remove LQ entry
                delete load_itr->second;
                loadQueue.erase(load_itr++);
                continue;
            }
        }
        ++load_itr;
    }
}

inline void
CribLSQBank::completeDataAccess(PacketPtr pkt)
{
    if (pkt->isRead()) {
        LQSenderState *load_state =
            dynamic_cast<LQSenderState*>(pkt->senderState);
        auto load_itr = loadQueue.find(load_state->seqNum);
        if (load_itr == loadQueue.end() ||
            (pkt->req != load_itr->second->memReq) ||
            (pkt->req->getVaddr() != load_itr->second->addr)) {
            DPRINTF(CribLSQ, "[sn:%i]: Ignoring Dcache access completed after "
                    "squash.\n", load_state->seqNum.seqNum);
        } else {
            LQEntry &lqe = *(load_itr->second);
            DPRINTF(CribLSQ, "[sn:%i]: Dcache access complete.\n",
                    lqe.inst->seqNum);

            assert(lqe.addr == pkt->req->getVaddr());
            assert(lqe.size == pkt->getSize());

            if (lqe.forwardStatus == LQEntry::ForwardStatus::PartialForward) {
                uint8_t *tmp_data = pkt->getPtr<uint8_t>();
                for (unsigned b = 0; b < lqe.size; b++) {
                    if (!lqe.forwardBytes[b]) {
                        lqe.data[b] = tmp_data[b];
                    }
                }
            } else {
                memcpy(lqe.data, pkt->getPtr<uint8_t>(), lqe.size);
            }

            lqe.status = LQEntry::Status::Executed;

            forwardData(load_itr->first, lqe);
            if (!lqe.prefetch) {
                if (lqe.split) {
                    if (lqe.split_hi) {
                        lqe.inst->cribEntry->splitHiDone = true;
                    } else {
                        lqe.inst->cribEntry->splitLoDone = true;
                    }
                    if (lqe.inst->cribEntry->splitLoDone &&
                            lqe.inst->cribEntry->splitHiDone) {
                        handleSplitLoad(load_itr->first, lqe);
                    }
                } else {
                    // Dig through store queue to forward data
                    Packet temp_pkt(pkt);
                    temp_pkt.dataStatic(lqe.data);
                    lqe.inst->completeAcc(&temp_pkt);
                }
            }
        }
    } else {
        assert(pkt->isResponse());
        SQSenderState *store_state =
            dynamic_cast<SQSenderState*>(pkt->senderState);
        DPRINTF(CribLSQ, "[sn:%i]: Looking up SQ entry for returning "
                " writeback.\n", store_state->seqNum.seqNum);
        auto store_itr = storeQueue.find(store_state->seqNum);
        assert(store_itr != storeQueue.end());

        SQEntry &sqe = *(store_itr->second);
        if (sqe.inst->isStoreConditional()) {
            if (sqe.split) panic("Not handling split store conditionals yet");
            sqe.inst->completeAcc(pkt);
            sqe.inst->setStoreConditionalComplete();
        }

        DPRINTF(CribLSQ, "[sn:%i]: Store writeback complete.\n",
                store_state->seqNum.seqNum);
        sqe.status = SQEntry::Status::SeniorComplete;
        /*
        delete store_itr->second;
        storeQueue.erase(store_itr);
        */
    }

    delete pkt->senderState;
    delete pkt->req;
    delete pkt;
}

inline void
CribLSQBank::deassertStoreForwardedLoads(const CribSeqNum &seq_num,
    const SQEntry &sqe)
{
    for (auto seq_itr = sqe.fwdLoadSeq.begin();
            seq_itr != sqe.fwdLoadSeq.end(); ++seq_itr) {
        auto ld_itr = loadQueue.find(*seq_itr);
        if (ld_itr == loadQueue.end()) continue;

        // Delete the load & mark instruction as executing
        const LQEntry &lqe = *(ld_itr->second);
        loadRequests.insert(std::make_pair(ld_itr->first,
                    LoadRequest(lqe.inst,
                                lqe.addr,
                                lqe.size,
                                lqe.flags,
                                lqe.split,
                                lqe.split_hi,
                                lqe.prefetch)));
        if (!lqe.prefetch) {
            if (lqe.split) {
                if (lqe.split_hi) {
                    lqe.inst->cribEntry->splitHiDone = false;
                } else {
                    lqe.inst->cribEntry->splitLoDone = false;
                }
            }
            lqe.inst->cribEntry->status = CribEntry::Status::Executing;
        }
        // Remove LQ entry
        delete ld_itr->second;
        unissuedLoads.erase(ld_itr->first);
        loadQueue.erase(ld_itr);
    }
}

inline void
CribLSQBank::deassertRequest(const CribDynInstPtr &inst, bool committed)
{
    const CribSeqNum seq_num = inst->getCribSeqNum();
    if (inst->isLoad()) {
        loadRequests.erase(seq_num);
        auto load_itr = loadQueue.find(seq_num);
        if (load_itr != loadQueue.end()) {
            LQEntry &lqe = *(load_itr->second);
            if (lqe.split) {
                if (lqe.split_hi) {
                    lqe.inst->cribEntry->splitHiDone = false;
                } else {
                    lqe.inst->cribEntry->splitLoDone = false;
                }
            }
            delete load_itr->second;
            loadQueue.erase(load_itr);
        }
        unissuedLoads.erase(seq_num);
    } else {
        storeRequests.erase(seq_num);
        auto store_itr = storeQueue.find(seq_num);
        if (store_itr != storeQueue.end()) {
            SQEntry &sqe = *(store_itr->second);
            if(sqe.status != SQEntry::Status::Senior &&
                    sqe.status != SQEntry::Status::SeniorDispatched &&
                    sqe.status != SQEntry::Status::SeniorComplete){
            if (sqe.split) {
                if (sqe.split_hi) {
                    sqe.inst->cribEntry->splitHiDone = false;
                } else {
                    sqe.inst->cribEntry->splitLoDone = false;
                }
            }

            if (!committed) {
                deassertStoreForwardedLoads(seq_num, sqe);
            }
            if (sqe.status >= SQEntry::Status::Executed) {
                delete sqe.memReq;
                delete sqe.pkt;
            }
            delete store_itr->second;
            storeQueue.erase(store_itr);
            }
        }
        unissuedStores.erase(seq_num);
    }
}

inline void
CribLSQBank::SquashYoungerMemoryOps(InstSeqNum seq_num){
    auto ld_req_itr = loadRequests.begin();
    while (ld_req_itr != loadRequests.end()) {
        if (ld_req_itr->first.seqNum > seq_num) {
            loadRequests.erase(ld_req_itr++);
        } else {
            ++ld_req_itr;
        }
    }

    auto st_req_itr = storeRequests.begin();
    while (st_req_itr != storeRequests.end()) {
        if (st_req_itr->first.seqNum > seq_num) {
            storeRequests.erase(st_req_itr++);
        } else {
            ++st_req_itr;
        }
    }

    // Clear load queue
    auto ld_itr = loadQueue.begin();
    while (ld_itr != loadQueue.end()) {
        if (ld_itr->first.seqNum > seq_num) {
            delete ld_itr->second;
            loadQueue.erase(ld_itr++);
        } else {
            ++ld_itr;
        }
    }

    auto st_itr = storeQueue.begin();
    while (st_itr != storeQueue.end()) {
        if (st_itr->first.seqNum > seq_num) {
            SQEntry &sqe = *(st_itr->second);
            assert(sqe.status != SQEntry::Status::Senior &&
                    sqe.status != SQEntry::Status::SeniorDispatched &&
                    sqe.status != SQEntry::Status::SeniorComplete);

            if (sqe.status >= SQEntry::Status::Executed) {
                delete sqe.memReq;
                delete sqe.pkt;
            }
            delete st_itr->second;
            storeQueue.erase(st_itr++);
        } else {
            ++st_itr;
        }
    }

    // Clear unissued loads
    auto un_itr = unissuedLoads.begin();
    while (un_itr != unissuedLoads.end()) {
        if (un_itr->first.seqNum > seq_num) {
            unissuedLoads.erase(un_itr++);
        } else {
            ++un_itr;
        }
    }

    auto st_un_itr = unissuedStores.begin();
    while (st_un_itr != unissuedStores.end()) {
        if (st_un_itr->first.seqNum > seq_num) {
            unissuedStores.erase(st_un_itr++);
        } else {
            ++st_un_itr;
        }
    }
}

inline void
CribLSQBank::deassertAllPrefetches(InstSeqNum seq_num)
{
    // Clear request queue
    auto req_itr = loadRequests.begin();
    while (req_itr != loadRequests.end()) {
        if (req_itr->first.seqNum == seq_num) {
            loadRequests.erase(req_itr++);
        } else {
            ++req_itr;
        }
    }

    // Clear load queue
    auto ld_itr = loadQueue.begin();
    while (ld_itr != loadQueue.end()) {
        if (ld_itr->first.seqNum == seq_num) {
            delete ld_itr->second;
            loadQueue.erase(ld_itr++);
        } else {
            ++ld_itr;
        }
    }

    // Clear unissued loads
    auto un_itr = unissuedLoads.begin();
    while (un_itr != unissuedLoads.end()) {
        if (un_itr->first.seqNum == seq_num) {
            unissuedLoads.erase(un_itr++);
        } else {
            ++un_itr;
        }
    }
}

inline void
CribLSQBank::makeSenior(const CribSeqNum &seq_num)
{
    auto st_itr = storeQueue.begin();
    while(st_itr != storeQueue.end()){
        if(st_itr->first < seq_num){
            if((st_itr->second->status != SQEntry::Status::Senior) &&
                (st_itr->second->status != SQEntry::Status::SeniorDispatched) &&
                (st_itr->second->status != SQEntry::Status::SeniorComplete)){
                DPRINTF(CribLSQ, "Erasing entry with Sn %i, since we are marking Sn %i as senior\n. Obviously we messed up somewhere\n",st_itr->first.seqNum, seq_num.seqNum); 
                delete st_itr->second;
                storeQueue.erase(st_itr); 
            }
        }else{
            break;
        }
        st_itr++; 
    }

    DPRINTF(CribLSQ, "[sn:%i]: Store transitioning to senior.\n",
            seq_num.seqNum);
    auto store_itr = storeQueue.find(seq_num);
    assert(store_itr != storeQueue.end());

    SQEntry &sqe = *(store_itr->second);
    if (sqe.status != SQEntry::Status::SeniorDispatched) {
        DPRINTF(CribLSQ, "[sn:%i]: Store finished transitioning to senior.\n",
                seq_num.seqNum);
        sqe.status = SQEntry::Status::Senior;
    }
}

inline void
CribLSQBank::handleSplitLoad(const CribSeqNum &seq_num, const LQEntry &lqe)
{
    uint8_t *merged = new uint8_t[lqe.inst->memSize];

    // Copy over data from this lqe
    unsigned offset = 0;
    if (lqe.split_hi) {
        offset = lqe.addr - lqe.inst->memAddr;
    }
    memcpy(merged+offset, lqe.data, lqe.size);

    LQEntry *other_lqe;
    if (lqe.split_hi) {
        other_lqe = prevBank->getLQEntry(seq_num);
        offset = 0;
    } else {
        other_lqe = nextBank->getLQEntry(seq_num);
        offset = other_lqe->addr - lqe.addr;
    }
    memcpy(merged+offset, other_lqe->data, other_lqe->size);
    Request mem_req(0, // asid
                    lqe.inst->memAddr, // addr (aligned)
                    lqe.inst->memSize, // request size (full line)
                    lqe.flags, // Flags
                    cpu->dataMasterId(),  // ID for bus access
                    lqe.inst->pcState().instAddr(), // PC
                    cpu->getThreadState(0)->contextId(), //cid
                    0); // tid

    Packet temp_pkt(&mem_req, MemCmd::ReadReq);
    temp_pkt.dataStatic(merged);
    lqe.inst->completeAcc(&temp_pkt);

    delete[] merged;
}

inline CribLSQBank::LQEntry*
CribLSQBank::getLQEntry(const CribSeqNum &seq_num)
{
    auto itr = loadQueue.find(seq_num);
    assert(itr != loadQueue.end());
    return itr->second;
}

inline void
CribLSQBank::handleSplitStore(const CribSeqNum &seq_num, const SQEntry &sqe)
{
    uint8_t *merged = new uint8_t[sqe.inst->memSize];

    // Copy over data from this sqe
    unsigned offset = 0;
    if (sqe.split_hi) {
        offset = sqe.addr - sqe.inst->memAddr;
    }
    memcpy(merged+offset, sqe.data, sqe.size);

    SQEntry *other_sqe;
    if (sqe.split_hi) {
        other_sqe = prevBank->getSQEntry(seq_num);
        offset = 0;
    } else {
        other_sqe = nextBank->getSQEntry(seq_num);
        offset = other_sqe->addr - sqe.addr;
    }
    memcpy(merged+offset, other_sqe->data, other_sqe->size);

    Request mem_req(0, // asid
                    sqe.inst->memAddr, // addr (aligned)
                    sqe.inst->memSize, // request size (full line)
                    sqe.flags, // Flags (will need to modify this later)
                    cpu->dataMasterId(),  // ID for bus access
                    sqe.inst->pcState().instAddr(), // PC
                    cpu->getThreadState(0)->contextId(), // cid
                    0); // tid
    Packet temp_pkt(&mem_req, MemCmd::WriteReq);
    temp_pkt.dataStatic(merged);
    sqe.inst->completeAcc(&temp_pkt);

    delete[] merged;
}

inline CribLSQBank::SQEntry*
CribLSQBank::getSQEntry(const CribSeqNum &seq_num)
{
    auto itr = storeQueue.find(seq_num);
    assert(itr != storeQueue.end());
    return itr->second;
}
