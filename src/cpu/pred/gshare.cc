/*
 *
 * Authors: Vignyan Reddy
 */

#include "base/bitfield.hh"
#include "base/intmath.hh"
#include "cpu/pred/gshare.hh"

Gshare::Gshare(const Params *params)
    : BPredUnit(params),
      globalPredictorSize(params->globalPredictorSize),
      globalCtrBits(params->globalCtrBits),
      globalHistoryBits(params->globalHistoryBits),
      instShiftAmt(params->instShiftAmt)
{
    if (!isPowerOf2(globalPredictorSize)) {
        fatal("Invalid global predictor size!\n");
    }

    //Setup the array of counters for the global predictor
    globalCtrs.resize(globalPredictorSize);

    for (int i = 0; i < globalPredictorSize; ++i)
        globalCtrs[i].setBits(globalCtrBits);

    //Clear the global history
    globalHistory = 0;
    // Set up the global history mask
    // this is equivalent to mask(log2(globalPredictorSize)
    globalHistoryMask = globalPredictorSize - 1;

    // Set thresholds for the three predictors' counters
    // This is equivalent to (2^(Ctr))/2 - 1
    globalThreshold = (ULL(1) << (globalCtrBits - 1)) - 1;
}

inline
void
Gshare::updateGlobalHistTaken()
{
    globalHistory = (globalHistory << 1) | 1;
    globalHistory = globalHistory & globalHistoryMask;
}

inline
void
Gshare::updateGlobalHistNotTaken()
{
    globalHistory = (globalHistory << 1);
    globalHistory = globalHistory & globalHistoryMask;
}

void
Gshare::btbUpdate(Addr branch_addr, void * &bp_history)
{
    //Update Global History to Not Taken (clear LSB)
    globalHistory &= (globalHistoryMask & ~ULL(1));
}

bool
Gshare::lookup(Addr branch_addr, void * &bp_history)
{
    //Lookup in the global predictor to get its branch prediction
    unsigned global_index = (branch_addr ^ globalHistory) & globalHistoryMask;

    bool global_prediction =
      globalCtrs[global_index].read() > globalThreshold;

    // Create BPHistory and pass it back to be recorded.
    BPHistory *history = new BPHistory;
    history->globalHistory = globalHistory;
    history->globalPredTaken = global_prediction;
    history->alreadySquashed = false;
    history->condBranch = true;
    history->brAddr = branch_addr;
    bp_history = (void *)history;

    if (global_prediction) {
        updateGlobalHistTaken();
        return true;
    } else {
        updateGlobalHistNotTaken();
        return false;
    }
}

void
Gshare::uncondBranch(Addr pcs_in, void * &bp_history)
{
    // Create BPHistory and pass it back to be recorded.
    BPHistory *history = new BPHistory;
    history->globalHistory = globalHistory;
    history->globalPredTaken = true;
    history->alreadySquashed = false;
    history->condBranch = false;
    history->brAddr = pcs_in;
    bp_history = static_cast<void *>(history);

    updateGlobalHistTaken();
}

void
Gshare::squash(bool taken, BPHistory *history)
{
    globalHistory = history->globalHistory;
    if(taken){
        updateGlobalHistTaken();
    }else{
        updateGlobalHistNotTaken();
    }
}

void
Gshare::update(Addr branch_addr, bool taken, void *bp_history,
                     bool squashed)
{
    BPHistory *history = static_cast<BPHistory *>(bp_history);
    // Update may also be called if the Branch target is incorrect even if
    // the prediction is correct. In that case do not update the counters.

    if (history->alreadySquashed && !squashed) {
        delete history; 
        return;
    }

    if(history->condBranch){

        unsigned ghist = history->globalHistory;

        unsigned global_index = (branch_addr ^ ghist) & globalHistoryMask;
        if (taken) {
            globalCtrs[global_index].increment();
        } else {
            globalCtrs[global_index].decrement();
        }
    }
    if (squashed) {
		squash(taken, history);
        history->alreadySquashed = true;
    }else{
        // We're done with this history, now delete it.
        delete history;
    }
}

void
Gshare::squash(void *bp_history)
{
    BPHistory *history = static_cast<BPHistory *>(bp_history);

    // Restore global history to state prior to this branch.
    globalHistory = history->globalHistory;
    // Delete this BPHistory now that we're done with it.
    delete history;
}

#ifdef DEBUG
int
Gshare::BPHistory::newCount = 0;
#endif
