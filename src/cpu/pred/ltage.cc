#include "base/intmath.hh"
#include "base/misc.hh"
#include "base/trace.hh"
#include "cpu/pred/ltage.hh"
#include "debug/Fetch.hh"
#include "debug/LTage.hh"

LTAGE::LTAGE(const Params *params)
    : BPredUnit(params),
    logSizeBiMP(params->logSizeBiMP),
    logSizeTagTables(params->logSizeTagTables),
    logSizeLoopPred(params->logSizeLoopPred),
    nHistoryTables(params->nHistoryTables),
    tagTableCounterBits(params->tagTableCounterBits),
    histBufferSize(params->histBufferSize),
    minHist(params->minHist),
    maxHist(params->maxHist),
    minTagWidth(params->minTagWidth)
{
    useAltPredForNewlyAllocated =0;
    seed = 0;
    logTick= 19;
    tCounter= (1 << (logTick - 1));	
    pathHist = 0;

    GHIST = (uint8_t*)malloc(histBufferSize*sizeof(uint8_t));
    gHist = GHIST;
    for(int i=0; i< histBufferSize; i++){
        gHist[i]=0;
    }
    ptGhist = 0;

    histLengths = new int [nHistoryTables+1];
    histLengths[1] = minHist;
    histLengths[nHistoryTables] = maxHist;

    for(int i=2; i<=nHistoryTables; i++){
        histLengths[i] = (int) (((double) minHist*
                    pow ((double) (maxHist) / (double) minHist,
                        (double) (i - 1) / (double) ((nHistoryTables- 1)))) + 0.5);
    }

    tagWidths = new int [15];
    tagWidths[1] = minTagWidth;
    tagWidths[2] = minTagWidth;
    tagWidths[3] = minTagWidth + 1;
    tagWidths[4] = minTagWidth + 1;
    tagWidths[5] = minTagWidth + 2;
    tagWidths[6] = minTagWidth + 3;
    tagWidths[7] = minTagWidth + 4;
    tagWidths[8] = minTagWidth + 5;
    tagWidths[9] = minTagWidth + 5;
    tagWidths[10] = minTagWidth + 6;
    tagWidths[11] = minTagWidth + 7;
    tagWidths[12] = minTagWidth + 8;

    tagTableSizes = new int [15];
    for (int i = 1; i <= 2; i++)
        tagTableSizes[i] = logSizeTagTables - 1;
    for (int i = 3; i <= 6; i++)
        tagTableSizes[i] = logSizeTagTables;
    for (int i = 7; i <= 10; i++)
        tagTableSizes[i] = logSizeTagTables - 1;
    for (int i = 11; i <= 12; i++)
        tagTableSizes[i] = logSizeTagTables - 2;

    computeIndices = new folded_history[nHistoryTables+1];
    computeTags[0] = new folded_history[nHistoryTables+1];
    computeTags[1] = new folded_history[nHistoryTables+1];

    for (int i = 1; i <= nHistoryTables; i++){
        computeIndices[i].init (histLengths[i], (tagTableSizes[i]));
        computeTags[0][i].init (computeIndices[i].OLENGTH, tagWidths[i]);
        computeTags[1][i].init (computeIndices[i].OLENGTH, tagWidths[i] - 1);
        DPRINTF(LTage, "HistLength:%d, TTSize:%d, TTTWidth:%d\n", histLengths[i], tagTableSizes[i], tagWidths[i]); 
    }

    btable = new bentry[1 << logSizeBiMP];
    ltable = new lentry[1 << logSizeLoopPred];
    gtable = (gentry**)malloc((nHistoryTables+1) * sizeof(gentry*));
    for(int i=1; i<=nHistoryTables; i++){
        gtable[i] = new gentry[1<<(tagTableSizes[i])];
    }

    tableIndices = new int [nHistoryTables+1];
    tableTags = new int [nHistoryTables+1];

    loopUseCounter = 0;

}


int LTAGE::bindex (Addr pc_in){
    //return ((pc_in>>1) & ((1 << (logSizeBiMP)) - 1));
    return ((pc_in) & ((1 << (logSizeBiMP)) - 1));
}




int LTAGE::lindex(Addr pc_in){
    //return (((pc_in>>1) & ((1 << (logSizeLoopPred - 2)) - 1)) << 2);
    return (((pc_in) & ((1 << (logSizeLoopPred - 2)) - 1)) << 2);
}




int LTAGE::F (int A, int size, int bank){
    int A1, A2;

    A = A & ((1 << size) - 1);
    A1 = (A & ((1 << tagTableSizes[bank]) - 1));
    A2 = (A >> tagTableSizes[bank]);
    A2 =
        ((A2 << bank) & ((1 << tagTableSizes[bank]) - 1)) + (A2 >> (tagTableSizes[bank] - bank));
    A = A1 ^ A2;
    A = ((A << bank) & ((1 << tagTableSizes[bank]) - 1)) + (A >> (tagTableSizes[bank] - bank));
    return (A);
}


// gindex computes a full hash of pc, ghist and pathHist
int LTAGE::gindex (Addr pc, int bank){
    int index;
    int M = (histLengths[bank] > 16) ? 16 : histLengths[bank];
    index =
        //(pc>>1) ^ ((pc>>1) >> (abs (tagTableSizes[bank] - bank) + 1)) ^
        (pc) ^ ((pc) >> (abs (tagTableSizes[bank] - bank) + 1)) ^
        computeIndices[bank].comp ^ F(pathHist, M, bank);

    return (index & ((1 << (tagTableSizes[bank])) - 1));
}




//  tag computation
uint16_t LTAGE::gtag (Addr pc, int bank)
{
    //int tag = (pc>>1) ^ computeTags[0][bank].comp ^ (computeTags[1][bank].comp << 1);
    int tag = (pc) ^ computeTags[0][bank].comp ^ (computeTags[1][bank].comp << 1);

    return (tag & ((1 << tagWidths[bank]) - 1));
}


// up-down saturating counter
void LTAGE::ctrupdate (int8_t & ctr, bool taken, int nbits) {
    if (taken){
        if (ctr < ((1 << (nbits - 1)) - 1))
            ctr++;
    } else {
        if (ctr > -(1 << (nbits - 1)))
            ctr--;
    }
}

//Bimodal prediction
bool LTAGE::getbim (Addr pc, BranchInfo* bi) {
    return (btable[bi->bimodalIndex].pred > 0);
}


// update  the bimodal predictor: a hysteresis bit is shared among 4 prediction bits
void LTAGE::baseupdate (Addr pc, bool taken, BranchInfo* bi){
    int inter = (btable[bi->bimodalIndex].pred << 1) + btable[bi->bimodalIndex ].hyst;
    if(taken){
        if (inter < 3)
            inter += 1;
    }
    else if (inter > 0)
        inter--;
    btable[bi->bimodalIndex].pred = inter >> 1;
    btable[bi->bimodalIndex].hyst = (inter & 1);
    DPRINTF(LTage, "Updating branch %lx, pred:%d, hyst:%d\n",pc, btable[bi->bimodalIndex].pred,btable[bi->bimodalIndex].hyst); 
}


//loop prediction: only used if high confidence
bool LTAGE::getloop (Addr pc, BranchInfo* bi)
{
    bi->loopHit = -1;
    bi->loopPredValid = false;
    bi->loopIndex = lindex (pc);
    //bi->loopTag = ((pc>>1) >> (logSizeLoopPred - 2));
    bi->loopTag = ((pc) >> (logSizeLoopPred - 2));

    for (int i = 0; i < 4; i++){
        if (ltable[bi->loopIndex + i].TAG == bi->loopTag){
            bi->loopHit = i;
            bi->loopPredValid = (ltable[bi->loopIndex + i].confid >= 3);
            bi->CurrentIter = ltable[bi->loopIndex + i].CurrentIterSpec;
            if (ltable[bi->loopIndex + i].CurrentIterSpec + 1 == ltable[bi->loopIndex + i].NbIter){
                return !(ltable[bi->loopIndex + i].dir);
            }else{
                return (ltable[bi->loopIndex + i].dir);
            }
        }
    }
    return false;
}

void LTAGE::SpecLoopUpdate(Addr pc, bool taken, BranchInfo* bi){
    if(bi->loopHit>=0){
        int index = lindex(pc); 
        if(taken != ltable[index].dir){
            ltable[index].CurrentIterSpec = 0;
        }else{
            ltable[index].CurrentIterSpec++;
        }
    }
}

void LTAGE::loopupdate (Addr pc, bool Taken, BranchInfo* bi){
    int idx = bi->loopIndex + bi->loopHit;
    if (bi->loopHit >= 0) {
        //already a hit
        if (bi->loopPredValid) {
            if (Taken != bi->loopPred) {
                // free the entry
                ltable[idx].NbIter = 0;
                ltable[idx].age = 0;
                ltable[idx].confid = 0;
                ltable[idx].CurrentIter = 0;
                return;
            }else if (bi->loopPred != bi->tagePred){
                DPRINTF(LTage, "Loop Prediction success:%lx\n",pc);  
                if (ltable[idx].age < 7)
                    ltable[idx].age++;
            }
        }


        ltable[idx].CurrentIter++;
        if (ltable[idx].CurrentIter > ltable[idx].NbIter){
            ltable[idx].confid = 0;
            if (ltable[idx].NbIter != 0){
                // free the entry
                ltable[idx].NbIter = 0;
                ltable[idx].age = 0;
                ltable[idx].confid = 0;
            }
        }

        if (Taken != ltable[idx].dir){

            if (ltable[idx].CurrentIter == ltable[idx].NbIter){
                DPRINTF(LTage, "Loop End predicted successfully:%lx\n", pc); 

                if (ltable[idx].confid < 7){
                    ltable[idx].confid++;
                }
                //just do not predict when the loop count is 1 or 2
                if (ltable[idx].NbIter < 3){
                    // free the entry
                    ltable[idx].dir = Taken;
                    ltable[idx].NbIter = 0;
                    ltable[idx].age = 0;
                    ltable[idx].confid = 0;
                }
            } else {
                DPRINTF(LTage, "Loop End predicted incorrectly:%lx\n", pc); 
                if (ltable[idx].NbIter == 0) {
                    // first complete nest;
                    ltable[idx].confid = 0;
                    ltable[idx].NbIter = ltable[idx].CurrentIter;
                } else {
                    //not the same number of iterations as last time: free the entry

                    ltable[idx].NbIter = 0;
                    ltable[idx].age = 0;
                    ltable[idx].confid = 0;

                }
            }
            ltable[idx].CurrentIter = 0;
        }

    } else if (Taken){//try to allocate an entry on taken branch
        for (int i = 0; i < 4; i++) {
            int loop_hit = (seed + i) & 3;
            idx = bi->loopIndex + loop_hit;
            if (ltable[idx].age == 0) {
                DPRINTF(LTage, "Allocating loop pred entry for branch %lx\n", pc); 
                ltable[idx].dir = !Taken;
                ltable[idx].TAG = bi->loopTag;
                ltable[idx].NbIter = 0;
                ltable[idx].age = 7;
                ltable[idx].confid = 0;
                ltable[idx].CurrentIter = 1;
                break;

            }
            else
                ltable[idx].age--;
        }
    }

}


int LTAGE::MYRANDOM (){
    seed++;
    return (seed & 3);
};

// shifting the global history:  we manage the history in a big table in order to reduce simulation time
void LTAGE::updateghist (uint8_t * &h, bool dir, uint8_t * tab, int &PT){
    if (PT == 0){
        DPRINTF(LTage, "Rolling over the histories\n"); 
        for (int i = 0; i < maxHist; i++)
            tab[histBufferSize - maxHist + i] = tab[i];
        PT =  histBufferSize - maxHist;
        h = &tab[PT];
    }
    PT--;
    h--;
    h[0] = (dir) ? 1 : 0;
}

//prediction
bool LTAGE::predict (Addr branch_pc, bool cond_branch, void* &b){
    BranchInfo *bi = new BranchInfo(nHistoryTables+1);
    b = (void*)(bi);
    Addr pc = branch_pc;
    bool pred_taken = true;
    bi->loopHit = -1; 

    if (cond_branch){

        // TAGE prediction

        // computes the table addresses and the partial tags
        for (int i = 1; i <= nHistoryTables; i++){
            tableIndices[i] = gindex (pc, i);
            bi->tableIndices[i] = tableIndices[i]; 
            tableTags[i] = gtag (pc, i);
            bi->tableTags[i] = tableTags[i];
        }

        bi->bimodalIndex = bindex (pc);

        bi->hitBank = 0;
        bi->altBank = 0;
        //Look for the bank with longest matching history
        for (int i = nHistoryTables; i > 0; i--) {
            if (gtable[i][tableIndices[i]].tag == tableTags[i]) {
                bi->hitBank = i;
                bi->hitBankIndex = tableIndices[bi->hitBank];
                break;
            }
        }
        //Look for the alternate bank
        for (int i = bi->hitBank - 1; i > 0; i--){
            if (gtable[i][tableIndices[i]].tag == tableTags[i]){
                bi->altBank = i;
                bi->altBankIndex = tableIndices[bi->altBank];
                break;
            }
        }
        //computes the prediction and the alternate prediction
        if (bi->hitBank > 0){
            if (bi->altBank > 0){
                bi->altTaken = (gtable[bi->altBank][tableIndices[bi->altBank]].ctr >= 0);
            }else{
                bi->altTaken = getbim (pc, bi);
            }
            bi->longestMatchPred = (gtable[bi->hitBank][tableIndices[bi->hitBank]].ctr >= 0);
            bi->pseudoNewAlloc=(abs (2 * gtable[bi->hitBank][bi->hitBankIndex].ctr + 1) <= 1);
            //if the entry is recognized as a newly allocated entry and
            //useAltPredForNewlyAllocated is positive  use the alternate prediction
            if ((useAltPredForNewlyAllocated < 0)
                    || (abs (2 * gtable[bi->hitBank][tableIndices[bi->hitBank]].ctr + 1) > 1))
                bi->tagePred = bi->longestMatchPred;
            else
                bi->tagePred = bi->altTaken;
        } else {
            bi->altTaken = getbim (pc, bi);
            bi->tagePred = bi->altTaken;
            bi->longestMatchPred = bi->altTaken; 
        }
        //end TAGE prediction

        bi->loopPred = getloop (pc, bi);	// loop prediction

        pred_taken = (((loopUseCounter >= 0) && (bi->loopPredValid))) ? (bi->loopPred): (bi->tagePred);
        DPRINTF(LTage, "Predict for %lx: taken?:%d, loopTaken?:%d, loopValid?:%d, loopUseCounter:%d, tagePred:%d, altPred:%d\n", branch_pc, pred_taken, bi->loopPred, bi->loopPredValid, loopUseCounter, bi->tagePred, bi->altTaken);
    }
    bi->branchPC = branch_pc;
    bi->condBranch = cond_branch;
    SpecLoopUpdate(branch_pc, pred_taken, bi); 
    return pred_taken;
}



// PREDICTOR UPDATE
void LTAGE::update(Addr branch_pc, bool taken, BranchInfo* bi){
    int NRAND = MYRANDOM ();
    Addr pc = branch_pc;
    if (bi->condBranch){
        DPRINTF(LTage, "Updating tables for branch:%lx; taken?:%d\n", branch_pc, taken); 
        // first update the loop predictor
        loopupdate(pc, taken, bi);

        if (bi->loopPredValid){
            if (bi->tagePred != bi->loopPred){
                ctrupdate (loopUseCounter, (bi->loopPred== taken), 7);
            }
        }

        // TAGE UPDATE
        // try to allocate a  new entries only if prediction was wrong
        bool LongestMatchPred = false;
        bool ALLOC = ((bi->tagePred != taken) & (bi->hitBank < nHistoryTables));
        if (bi->hitBank > 0) {
            // Manage the selection between longest matching and alternate matching
            // for "pseudo"-newly allocated longest matching entry
             LongestMatchPred = bi->longestMatchPred; 
            bool PseudoNewAlloc = bi->pseudoNewAlloc; 
            // an entry is considered as newly allocated if its prediction counter is weak
            if (PseudoNewAlloc){
                if (LongestMatchPred == taken){
                    ALLOC = false;
                }
                // if it was delivering the correct prediction, no need to allocate a new entry
                //even if the overall prediction was false

                if (LongestMatchPred != bi->altTaken){
                    ctrupdate(useAltPredForNewlyAllocated, (bi->altTaken == taken), 4);
                }
            }
        }

        if (ALLOC) {
            // is there some "unuseful" entry to allocate
            int8_t min = 1;
            for (int i = nHistoryTables; i > bi->hitBank; i--){
                if (gtable[i][bi->tableIndices[i]].u < min){
                    min = gtable[i][bi->tableIndices[i]].u;
                }
            }

            // we allocate an entry with a longer history
            //to  avoid ping-pong, we do not choose systematically the next entry, but among the 3 next entries
            int Y = NRAND & ((1 << (nHistoryTables - bi->hitBank - 1)) - 1);
            int X = bi->hitBank + 1;
            if (Y & 1) {
                X++;
                if (Y & 2)
                    X++;
            }
            //NO ENTRY AVAILABLE:  ENFORCES ONE TO BE AVAILABLE
            if (min > 0){
                gtable[X][bi->tableIndices[X]].u = 0;
            }


            //Allocate only  one entry
            for (int i = X; i <= nHistoryTables; i += 1){
                if ((gtable[i][bi->tableIndices[i]].u == 0)) {
                    gtable[i][bi->tableIndices[i]].tag = bi->tableTags[i];
                    gtable[i][bi->tableIndices[i]].ctr = (taken) ? 0 : -1;
                    gtable[i][bi->tableIndices[i]].u = 0; //?
                }
            }
        }
        //periodic reset of u: reset is not complete but bit by bit
        tCounter++;
        if ((tCounter & ((1 << logTick) - 1)) == 0){
            // reset least significant bit
            // most significant bit becomes least significant bit
            for (int i = 1; i <= nHistoryTables; i++){
                for (int j = 0; j < (1 << tagTableSizes[i]); j++){
                    gtable[i][j].u = gtable[i][j].u >> 1;
                }
            }
        }

        if (bi->hitBank > 0){
            DPRINTF(LTage, "Updating tag table entry (%d,%d) for branch %lx\n", bi->hitBank, bi->hitBankIndex, branch_pc); 
            ctrupdate (gtable[bi->hitBank][bi->hitBankIndex].ctr, taken, tagTableCounterBits);
            //if the provider entry is not certified to be useful also update the alternate prediction
            if (gtable[bi->hitBank][bi->hitBankIndex].u == 0){
                if (bi->altBank > 0){
                    ctrupdate (gtable[bi->altBank][bi->altBankIndex].ctr, taken, tagTableCounterBits);
                    DPRINTF(LTage, "Updating tag table entry (%d,%d) for branch %lx\n", bi->hitBank, bi->hitBankIndex, branch_pc); 
                }
                if (bi->altBank == 0){
                    baseupdate (pc, taken, bi);
                }
            }

            // update the u counter
            if (LongestMatchPred != bi->altTaken){
                if (LongestMatchPred == taken){
                    if (gtable[bi->hitBank][bi->hitBankIndex].u < 1){
                        gtable[bi->hitBank][bi->hitBankIndex].u++;
                    }
                }
            }
        } else{
            baseupdate (pc, taken, bi);
        }

        //END PREDICTOR UPDATE
    }
    delete bi;
}


void LTAGE::UpdateHistories(Addr branch_pc, bool taken, void* b){
    BranchInfo* bi = (BranchInfo*)(b);
    //  UPDATE HISTORIES

    //VKN: branchPC's last bit will always be zero for ARM. shift right.
    //bool PATHBIT = ((branch_pc>>1) & 1);
    bool PATHBIT = ((branch_pc) & 1);
    //on a squash, return pointers to this and recompute indices.

    //update user history
    updateghist (gHist, taken, GHIST, ptGhist);
    pathHist = (pathHist << 1) + PATHBIT;
    pathHist = (pathHist & ((1 << 16) - 1));

    bi->ptGhist = ptGhist;
    bi->pathHist = pathHist;
    //prepare next index and tag computations for user branchs
    for (int i = 1; i <= nHistoryTables; i++)
    {
        bi->ci[i]  = computeIndices[i].comp;
        bi->ct0[i] = computeTags[0][i].comp;
        bi->ct1[i] = computeTags[1][i].comp;
        computeIndices[i].update (gHist);
        computeTags[0][i].update (gHist);
        computeTags[1][i].update (gHist);
    }
    DPRINTF(LTage, "Updating global histories with branch:%lx; taken?:%d, path Hist: %x; pointer:%d\n", branch_pc, taken, pathHist, ptGhist); 
}


void LTAGE::squash(bool taken, void *bp_history){
    BranchInfo* bi = (BranchInfo*)(bp_history);
    DPRINTF(LTage, "Restoring branch info: %lx; taken? %d; PathHistory:%x, pointer:%d\n", bi->branchPC,taken, bi->pathHist, bi->ptGhist); 
    pathHist = bi->pathHist;
    ptGhist = bi->ptGhist;
    gHist = &GHIST[ptGhist];
    gHist[0] = (taken?1:0);
    for (int i = 1; i <= nHistoryTables; i++){
        computeIndices[i].comp = bi->ci[i];
        computeTags[0][i].comp = bi->ct0[i];
        computeTags[1][i].comp = bi->ct1[i];
        computeIndices[i].update (gHist);
        computeTags[0][i].update (gHist);
        computeTags[1][i].update (gHist);
    }

    if(bi->condBranch){
        if (bi->loopHit >= 0) {
            int idx = bi->loopIndex + bi->loopHit;
            ltable[idx].CurrentIterSpec = bi->CurrentIter; 
        }
    }

}


void LTAGE::squash(void *bp_history){
    BranchInfo* bi = (BranchInfo*)(bp_history);
    DPRINTF(LTage, "Deleting branch info: %lx\n", bi->branchPC); 
    if(bi->condBranch){
        if (bi->loopHit >= 0) {
            int idx = bi->loopIndex + bi->loopHit;
            ltable[idx].CurrentIterSpec = bi->CurrentIter; 
        }
    }

    delete bi;
}

bool LTAGE::lookup(Addr branch_pc, void* &bp_history){
    bool retval = predict (branch_pc, true, bp_history);
    DPRINTF(LTage, "Lookup branch: %lx; taken predict:%d\n", branch_pc, retval); 
    UpdateHistories(branch_pc, retval, bp_history);
    assert(gHist == &GHIST[ptGhist]); 
    /*
       fprintf(stderr, "CB:%lx:\t", branch_pc); 
       for(int i=0; i<100; i++){
       fprintf(stderr, "%d", gHist[i]); 
       }
       fprintf(stderr,"\n"); 
     */

    return retval;
}

void LTAGE::btbUpdate(Addr branch_pc, void* &bp_history){
    BranchInfo* bi = (BranchInfo*) bp_history; 
    DPRINTF(LTage, "BTB miss resets prediction: %lx\n", branch_pc); 
    assert(gHist == &GHIST[ptGhist]); 
    gHist[0] = 0;
    for (int i = 1; i <= nHistoryTables; i++){
        computeIndices[i].comp = bi->ci[i];
        computeTags[0][i].comp = bi->ct0[i];
        computeTags[1][i].comp = bi->ct1[i];
        computeIndices[i].update (gHist);
        computeTags[0][i].update (gHist);
        computeTags[1][i].update (gHist);
    }
    /*
       fprintf(stderr, "BT:%lx:\t", branch_pc); 
       for(int i=0; i<100; i++){
       fprintf(stderr, "%d", gHist[i]); 
       }
       fprintf(stderr,"\n");
     */
}

void LTAGE::update(Addr branch_pc, bool taken, void *bp_history, bool squashed){
    //fprintf(stderr, "%lx:%d:%d\n", branch_pc, taken, squashed); 
    BranchInfo* bi = (BranchInfo*) bp_history; 
    if(squashed){
        DPRINTF(LTage, "Squashing Branch: %lx; taken?:%d\n", branch_pc, taken); 
        squash(taken, bp_history);
        /*
           fprintf(stderr, "Sq:%lx:\t", branch_pc); 
           for(int i=0; i<100; i++){
           fprintf(stderr, "%d", gHist[i]); 
           }
           fprintf(stderr,"\n"); 
         */
    }
    else{
        /*if(GHIST[bi->ptGhist] != taken){
            fprintf(stderr, "%lx: branch update fail; u_taken:%d, hist:%d. gHist[0]=%d, ptr:%d, cur_ptr:%d\n", branch_pc, taken, GHIST[bi->ptGhist], gHist[0], bi->ptGhist, ptGhist);
            assert(0); 
        }*/
    }
    DPRINTF(LTage, "Updating Branch: %lx; taken?:%d\n", branch_pc, taken); 
    update(branch_pc, taken, bi);
    assert(gHist == &GHIST[ptGhist]); 

}

void LTAGE::uncondBranch(Addr br_pc, void* &bp_history){
    DPRINTF(LTage, "UnConditionalBranch: %lx\n", br_pc); 
    predict(br_pc, false, bp_history);
    UpdateHistories(br_pc, true, bp_history);
    assert(gHist == &GHIST[ptGhist]); 
    /*
       fprintf(stderr, "UC:%lx:\t", br_pc); 
       for(int i=0; i<100; i++){
       fprintf(stderr, "%d", gHist[i]); 
       }
       fprintf(stderr,"\n");
     */
}
