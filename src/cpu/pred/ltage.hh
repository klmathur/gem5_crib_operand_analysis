#ifndef __CPU_PRED_LTAGE
#define __CPU_PRED_LTAGE

#include <vector>

#include "base/types.hh"
#include "cpu/pred/bpred_unit.hh"
#include "cpu/pred/sat_counter.hh"

//loop predictor entry
class lentry
{
    public:
        uint16_t NbIter;
        uint8_t confid;
        uint16_t CurrentIter;
        uint16_t CurrentIterSpec; 
        uint16_t TAG;
        uint8_t age;
        bool dir; 

        lentry (){
            confid = 0;
            CurrentIter = 0;
            CurrentIterSpec =0; 
            NbIter = 0;
            TAG = 0;
            age = 0;
            dir = false;
        }
};

//bimodal entry
class bentry{
    public:
        uint8_t hyst;
        uint8_t pred;
        bentry (){
            pred = 0;
            hyst = 1;
        }
};

//Tage entry
class gentry{
    public:
        int8_t ctr;
        uint16_t tag;
        int8_t u;
        gentry (){
            ctr = 0;
            tag = 0;
            u = 0;
        }
};

//Folded history table
class folded_history
{
    public:
        unsigned comp;
        int CLENGTH;
        int OLENGTH;
        int OUTPOINT;

        folded_history ()
        {
        }

        void init (int original_length, int compressed_length)
        {
            comp = 0;
            OLENGTH = original_length;
            CLENGTH = compressed_length;
            OUTPOINT = OLENGTH % CLENGTH;
        }

        void update (uint8_t * h)
        {
            comp = (comp << 1) | h[0];
            comp ^= h[OLENGTH] << OUTPOINT;
            comp ^= (comp >> CLENGTH);
            comp &= (1 << CLENGTH) - 1;
        }
};

struct BranchInfo{
    int pathHist;
    int ptGhist;
    int hitBank;
    int hitBankIndex;
    int altBank;
    int altBankIndex;
    int bimodalIndex;
    int *tableIndices;
    int *tableTags;
    int *ci;
    int *ct0; 
    int *ct1;
    int loopTag;
    uint16_t CurrentIter;

    bool tagePred;
    bool altTaken;
    bool loopPred;
    bool loopPredValid;
    int  loopIndex;
    int loopHit;
    bool condBranch;
    bool longestMatchPred; 
    bool pseudoNewAlloc; 
    Addr branchPC;
    BranchInfo(int sz){
        tableIndices = new int [sz];
        tableTags = new int [sz];
        ci = new int[sz];
        ct0 = new int[sz];
        ct1 = new int[sz]; 
    }
    ~BranchInfo(){
        delete tableIndices;
        delete tableTags;
        delete ci;
        delete ct0;
        delete ct1;
    }
};


class LTAGE: public BPredUnit
{
    public:
        LTAGE(const Params *params);

        void uncondBranch(Addr br_pc, void * &bp_history);
        void fallThroughBranch(void * &bp_history) { };
        bool lookup(Addr branch_addr, void * &bp_history);
        void btbUpdate(Addr branch_addr, void * &bp_history);
        void update(Addr branch_addr, bool taken, void *bp_history, bool squashed);
        void squash(void *bp_history);
        //void restore(Addr branch_addr, void* bp_history){ }

    private:
        int bindex(Addr pc_in);
        int lindex(Addr pc_in);
        int F (int A, int size, int bank);
        int gindex (Addr pc, int bank);
        uint16_t gtag (Addr pc, int bank);
        void ctrupdate (int8_t & ctr, bool taken, int nbits) ;
        bool getbim (Addr pc, BranchInfo* bi) ;
        void baseupdate (Addr pc, bool taken, BranchInfo* bi);
        bool getloop (Addr pc, BranchInfo* bi);
        void loopupdate (Addr pc, bool Taken, BranchInfo* bi);
        int MYRANDOM ();
        void updateghist (uint8_t * &h, bool dir, uint8_t * tab, int &PT);
        bool predict (Addr branch_pc, bool cond_branch, void* &b);
        void update(Addr branch_pc, bool taken, BranchInfo* bi);
        void UpdateHistories(Addr branch_pc, bool taken, void* b);
        void squash(bool taken, void *bp_history);
        void SpecLoopUpdate(Addr pc, bool taken, BranchInfo* bi);


        unsigned logSizeBiMP;
        unsigned logSizeTagTables;
        unsigned logSizeLoopPred;
        unsigned nHistoryTables;
        unsigned tagTableCounterBits;
        unsigned histBufferSize;
        unsigned minHist;
        unsigned maxHist;
        unsigned minTagWidth;

        bentry *btable;
        gentry **gtable;
        lentry *ltable;

        int pathHist;
        uint8_t* GHIST;
        uint8_t* gHist;
        int ptGhist;

        folded_history *computeIndices;
        folded_history *computeTags[2];

        int *tagWidths;
        int *histLengths;
        int *tagTableSizes;
        int *tableIndices;
        int *tableTags;

        int seed;

        int8_t loopUseCounter;
        int8_t useAltPredForNewlyAllocated;
        int tCounter;
        int logTick;

};


#endif // __CPU_PRED_LTAGE
